![Image](docs/_static/LibKet_logo_color.png?raw=true)

# LibKet - The Quantum Expression Template Library

[![License: MPL 2.0](https://img.shields.io/badge/License-MPL%202.0-brightgreen.svg)](https://opensource.org/licenses/MPL-2.0)
[![Documentation Status](https://readthedocs.org/projects/libket/badge/?version=latest)](http://libket.readthedocs.io/en/latest/?badge=latest)
[![Documentation](https://img.shields.io/badge/docs-doxygen-blue.svg)](https://mmoelle1.gitlab.io/LibKet/)
[![pipeline status](https://gitlab.com/mmoelle1/LibKet/badges/matthias_branch/pipeline.svg)](https://gitlab.com/mmoelle1/LibKet/-/commits/matthias_branch)
[![Build status](https://ci.appveyor.com/api/projects/status/ejax6hyqfnpd51j5?svg=true)](https://ci.appveyor.com/project/mmoelle168354/libket)
[![Coverage Status](https://coveralls.io/repos/gitlab/mmoelle1/LibKet/badge.svg?branch=matthias_branch)](https://coveralls.io/gitlab/mmoelle1/LibKet?branch=matthias_branch)
[![Coverity Scan Build Status](https://scan.coverity.com/projects/libket/badge.svg)](https://scan.coverity.com/projects/libket)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/e8679cc335b34b1babc4bdb34a60c100)](https://www.codacy.com/manual/mmoelle1/LibKet?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=mmoelle1/LibKet&amp;utm_campaign=Badge_Grade)

**LibKet** (pronounced lib-ket) is a lightweight [expression
template](https://en.wikipedia.org/wiki/Expression_templates) library
written in modern C++14 that provides building blocks for the rapid
prototyping of quantum algorithms and their efficient testing on
different quantum computer simulators and hardware platforms. These
building blocks are quantum bits, so-called
[qubits](https://en.wikipedia.org/wiki/Qubit), [quantum
registers](https://en.wikipedia.org/wiki/Quantum_register), [quantum
logic gates](https://en.wikipedia.org/wiki/Quantum_logic_gate), and
some basic [quantum
algorithms](https://en.wikipedia.org/wiki/Quantum_algorithm) like the
[Quantum Fourier
Transform](https://en.wikipedia.org/wiki/Quantum_Fourier_transform)
(QFT) and its inverse operation, which are both widely used in
advanced quantum algorithms.

**LibKet** makes it possible to formulate quantum algorithms in a more
abstract way than this is typically possible in other quantum
computing SDKs, which require you to express quantum algorithms using
low-level quantum gates and are often restricted to a particular
quantum assembly or instruction language. In contrast, **LibKet**
allows you to implement quantum algorithms as generic quantum
expressions that can be executed on different quantum computer
simulators and hardware plaforms.

## Getting Started

**LibKet** is designed as header-only C++ library with minimal
external dependencies. All you need to get started is a C++14 compiler
and, optionally, Python 3.x to execute quantum algorithms directly
from within **LibKet**.

The following code snippet shows how to generate the quantum algorithm
for an n-qubit QFT circuit and execute it on the [Quantum
Inspire](https://www.quantum-inspire.com/) simulator platform using
6-qubits:

```cpp
#include <LibKet.hpp>

using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

// Create generic quantum expression
auto expr = qft(init());

// Execute QFT<6> on Quantum-Inspire platform
try {
  QDevice<QDeviceType::qi_26_simulator, 6> qi; qi(expr);
  utils::json result = qi.execute();
                      
  QInfo << result << std::endl;

  QInfo << "job ID     : " << qi.get<QResultType::id>(result)               << std::endl;
  QInfo << "time stamp : " << qi.get<QResultType::timestamp>(result)        << std::endl;
  QInfo << "duration   : " << qi.get<QResultType::duration>(result).count() << std::endl;
  QInfo << "best       : " << qi.get<QResultType::best>(result)             << std::endl;
  QInfo << "histogram  : " << qi.get<QResultType::histogram>(result)        << std::endl;
                                         
} catch(const std::exception &e) {
  QWarn << e.what() << std::endl;
}
```


## Next Steps

Check out the [documentation](https://libket.readthedocs.org/) with
[installation
instructions](https://libket.readthedocs.io/en/latest/install.html)
and a
[tutorial](https://libket.readthedocs.io/en/latest/getting_started.html)
on how to use **LibKet** and the [API
reference](https://mmoelle1.gitlab.io/LibKet/).


## Copyright

Copyright (c) 2018-2020 Matthias Möller (m.moller@tudelft.nl).

In Dutch, 'Quantum' is spelled 'Kwantum', which explains the spelling
**LibKet**. The name is an allusion to the famous [bra-ket
notation](https://en.wikipedia.org/wiki/Bra%E2%80%93ket_notation) that
is widely used for expressing quantum algorithms.

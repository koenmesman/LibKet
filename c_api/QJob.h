/** @file c_api/QJob.h

@brief LibKet quantum job execution class declaration

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#pragma once
#ifndef C_API_QJOB_H
#define C_API_QJOB_H

#include <QBase.h>
#include <QConfig.h>

#ifdef LIBKET_WITH_PYTHON
// Remove keyword 'register' which is deprecated in C++11
#if __cplusplus > 199711L
#define register
#endif

#include <Python.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif
  
  struct qPythonJob;
  typedef struct qPythonJob qPythonJob_t;
  
  qError_t qPythonJobCreate(qPythonJob_t* job,
                            const char* script,
                            const char* method_run,
                            const char* method_wait,
                            const char* method_query,
                            PyObject* module,
                            PyObject* global,
                            PyObject* local,
                            const qPythonJob_t* depend);
  qError_t qPythonJobDestroy(qPythonJob_t* job);
  qError_t qPythonJobRun(qPythonJob_t* job, qPythonJob_t* depend);
  qError_t qPythonJobGet(const qPythonJob_t* job /* C JSON */);
  qError_t qPythonJobWait(const qPythonJob_t* job);
  qError_t qPythonJobQuery(const qPythonJob_t* job);
  qError_t qPythonJobDuration(const qPythonJob_t* job, double* count);
  
  struct qCppJob;
  typedef struct qCppJob qCppJob_t;

  qError_t qCppJobCreate(qCppJob_t* job);
  qError_t qCppJobDestroy(qCppJob_t* job);
  qError_t qCppJobRun(const qCppJob_t* job, qCppJob_t* depend);
  qError_t qCppJobWait(const qCppJob_t* job);
  qError_t qCppJobQuery(const qCppJob_t* job);
  qError_t qCppJobDuration(const qCppJob_t* job, double* count);
  
#ifdef __cplusplus
}
#endif

#endif // C_API_QJOB_H

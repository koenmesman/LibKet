/** @file c_api/QStream.cpp

@brief LibKet quantum stream class implementation

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#include <stdlib.h>

#include <QStream.h>

#include <QBase.hpp>

/**
   @brief LibKet quantum stream class specialization for the Python
   execution unit
   
   \ingroup C_API
*/
struct qPythonStream {
  void *obj;
};

/**
   @brief LibKet quantum stream class specialization for the C++
   execution unit
   
   \ingroup C_API
*/
struct qCppStream {
  void *obj;
};


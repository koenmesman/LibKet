/** @file c_api/QStream.h

@brief LibKet quantum stream class declaration

@copyright This file is part of the LibKet library (C API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#pragma once
#ifndef C_API_QSTREAM_H
#define C_API_QSTREAM_H

#include <QBase.h>
#include <QConfig.h>

#ifdef __cplusplus
extern "C" {
#endif

  struct qPythonStream;
  typedef struct qPythonStream qPythonStream_t;

  qError_t qPythonStreamCreate(qPythonStream_t* stream);
  qError_t qPythonStreamDestroy(qPythonStream_t* stream);

  qError_t qPythonStreamRun(qPythonStream_t* stream,
                            const char* py_script,
                            const char* py_method_run,
                            const char* py_method_wait,
                            const char* py_method_query);

  qError_t qPythonStreamSize(qPythonStream_t* stream, size_t* size);
  qError_t qPythonStreamWait(qPythonStream_t* stream);
  qError_t qPythonStreamQuery(qPythonStream_t* stream);
  
  struct qCppStream;
  typedef struct qCppStream qCppStream_t;

  qError_t qCppStreamCreate(qCppStream_t* stream);
  qError_t qCppStreamDestroy(qCppStream_t* stream);

  qError_t qCppStreamRun(qCppStream_t* stream);
  
  qError_t qCppStreamSize(qCppStream_t* stream, size_t* size);
  qError_t qCppStreamWait(qCppStream_t* stream);
  qError_t qCppStreamQuery(qCppStream_t* stream);
  
#ifdef __cplusplus
}
#endif

#endif // C_API_QSTREAM_H

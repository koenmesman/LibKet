/** @file examples/c-tutorial01.c

    @brief LibKet C-tutorial01: Quantum expressions and quantum filters

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <stdio.h>

#include "LibKet.h"

int main(int argc, char *argv[])
{
  printf("Hello World\n");
  
  return 0;
}

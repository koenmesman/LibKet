########################################################################
# OpenQL.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2020 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# OpenQL
########################################################################

if (LIBKET_BUILTIN_OPENQL)
  
  # include(DownloadProject)
  # download_project(
  #   PROJ              OpenQL
  #   GIT_REPOSITORY    https://github.com/QE-Lab/OpenQL.git
  #   GIT_TAG           develop
  #   TIMEOUT           180
  #   PREFIX            ${CMAKE_BINARY_DIR}/external/OpenQL
  #   ${UPDATE_DISCONNECTED_IF_AVAILABLE}
  #   )

  set(OpenQL_BINARY_DIR ${CMAKE_BINARY_DIR}/external/OpenQL)
  set(OpenQL_SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/OpenQL)
  
  # Add include directory
  include_directories("${OpenQL_SOURCE_DIR}/src" 
    "${OpenQL_SOURCE_DIR}/deps/CLI11/include" 
    "${OpenQL_SOURCE_DIR}/deps/lemon" 
    "${OpenQL_SOURCE_DIR}/deps/libqasm/src/library")
  
else()

  # Add include directory
  if(OpenQL_INCLUDE_PATH)
    include_directories(${OpenQL_INCLUDE_PATH})
  else()
    message(WARNING "Variable OpenQL_INCLUDE_PATH is not defined. LibKet might be unable to find OpenQL include files.")
  endif()
    
endif()

# Process OpenQL project (shared-library mode is broken)
if (NOT TARGET ql)
  set(BUILD_SHARED_LIBS_SAVED "${BUILD_SHARED_LIBS}")
  set(BUILD_SHARED_LIBS OFF)
  add_subdirectory(${OpenQL_SOURCE_DIR} ${OpenQL_BINARY_DIR})
  set(BUILD_SHARED_LIBS "${BUILD_SHARED_LIBS_SAVED}")
endif()
  
# Add include directory
include_directories("${OpenQL_SOURCE_DIR}/include")
include_directories("${OpenQL_BINARY_DIR}/deps/lemon")
include_directories("${OpenQL_SOURCE_DIR}/deps/lemon")
include_directories("${OpenQL_SOURCE_DIR}/deps/CLI11/include")
include_directories("${OpenQL_SOURCE_DIR}/deps/libqasm/src/library")

# Add link directory
link_directories("${OpenQL_BINARY_DIR}")
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   ql)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES ql)

# Copy JSON configuration files 
file(GLOB_RECURSE OpenQL_JSONS 
  "${OpenQL_SOURCE_DIR}/tests/hardware_config_cc_light.json"
  "${OpenQL_SOURCE_DIR}/tests/hardware_config_cc_light17.json"
  "${OpenQL_SOURCE_DIR}/tests/hardware_config_qx.json"
  "${OpenQL_SOURCE_DIR}/tests/test_config_default.json"
  )
file(COPY ${OpenQL_JSONS} DESTINATION ${PROJECT_BINARY_DIR})


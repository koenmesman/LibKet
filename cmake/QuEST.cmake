########################################################################
# QuEST.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2020 by the LibKet authors
#
# This file is part of the LibKet library
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# QuEST
########################################################################

if (LIBKET_BUILTIN_QUEST)
  
  # include(DownloadProject)
  # download_project(
  #   PROJ              QUEST
  #   GIT_REPOSITORY    https://github.com/QuEST-Kit/QuEST.git
  #   GIT_TAG           develop
  #   TIMEOUT           180
  #   PREFIX            ${CMAKE_BINARY_DIR}/external/QuEST
  #   ${UPDATE_DISCONNECTED_IF_AVAILABLE}
  #   )
 
  set(QUEST_BINARY_DIR ${CMAKE_BINARY_DIR}/external/QuEST)
  set(QUEST_SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/QuEST)
  
  set(TESTING        0 CACHE BOOL "")
  
  # Add include directory
  include_directories("${QUEST_SOURCE_DIR}/QuEST/include")
  
else()

  # Add include directory
  if(QUEST_INCLUDE_PATH)
    include_directories(${QUEST_INCLUDE_PATH})
  else()
    message(WARNING "Variable QUEST_INCLUDE_PATH is not defined. LibKet might be unable to find QuEST include files.")
  endif()
    
endif()

# Process QuEST project
if (NOT TARGET QuEST)
  add_subdirectory(${QUEST_SOURCE_DIR} ${QUEST_BINARY_DIR})
endif()

# Get compile definitions
get_target_property(QUEST_COMPILE_DEFINITIONS QuEST COMPILE_DEFINITIONS)

# Add include directory
include_directories("${QUEST_SOURCE_DIR}/QuEST/include")

# Add library
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   QuEST)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES QuEST)

########################################################################
# UnitTest++.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2020 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

########################################################################
# UnitTest
########################################################################

if (LIBKET_BUILTIN_UNITTESTS)
  
  # include(DownloadProject)
  # download_project(
  #   PROJ              unittest
  #   GIT_REPOSITORY    https://github.com/unittest-cpp/unittest-cpp
  #   GIT_TAG           master
  #   TIMEOUT           180
  #   PREFIX            ${CMAKE_BINARY_DIR}/external/UnitTest++
  #   PATCH_COMMAND     patch -p1 -N -d ${CMAKE_BINARY_DIR}/external/UnitTest++/unittest-src < ${CMAKE_SOURCE_DIR}/patches/UnitTest++_cmake.patch
  #   ${UPDATE_DISCONNECTED_IF_AVAILABLE}
  #   )

  set(UNITTESTPP_BINARY_DIR ${CMAKE_BINARY_DIR}/external/unittest-cpp)
  set(UNITTESTPP_SOURCE_DIR ${CMAKE_SOURCE_DIR}/external/unittest-cpp)
    
  # Add include directory
  include_directories("${UNITTESTPP_SOURCE_DIR}/UnitTest")
  
else()
  
  # Add include directory
  if(UNITTESTPP_INCLUDE_PATH)
    include_directories(${UNITTESTPP_INCLUDE_PATH})
  else()
    message(WARNING "Variable UNITTESTPP_INCLUDE_PATH is not defined. LIBKET might be unable to find UnitTest++ include files.")
  endif()

endif()

# Process UnitTest++ project
if (NOT TARGET UnitTest++)
  add_subdirectory(${UNITTESTPP_SOURCE_DIR} ${UNITTESTPP_BINARY_DIR})
endif()

# Add include directory
include_directories("${UNITTESTPP_SOURCE_DIR}")

# Add library
list(APPEND LIBKET_C_TARGET_LINK_LIBRARIES   UnitTest++)
list(APPEND LIBKET_CXX_TARGET_LINK_LIBRARIES UnitTest++)

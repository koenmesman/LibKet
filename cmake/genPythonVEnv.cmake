########################################################################
# genPythonVEnv.cmake
#
# Author: Matthias Moller
# Copyright (C) 2018-2020 by the LibKet authors
#
# This file is part of the LibKet project
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.
# 
########################################################################

#
# CMake macro: generate custom targets to create Python virtual environments
#
macro(genPythonVEnv)


  # Cirq
  foreach(version in 0.3.1.27 0.3.1.35 
      0.4.0
      0.5.0 0.5.555 0.5.556
      0.6.0
      0.7.0
      0.8.0 0.8.1 0.8.2)

    add_custom_target(cirq-${version} 
      COMMAND ${CMAKE_COMMAND} -E make_directory ${PROJECT_BINARY_DIR}/venv/cirq-${version}
      COMMAND ${PYTHON_EXECUTABLE} -m venv ${PROJECT_BINARY_DIR}/venv/cirq-${version}
      COMMAND ${PROJECT_BINARY_DIR}/venv/cirq-${version}/bin/pip install --upgrade pip setuptools
      COMMAND ${PROJECT_BINARY_DIR}/venv/cirq-${version}/bin/pip install cirq==${version}
      )
  endforeach()

  add_custom_target(cirq DEPENDS cirq-0.8.2)

  # Quantum Inspire
  foreach(version in 0.4.0 0.4.2 
      0.5.0 
      0.6.0 
      0.7.0 
      1.0.0 
      1.1.0
      1.2.0)
    add_custom_target(quantuminspire-${version} 
      COMMAND ${CMAKE_COMMAND} -E make_directory ${PROJECT_BINARY_DIR}/venv/quantuminspire-${version}
      COMMAND ${PYTHON_EXECUTABLE} -m venv ${PROJECT_BINARY_DIR}/venv/quantuminspire-${version}
      COMMAND ${PROJECT_BINARY_DIR}/venv/quantuminspire-${version}/bin/pip install --upgrade pip setuptools
      COMMAND ${PROJECT_BINARY_DIR}/venv/quantuminspire-${version}/bin/pip install quantuminspire==${version} matplotlib
      )
  endforeach()

  add_custom_target(quantuminspire DEPENDS quantuminspire-1.2.0)

  # Qiskit
  foreach(version in 0.3.2 0.3.3 0.3.4 0.3.5 0.3.6 0.3.7 0.3.8 0.3.9 0.3.10 0.3.11 0.3.12 0.3.13 0.3.14 0.3.15 0.3.16 
      0.4.0 0.4.1 0.4.2 0.4.3 0.4.4 0.4.5 0.4.6 0.4.7 0.4.8 0.4.9 0.4.10 0.4.11 0.4.12 0.4.13 0.4.14 0.4.15 
      0.5.0 0.5.1 0.5.2 0.5.3 0.5.4 0.5.5 0.5.6 0.5.7 
      0.6.0 0.6.1 
      0.7.0 0.7.1 0.7.2 0.7.3 
      0.8.0 0.8.1 
      0.9.0 
      0.10.0 0.10.1 0.10.2 0.10.3 0.10.4 0.10.5 
      0.11.0 0.11.1 0.11.2 
      0.12.0 0.12.1 0.12.2 
      0.13.0 
      0.14.0 0.14.1 
      0.15.0 
      0.16.0 0.16.1 0.16.2
      0.17.0
      0.18.0 0.18.1 0.18.2 0.18.3
      0.19.0 0.19.1 0.19.2 0.19.3 0.19.4 0.19.5 0.19.6)
    add_custom_target(qiskit-${version} 
      COMMAND ${CMAKE_COMMAND} -E make_directory ${PROJECT_BINARY_DIR}/venv/qiskit-${version}
      COMMAND ${PYTHON_EXECUTABLE} -m venv ${PROJECT_BINARY_DIR}/venv/qiskit-${version}
      COMMAND ${PROJECT_BINARY_DIR}/venv/qiskit-${version}/bin/pip install --upgrade pip setuptools
      COMMAND ${PROJECT_BINARY_DIR}/venv/qiskit-${version}/bin/pip install qiskit==${version} matplotlib
      )
  endforeach()

  add_custom_target(qiskit DEPENDS qiskit-0.19.6)


  # Qsharp
  foreach(version in 0.5.1902.2802 0.5.1903.2902 0.5.1904.1302
      0.6.1905.301
      0.7.1905.1603b1 0.7.1905.2101b1 0.7.1905.2303b1 0.7.1905.3003b1 0.7.1905.3109 
      0.8.1906.1704b1 0.8.1907.1701 
      0.9.1908.2902 0.9.1908.2906 0.9.1909.3002 
      0.10.1910.3107 0.10.1911.307 0.10.1911.1602b1 0.10.1911.1606b1 0.10.1911.1607 0.10.1912.501 0.10.1912.1606b1 0.10.2001.2811 0.10.2001.2831 0.10.2002.2610 
      0.11.2003.2506 0.11.003.3107)

    add_custom_target(qsharp-${version} 
      COMMAND ${CMAKE_COMMAND} -E make_directory ${PROJECT_BINARY_DIR}/venv/qsharp-${version}
      COMMAND ${PYTHON_EXECUTABLE} -m venv ${PROJECT_BINARY_DIR}/venv/qsharp-${version}
      COMMAND ${PROJECT_BINARY_DIR}/venv/qsharp-${version}/bin/pip install --upgrade pip setuptools
      COMMAND ${PROJECT_BINARY_DIR}/venv/qsharp-${version}/bin/pip install qsharp==${version}
      )
  endforeach()

  add_custom_target(qsharp DEPENDS qsharp-0.11.2003.3107)

  # PyQuil
  foreach(version in 0.0.1 0.0.2 0.0.3 0.1.0 
      1.0.0 
      1.1.1 1.1.2 1.1.3 1.1.4 
      1.2.0 
      1.3.0 1.3.1 1.3.2 
      1.4.0 1.4.1 1.4.2 1.4.3 1.4.4 1.4.5 1.4.6 
      1.5.0 1.5.1 
      1.6.0 1.6.1 1.6.2 1.6.3 
      1.7.0
      1.8.0 
      1.9.0
      2.0.0b1 2.0.0b3 2.0.0b4 2.0.0b5 2.0.0b6 2.0.0b7 2.0.0b8 2.0.0 
      2.1.0 2.1.1 
      2.2.0 2.2.1 
      2.3.0 
      2.4.0
      2.5.0 2.5.1 2.5.2 
      2.6.0 
      2.7.0 2.7.1 2.7.2 
      2.8.0 
      2.9.0 2.9.1 
      2.10.0 
      2.11.0 
      2.12.0 
      2.13.0 
      2.14.0 
      2.15.0 
      2.16.0 
      2.17.0 
      2.18.0
      2.19.0
      2.20.0
      2.21.0)
    add_custom_target(pyquil-${version} 
      COMMAND ${CMAKE_COMMAND} -E make_directory ${PROJECT_BINARY_DIR}/venv/pyquil-${version}
      COMMAND ${PYTHON_EXECUTABLE} -m venv ${PROJECT_BINARY_DIR}/venv/pyquil-${version}
      COMMAND ${PROJECT_BINARY_DIR}/venv/pyquil-${version}/bin/pip install --upgrade pip setuptools
      COMMAND ${PROJECT_BINARY_DIR}/venv/pyquil-${version}/bin/pip install pyquil==${version}
      )
  endforeach()

  add_custom_target(pyquil DEPENDS pyquil-2.19.0)

endmacro()

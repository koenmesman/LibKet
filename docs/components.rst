.. _LibKet Components:

Components
==========

.. _LibKet Filters:

Filters
-------
Starting from the global Q-memory, filters allow you to restrict the qubits on which an action is performed. There are several ways in which one can create a filter, we will explain those by the following lines of code. 

.. code-block:: cpp
	:linenos:

	auto f0 = select<0,2,3>();
	auto f1 = range<1,2>(f0);
	auto f2 = tag<0>(f1);
	auto f3 = qubit<1>(f2);
	auto f4 = tag<1>(f3);
	auto f5 = gototag<0>(f4);

In line 1 we first create a filter f0 which selects the 0th, 2nd and 3rd qubit from the original register. 

In line 2 we create a filter f1 which selects qubits from f0. Since it selects the range from the 1st to the 2nd qubit in that register it selects qubit 2 and 3. 

In line 3 we use another LibKet functionality called tag. Tag assigns a unique tag to a position in a quantum filter chain. In this line we assign the tag 0 to the selection of qubits that are now in f1. It can be used together with the gototag filter to revert to a previous location in the quantum filter chain. 


in line 4 we create a filter f3 which consists of the 1st qubit from f2, so it consists of qubit 2.

In line 5 we create another tag, this time on the qubits of filter 3. 

In line 6 we use gototag. Gototag can be used to revert back to a previous location in the quantum filter chain. Here we are in the position f4, but we use gototag<0> on f4 to go back to the position we were in when we used tag<0>. So in this case we go back to filter f1. 




.. _LibKet Gates:

Gates
-----

Gates apply to all qubits of the current filter chain (SIMD-ops). We will first give a small example that shows how how these gates can be used. 

.. code-block:: cpp
	:linenos:

	auto e0 = init();
	auto e1 = sel<0,2>(e0);
	auto e2 = h(e1); 
	auto e3 = all(e2);
	auto e4 = cnot(sel<0,2>(), sel<1,4>(e3));
	auto e5 = measure(all(e4));

In this line of code we create the following quantum circuit. 

.. image:: images/gates_picture.png
  :width: 300
  :alt: This image shows the circuit created with the above line of code

In LibKet we have provided as standard implementation of many of the quantum gates commonly used in quantum algorithms. Below we give an overview of the gates of which LibKet has a standard implementation. In the examples below we are applying the states to a register of qubits named e0. 

.. _LibKet Gates CNOT: 

CNOT
^^^^

The CNOT gate can be applied to the 0th and 1st qubit of e0 as follows.

.. code-block:: cpp

	auto e1 = cnot(sel<0>(), sel<1>(e0));


The CNOT gate has the following matrix representation: 

.. @MEREL: INSERT MATRIX REP



.. _LibKet Gates Hadamard: 

Hadamard
^^^^^^^^

The Hadamard gate can be applied to a selection of qubits named e0 as follows. 

.. code-block:: cpp

	auto e1 = h(e0);

The Hadamard gate has the following matrix representation: 

.. @MEREL: INSERT MATRIX REP


.. _LibKet Circuits:

Circuits
--------
Certain quantum circuits are used in the implementation of many quantum algorithms. To make it easier to implement large quantum algorithms some of these circuits are standardly implemented in LibKet. This way the user can create a large quantum circuit with just a single line of code!

The code below can be used to apply the Quantum Fourier Transform on qubits 0 to n. 

.. code-block:: cpp

	auto expr = qft(range<0,n>(init()));

This generates the following circuit

.. image:: images/qft_knipsel.png
  :width: 700
  :alt: This image shows the circuit created with the above line of code

Apart from QFT LibKet also has a standard implementation of the following other quantum circuits. 

.. _LibKet Circuits AllSwap:

AllSwap
^^^^^^^

The LibKet AllSwap circuit swaps all qubits in a given selection. 

The LibKet AllSwap circuit can be applied to the first n qubits of your register as follows: 

.. code-block:: cpp

	auto e1 = AllSwap(range<0,n>(init()));

This creates the following circuit for n = 5:
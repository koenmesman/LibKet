.. _LibKet FAQ:
   
Frequently Asked Questions
==========================

#. **Why the name LibKet?**

   **LibKet** stands for Quantum Expression Template Library. In
   Dutch, the word `Quantum` is spelled `Kwantum`, which explains the
   spelling **LibKet**. The name is an allusion to the famous `bra-ket
   notation <https://en.wikipedia.org/wiki/Bra–ket_notation>`_ that is
   widely used for expressing quantum algorithms.
   
   
#. **Can you add support for the quantum device XZY?**

   Sure, as long as there exists either a Python package or a C/C++
   API that allows to communicate with the quantum device, we can add
   support for it. Just let us know which quantum device you would
   like to see supported in **LibKet**.


#. **I think I found a bug in LibKet. How can I report it?**

   Please create an `issue
   <https://gitlab.com/mmoelle1/LibKet/-/issues>`_ describing the bug,
   your specific setup (operating system and compiler versions) and
   the steps to reproduce the bug. We will take care it is fixed.


#. **I like LibKet. Can I contribute to its further development?**

   That's nice to here. Sure, you are more than welcome to contribute
   to the developmet of LibKet. Please contact one of the main
   developers to discuss possible collaborations.

.. _LibKet Getting Started:

Getting Started with LibKet
===========================

LibKet is designed as header-only C++ library with minimal external
dependencies. All you need to get started is a C++14 (or better)
compiler and, optionally, Python 3.x to execute quantum algorithms
directly from within LibKet. Instructions on how to :ref:`install
prerequisites<LibKet Installing prerequisites>`, :ref:`download<LibKet
Downloading LibKet>`, and :ref:`configure and
build<LibKet Configuring and building LibKet>` can be found below.

.. _LibKet Docker images:

Docker images
-------------

If you want to explore LibKet just quickly without going through all
installation steps we recommend trying the pre-build `Docker
images <https://hub.docker.com/repository/docker/mmoelle1/libket>`_

Quantum Inspire
~~~~~~~~~~~~~~~

For instance, if you want to run one of the existing tutorials on the Quantum

.. _LibKet Installing prerequisites:

Installing prerequisites
------------------------

LibKet uses standard C++14 code and has minimal requirements, which are as follows:

- C/C++compiler that supports the C++14 standard (or better);
- `CMake <http://www.cmake.org>`_ configuration tools version 3.x (or better);
- `Python <https://www.python.org>`_ version 3.x (or better) header and library files (optional);
- `Doxygen <http://www.doxygen.org>`_ documentation tool (optional);
- `Sphinx <https://www.sphinx-doc.org>`_ documentation tool (optional)

These prerequisites can be installed as follows:

Linux RedHat/CentOS 7.x (or better)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

First, install the basic development tools

.. code-block:: bash

    sudo yum group install "Development Tools"

Next, install CMake3, Doxygen (optional) and Python3.x (optional)

.. code-block:: bash
                
    sudo yum install cmake3 doxygen git python36u python36u-devel python36u-libs

Finally, you need to install a more recent version of GCC than the one
that is shipped with RedHat/CentOS 7.x from the `Software Collections
<https://www.softwarecollections.org/en/>`_

.. code-block:: bash
                
    sudo yum install centos-release-scl
    sudo yum install devtoolset-7

From now on, GCC v7.x can be used by running

.. code-block:: bash
                
    scl enable devtoolset-7 bash

Linux Ubuntu 18.x (or better)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

All prerequisites can be installed by running the following oneliner

.. code-block:: bash
                
    sudo apt-get update
    sudo apt install build-essential cmake doxygen git python3-dev python3-pip --fix-missing

macOS
~~~~~

The easiest way to get started under macOS is to install `XCode
<https://developer.apple.com/xcode/>`_ via `Apple's AppStore
<https://apps.apple.com/us/app/xcode/id497799835?mt=12>`_ and the
package manager `homebrew <https://brew.sh>`_. Then you can install
the prerequisites as follows

.. code-block:: bash
                
    brew update
    brew install cmake doxygen gcc python 

Windows 10
~~~~~~~~~~

The easiest way to get started under Windows 10 is to install the
`Windows 10 Linux subsystem
<https://docs.microsoft.com/en-us/windows/wsl/install-win10>`_ and a
Linux distribution of your choice and compile and run LibKet
inside the Linux subsystem. In our experience, `Ubuntu Linux
<https://tutorials.ubuntu.com/tutorial/tutorial-ubuntu-on-windows>`_
works fine.

.. _LibKet Downloading LibKet:

Downloading LibKet
------------------

.. _LibKet Stable version:

Stable version
~~~~~~~~~~~~~~

**Compilation status:** |Build status stable| |Coverage report stable|

.. |Build status stable| image:: https://gitlab.com/mmoelle1/LibKet/badges/master/pipeline.svg)](https://gitlab.com/mmoelle1/LibKet/commits/master
.. |Coverage report stable| image:: https://gitlab.com/mmoelle1/LibKet/badges/master/coverage.svg)](https://gitlab.com/mmoelle1/LibKet/commits/master

The latest **stable version** of the LibKet code can be obtained
from `GitLab <https://gitlab.com/mmoelle1/LibKet>`_ as:

-  https://gitlab.com/mmoelle1/LibKet/-/archive/master/LibKet-master.zip
-  https://gitlab.com/mmoelle1/LibKet/-/archive/master/LibKet-master.tar.gz
-  https://gitlab.com/mmoelle1/LibKet/-/archive/master/LibKet-master.tar.bz2
-  https://gitlab.com/mmoelle1/LibKet/-/archive/master/LibKet-master.tar

If you are interested in a specific version you can download zip files
of `specific releases <https://gitlab.com/mmoelle1/LibKet/releases>`_.

A better way to obtain the latest revision from `GitLab
<https://gitlab.com/mmoelle1/LibKet>`_ and additionally have the
convenience to receive updates of the code is to use Git.

On **Linux**/**macOS**, you may checkout the latest revision using
        
.. code-block:: bash
                
    git clone https://gitlab.com/mmoelle1/LibKet.git

or

.. code-block:: bash
                
    git clone git@gitlab.com:mmoelle1/LibKet.git


On **Windows**, you can use `GitHub Windows client
<https://windows.github.com>`_ or any other Git client.


.. _LibKet Developer version:

Developer version
~~~~~~~~~~~~~~~~~

**Compilation status:** |Build status develop| |Coverage report develop|

.. |Build status develop| image:: https://gitlab.com/mmoelle1/LibKet/badges/develop/pipeline.svg)](https://gitlab.com/mmoelle1/LibKet/commits/develop
.. |Coverage report develop| image:: https://gitlab.com/mmoelle1/LibKet/badges/develop/coverage.svg)](https://gitlab.com/mmoelle1/LibKet/commits/develop

If you are interested in trying out the **development version** of the
LibKet code switch to the `develop` branch once you the initial
cloning of the Git repository succeeded

.. code-block:: bash
                
    cd LibKet
    git checkout --track origin/develop

    
.. _LibKet Configuring and building LibKet:

Configuring and building LibKet
-------------------------------

Assuming that LibKet has been downloaded to the source folder
``LibKet`` the following sequence of commands will compile all
examples with the `common Quantum Assembly Language (CQASM) v1.0
<https://arxiv.org/abs/1805.09607>`_ (cQASMv1) backend enabled and
execute the program ``tutorial01_simple``.

.. code-block:: bash
                
   cd LibKet
   mkdir build
   cd build
   cmake .. -DLIBKET_WITH_EXAMPLES=ON -DLIBKET_WITH_CQASM=ON
   make
   ...
   [100%] Built
   ./examples/tutorial01_simple

Please note that the so-compiled tutorials try to execute the quantum
kernels on QuTech's `Quantum Inspire (QI)
<https://quantuminspire.com>`_ cloud platform, which requires you to
have a user account. Once you created a user account it suffices to
set the following environment variables

**Bash**

.. code-block:: bash

    export QI_USERNAME=<your username>
    export QI_PASSWORD=<your password>

**Csh/Tcsh**

.. code-block:: bash

    setenv QI_USERNAME <your username>
    setenv QI_PASSWORD <your password>

The tutorials will use this information to establish the connection
with the remote QI simulator.

Activating additional quantum backends
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

LibKet supports the following quantum computing backends

+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| backend name                     | description                                                                                        | third-party prerequisites       |
+==================================+====================================================================================================+=================================+
| ``LibKet::QBackend::AQASM``      | `Atos Quantum Assembly Language (AQASM) <https://atos.net/en/solutions/quantum-learning-machine>`_ | (1)                             |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::Cirq``       | `Cirq <https://github.com/quantumlib/Cirq>`_                                                       | ``pip3 install cirq``           |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::cQASMv1``    | `Common Quantum Assembly Language (cQASM) v1.0 <https://arxiv.org/abs/1805.09607>`_                | ``pip3 install quantuminspire`` |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::openQASMv2`` | `Open Quantum Assembly Language (openQASM) v2.0 <https://arxiv.org/abs/1707.03429>`_               | ``pip3 install qiskit``         |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::OpenQL``     | `QuTech's OpenQL framework <https://github.com/QE-Lab/OpenQL>`_                                    | (2)                             |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::QASM``       | `QASM for the quantum circuit viewer qasm2circ <https://www.media.mit.edu/quanta/qasm2circ>`_      | none (2)                        |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::Quil``       | `Rigetti's Quantum Instruction Language <https://arxiv.org/abs/1608.03355>`_                       | ``pip3 install pyquil``         |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::QuEST``      | `Quantum Exact Simulation Toolkit (QuEST) <https://quest.qtechtheory.org>`_                        | (2)                             |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+
| ``LibKet::QBackend::QX``         | `QuTech's QX simulator <https://github.com/QE-Lab/qx-simulator>`_                                  | (2)                             |
+----------------------------------+----------------------------------------------------------------------------------------------------+---------------------------------+

- (1) Atos requires you to have access to a Quantum Learning Maching
  (QLM) and does not permit to copy the proprietary toolkit from
  there. So there are no prerequisites that you can install yourself.

- (2) These prerequisites are bundled with LibKet as Git submodules
  and do not have to be installed separately. It is, however, still
  possible to install them externally and request LibKet to use them
  by passing the following arguments to CMake

  .. code-block:: bash

      -DLIBKET_BUILTIN_QUEST=OFF -QUEST_INCLUDE_PATH=<path to QuEST include files>
   
.. _LibKet Generating the LibKet documentation:

Generating the LibKet documentation
-----------------------------------

If `Doxygen <http://www.doxygen.org/>`_ is available on your system,
you can generate and open the Doxygen HTML pages by executing

.. code-block:: bash
                
    cd build
    make Doxygen
    ...
    Built target Doxygen
    firefox doc/doxygen/html/index.html


If `Sphinx <https://www.sphinx-doc.org/>`_ is available on your
system, you can generate and open the Sphinx HTML pages by executing

.. code-block:: bash
                
    cd build
    make Sphinx
    ...
    Built target Sphinx
    firefox doc/sphinx/index.html

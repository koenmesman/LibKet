.. _LibKet Index:

.. image:: _static/LibKet_logo_color.png
    :alt: LibKet

Welcome to LibKet
=================

The open-source expression template library LibKet (pronounced
lib-ket) makes it possible to develop quantum-accelerated scientific
applications and test them on different gate-based quantum computing
platforms like QuTech's `Quantum Inspire
<https://quantuminspire.com>`_, IBM's `Quantum Experience
<https://quantum-computing.ibm.com>`_, Rigetti's `Quantum Cloud
Services <https://qcs.rigetti.com>`_ and many others without the need
to reimplement quantum algorithms in vendor-specific SDKs like `Cirq
<https://cirq.readthedocs.io>`_, `Qiskit <https://qiskit.org>`_ and
`PyQuil <http://docs.rigetti.com/>`_.

LibKet is designed as C++14 expression template library that allows to
formulate quantum algorithms as generic expressions, which are
synthesized to backend-optimized quantum kernels that can be executed
on quantum simulators and cloud-based quantum computers.

Next to the basic quantum :ref:`gates<LibKet Gates>`, LibKet comes
with a growing collection of customizable quantum
:ref:`circuits<LibKet Circuits>` like the Quantum Fourier
transformation (QFT) that simplify the development of
quantum-accelerated applications.

All you need to get started is a C++14 (or better) compiler and,
optionally, `Python 3.x <https://www.python.org>`_ to execute quantum
algorithms directly from within LibKet. The following code snippet
shows how to generate the quantum algorithm for an :math:`n`-qubit QFT
circuit (line 9) and execute it on the `Quantum Inspire
<https://quantuminspire.com>`_ cloud platform using 6-qubits (lines
13-14):

.. code-block:: cpp
   :linenos:
   :emphasize-lines: 9,13,14

    #include <LibKet.hpp>

    using namespace LibKet;
    using namespace LibKet::circuits;
    using namespace LibKet::filters;
    using namespace LibKet::gates;
    
    // Create generic quantum expression
    auto expr = qft(init());
    
    // Execute QFT<6> on Quantum-Inspire platform
    try {
      QDevice<QDeviceType::qi_26_simulator, 6> qi; qi(expr);
      utils::json result = qi.execute();
      
      QInfo << "job ID     : " << qi.get<QResultType::id>(result)               << std::endl;
      QInfo << "time stamp : " << qi.get<QResultType::timestamp>(result)        << std::endl;
      QInfo << "duration   : " << qi.get<QResultType::duration>(result).count() << std::endl;
      QInfo << "best       : " << qi.get<QResultType::best>(result)             << std::endl;
      QInfo << "histogram  : " << qi.get<QResultType::histogram>(result)        << std::endl;
      
    } catch(const std::exception &e) {
      QWarn << e.what() << std::endl;
    }

.. _LibKet User Documentation:

User Documentation
------------------

.. toctree::
   :maxdepth: 2

   getting_started
   components
   release_notes
   faq

.. Hiding - Indices and tables
   :ref:`genindex`
   :ref:`modindex`
   :ref:`search`

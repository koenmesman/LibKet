/** @file examples/tutorial12_hook.cpp

    @brief LibKet tutorial12: Quantum expression with hook gate

    This tutorial illustrates the usage of the hook gate

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include <iostream>

#include <LibKet.hpp>

// Import LibKet namespaces
using namespace LibKet;
using namespace LibKet::circuits;
using namespace LibKet::filters;
using namespace LibKet::gates;

namespace LibKet {
  QFunctor_alias(Ftor_expr, measure(h(init())) );
  QFunctor_alias(Ftor_str,  std::string("h q[#]\ni q[#]\n") );  
}

int main(int argc, char *argv[])
{
  // Set number of qubits
  const std::size_t nqubits = 6;
  
  // Create quantum gate from expression functor
  auto expr = hook< Ftor_expr >();
  show<99>(expr);

  // Create quantum gate from string functor (only cQASM)
  auto expr_cQASM = hook< Ftor_str >( init() );
  show<99>(expr_cQASM);
    
  // Create empty JSON object to receive results
  utils::json result;

#ifdef LIBKET_WITH_AQASM
  QInfo << "\n\n\n===== AQASM =====\n\n\n";
  QDevice<QDeviceType::atos_qlm_feynman_simulator, nqubits> aqlm;
  QInfo << aqlm(expr) << std::endl;
  
  try {
    result = aqlm.eval();

    QInfo << "job ID     : " << aqlm.get<QResultType::id>(result) << std::endl;
    QInfo << "time stamp : " << aqlm.get<QResultType::timestamp>(result) << std::endl;
    QInfo << "duration   : " << aqlm.get<QResultType::duration>(result).count() << std::endl;
    QInfo << "best       : " << aqlm.get<QResultType::best>(result) << std::endl;
    QInfo << "histogram  : " << aqlm.get<QResultType::histogram>(result) << std::endl;
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif

#ifdef LIBKET_WITH_CIRQ
  QInfo << "\n\n\n===== Cirq =====\n\n\n";
  QDevice<QDeviceType::cirq_simulator, nqubits> cirq;
  QInfo << cirq(expr) << std::endl;
  
  try {
    result = cirq.eval();

    QInfo << "job ID     : " << cirq.get<QResultType::id>(result) << std::endl;
    QInfo << "time stamp : " << cirq.get<QResultType::timestamp>(result) << std::endl;
    QInfo << "duration   : " << cirq.get<QResultType::duration>(result).count() << std::endl;
    QInfo << "best       : " << cirq.get<QResultType::best>(result) << std::endl;
    QInfo << "histogram  : " << cirq.get<QResultType::histogram>(result) << std::endl;
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif
  
#ifdef LIBKET_WITH_CQASM
  QInfo << "\n\n\n===== cQASM =====\n\n\n";
  QDevice<QDeviceType::qi_26_simulator, nqubits> qi;
  QInfo << qi(expr) << std::endl;
  
  try {
    result = qi.eval();

    QInfo << "job ID     : " << qi.get<QResultType::id>(result) << std::endl;
    QInfo << "time stamp : " << qi.get<QResultType::timestamp>(result) << std::endl;
    QInfo << "duration   : " << qi.get<QResultType::duration>(result).count() << std::endl;
    QInfo << "best       : " << qi.get<QResultType::best>(result) << std::endl;
    QInfo << "histogram  : " << qi.get<QResultType::histogram>(result) << std::endl;
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif

#ifdef LIBKET_WITH_OPENQASM
  QInfo << "\n\n\n===== OpenQASM =====\n\n\n";
  QDevice<QDeviceType::ibmq_16_melbourne_simulator, nqubits> ibmq;
  QInfo << ibmq(expr) << std::endl;

  try {
    result = ibmq.eval();

    QInfo << "job ID     : " << ibmq.get<QResultType::id>(result) << std::endl;
    QInfo << "time stamp : " << ibmq.get<QResultType::timestamp>(result) << std::endl;
    QInfo << "duration   : " << ibmq.get<QResultType::duration>(result).count() << std::endl;
    QInfo << "best       : " << ibmq.get<QResultType::best>(result) << std::endl;
    QInfo << "histogram  : " << ibmq.get<QResultType::histogram>(result) << std::endl;
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif

#ifdef LIBKET_WITH_OPENQL
  QInfo << "\n\n\n===== OpenQL =====\n\n\n";
  QDevice<QDeviceType::openql_qx_compiler, nqubits> openql;
  QInfo << openql(expr) << std::endl;

  try {
    openql.compile("tutorial12_hook");
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif

#ifdef LIBKET_WITH_QASM
  QInfo << "\n\n\n===== QASM =====\n\n\n";
  QDevice<QDeviceType::qasm2tex_visualizer, nqubits> qasm2tex;
  QInfo << qasm2tex(expr) << std::endl;
  
  qasm2tex.to_file("tutorial12_hook");
#endif
  
#ifdef LIBKET_WITH_QUIL
  QInfo << "\n\n\n===== Quil =====\n\n\n";
  QDevice<QDeviceType::rigetti_9q_square_simulator, nqubits> rigetti;
  QInfo << rigetti(expr) << std::endl;

  try {
    result = rigetti.eval();

    QInfo << "job ID     : " << rigetti.get<QResultType::id>(result) << std::endl;
    QInfo << "time stamp : " << rigetti.get<QResultType::timestamp>(result) << std::endl;
    QInfo << "duration   : " << rigetti.get<QResultType::duration>(result).count() << std::endl;
    QInfo << "best       : " << rigetti.get<QResultType::best>(result) << std::endl;
    QInfo << "histogram  : " << rigetti.get<QResultType::histogram>(result) << std::endl;
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif

#ifdef LIBKET_WITH_QUEST
  QInfo << "\n\n\n===== QuEST =====\n\n\n";
  QDevice<QDeviceType::quest, nqubits> quest;
  QInfo << quest(expr) << std::endl;

  try {
    auto job = quest.execute(1);
    std::cout << "TIME: " << job->duration().count() << std::endl;
    QInfo << "result        : " << quest.creg() << std::endl;
    QInfo << "probabilities : " << quest.probabilities() << std::endl;
    QInfo << "outcome       : " << quest.outcome(QBitArray<nqubits>(999999)) << std::endl;
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif
  
#ifdef LIBKET_WITH_QX
  QInfo << "\n\n\n===== QX =====\n\n\n";
  QDevice<QDeviceType::qx, nqubits> qx;
  QInfo << qx(expr) << std::endl;

  try {
    auto job = qx.execute(1);
    std::cout << "TIME: " << job->duration().count() << std::endl;
    //    qx.reg().dump();
    //    qx.reset();
  } catch(const std::exception &e) { QWarn << e.what() << std::endl; }
#endif

  QDebug << "Total number of jobs run: "
         << _qstream_python.size() << " Python jobs, "
         << _qstream_cxx.size() << " C++ jobs\n";
  
  return 0;
}

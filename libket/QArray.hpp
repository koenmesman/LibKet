/** @file libket/QArray.hpp

    @brief LibKet array class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef QARRAY_HPP
#define QARRAY_HPP

#include <bitset>

#include <QBase.hpp>
#include <QUtils.hpp>

namespace LibKet {

/**
 @brief LibKet array class

 The LibKet array class represents a fixed-size array of N items of type T.
*/
template<std::size_t N, typename T, QEndianness _endian = QEndianness::lsb>
class QArray : public std::array<T, N>
{
public:
  static const constexpr QEndianness endian = _endian;

public:
  /// Default constructor
  QArray()
    : std::array<T, N>()
  {}

  /// Copy constructor
  template<QEndianness __endian>
  QArray(const QArray<N, T, __endian>& other)
    : std::array<T, N>(other)
  {
    if (_endian != __endian)
      this->reverse();
  }

  /// Move constructor
  template<QEndianness __endian>
  QArray(QArray<N, T, __endian>&& other)
    : std::array<T, N>(other)
  {
    if (_endian != __endian)
      this->reverse();
  }

  /// Constructor from string
  QArray(const std::string& value)
  {

    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned long long int
  QArray(unsigned long long int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned long int
  QArray(unsigned long int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned int
  QArray(unsigned int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned short int
  QArray(unsigned short int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned char
  QArray(unsigned char value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed long long int
  QArray(signed long long int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed long int
  QArray(signed long int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed int
  QArray(signed int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed short int
  QArray(signed short int value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed char
  QArray(signed char value)
  {
    if (_endian != QEndianness::msb)
      this->reverse();
  }

  /// Conversion operator to unsigned long long int
  operator unsigned long long int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ullong();
    } else
      return this->to_ullong();
  }

  /// Conversion operator to unsigned long int
  operator unsigned long int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to unsigned int
  operator unsigned int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to unsigned short int
  operator unsigned short int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to unsigned char
  operator unsigned char() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to signed long long int
  operator signed long long int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ullong() + 1);
      else
        return array.to_ullong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed long int
  operator signed long int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed int
  operator signed int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed short int
  operator signed short int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed char
  operator signed char() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Access operator[]
#if __cplusplus >= 201402L
  constexpr
#endif
    typename std::array<T, N>::const_reference
    operator[](std::size_t index) const
  {
    return std::array<T, N>::operator[](index);
  }

  /// Access operator[]
#if __cplusplus >= 201703L
  constexpr
#endif
    typename std::array<T, N>::reference
    operator[](std::size_t index)
  {
    return std::array<T, N>::operator[](index);
  }

  /// Comparison operator==
  bool operator==(const QArray& other)
  {
    if (endian == other.endian)
      return std::array<T, N>::operator==(other);
    else {
      QArray<N, T, _endian> temp(other);
      return std::array<T, N>::operator==(temp);
    }
  }

  /// Comparison operator!=
  bool operator!=(const QArray& other)
  {
    if (endian == other.endian)
      return std::array<T, N>::operator!=(other);
    else {
      QArray<N, T, _endian> temp(other);
      return std::array<T, N>::operator!=(temp);
    }
  }

  /// Reverse order
  QArray& reverse()
  {
    for (std::size_t i = 0; i < std::floor(this->size() / 2.0); i++) {
      T temp = std::array<T, N>::operator[](i);
      std::array<T, N>::operator[](i) =
        std::array<T, N>::operator[](this->size() - i - 1);
      std::array<T, N>::operator[](this->size() - i - 1) = temp;
    }
    return *this;
  }
};

/// Serialize operator
template<std::size_t N, typename T, QEndianness _endian>
std::ostream&
operator<<(std::ostream& os, const QArray<N, T, _endian>& array)
{
  for (std::size_t index = 0; index < N - 1; ++index)
    os << utils::to_string(array[index]) << ",";
  os << utils::to_string(array[N - 1]);
  return os;
}

/**
   @brief LibKet array class specialization for bool data

   The LibKet array class specialization for bool types represents a
   fixed-size array of N items of bool type. It represents bitsets
   that can be manipulated by standard logic operators and converted
   to and from strings and integers.
*/

template<std::size_t N, QEndianness _endian>
class QArray<N, bool, _endian> : public std::bitset<N>
{
public:
  static const constexpr QEndianness endian = _endian;

public:
  /// Default constructor
  QArray()
    : std::bitset<N>(0)
  {}

  /// Copy constructor
  template<QEndianness __endian>
  QArray(const QArray<N, bool, __endian>& other)
    : std::bitset<N>(other)
  {
    if (_endian != __endian)
      this->reverse();
  }

  /// Move constructor
  template<QEndianness __endian>
  QArray(QArray<N, bool, __endian>&& other)
    : std::bitset<N>(other)
  {
    if (_endian != __endian)
      this->reverse();
  }

  /// Constructor from string
  QArray(const std::string& value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned long long int
  QArray(unsigned long long int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned long int
  QArray(unsigned long int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned int
  QArray(unsigned int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned short int
  QArray(unsigned short int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from unsigned char
  QArray(unsigned char value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed long long int
  QArray(signed long long int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed long int
  QArray(signed long int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed int
  QArray(signed int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed short int
  QArray(signed short int value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Constructor from signed char
  QArray(signed char value)
    : std::bitset<N>(value)
  {
    if (_endian == QEndianness::msb)
      this->reverse();
  }

  /// Conversion operator to unsigned long long int
  operator unsigned long long int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ullong();
    } else
      return this->to_ullong();
  }

  /// Conversion operator to unsigned long int
  operator unsigned long int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to unsigned int
  operator unsigned int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to unsigned short int
  operator unsigned short int() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to unsigned char
  operator unsigned char() const
  {
    if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      return array.to_ulong();
    } else
      return this->to_ulong();
  }

  /// Conversion operator to signed long long int
  operator signed long long int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ullong() + 1);
      else
        return array.to_ullong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed long int
  operator signed long int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed int
  operator signed int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed short int
  operator signed short int() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Conversion operator to signed char
  operator signed char() const
  {
    if (_endian == QEndianness::lsb &&
        std::bitset<N>::operator[](this->size() - 1)) {
      QArray array(*this);
      return -(array.flip().to_ulong() + 1);
    } else if (_endian == QEndianness::msb) {
      QArray array(*this);
      array.reverse();
      if (array[this->size() - 1])
        return -(array.flip().to_ulong() + 1);
      else
        return array.to_ulong();
    }
    return this->to_ulong();
  }

  /// Access operator[]
  constexpr bool operator[](std::size_t index) const
  {
    return std::bitset<N>::operator[](index);
  }

  /// Access operator[]
  typename std::bitset<N>::reference operator[](std::size_t index)
  {
    return std::bitset<N>::operator[](index);
  }

  /// Comparison operator==
  bool operator==(const QArray& other)
  {
    if (endian == other.endian)
      return std::bitset<N>::operator==(other);
    else {
      QArray<N, bool, _endian> temp(other);
      return std::bitset<N>::operator==(temp);
    }
  }

  /// Comparison operator!=
  bool operator!=(const QArray& other)
  {
    if (endian == other.endian)
      return std::bitset<N>::operator!=(other);
    else {
      QArray<N, bool, _endian> temp(other);
      return std::bitset<N>::operator!=(temp);
    }
  }

  /// Reverse bit order
  QArray& reverse()
  {
    for (std::size_t i = 0; i < std::floor(this->size() / 2.0); i++) {
      bool temp = std::bitset<N>::operator[](i);
      std::bitset<N>::operator[](i) =
        std::bitset<N>::operator[](this->size() - i - 1);
      std::bitset<N>::operator[](this->size() - i - 1) = temp;
    }
    return *this;
  }
};

/// Serialize operator
template<std::size_t N, QEndianness _endian>
std::ostream&
operator<<(std::ostream& os, const QArray<N, bool, _endian>& bitarray)
{
  os << bitarray.to_string();
  return os;
}

template<std::size_t N, QEndianness _endian = QEndianness::lsb>
using QBitArray = QArray<N, bool, _endian>;

} // namespace LibKet

#endif // QBITARRAY_HPP

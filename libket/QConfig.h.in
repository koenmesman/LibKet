/** @file libket/QConfig.h

    @brief LibKet configuration

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QCONFIG_H
#define QCONFIG_H

#ifdef __cplusplus
#include <complex>
#endif

// Define the LibKet version number
#define LIBKET_VERSION  "@LIBKET_VERSION@"   
#define LIBKET_MAJOR    @LIBKET_MAJOR_VERSION@
#define LIBKET_MINOR    @LIBKET_MINOR_VERSION@
#define LIBKET_REVISION @LIBKET_REVISION_VERSION@

#define LIBKET_BINARY   "@PROJECT_BINARY_DIR@/"

#ifdef __cplusplus
namespace LibKet {

  /// Define index type
  using index_t = @LIBKET_INDEX_TYPE@;
  
  /// Define real type
  using real_t = @LIBKET_COEFF_TYPE@;

  /// Define complex data type  
  using complex_t = std::complex<real_t>;
}
#endif

// Enable left-to-right evaluation
#cmakedefine LIBKET_L2R_EVALUATION

// Enable generation of profiling data
#cmakedefine LIBKET_GEN_PROFILING

// Enable optimizations
#cmakedefine LIBKET_OPTIMIZE_GATES

// Enable precompiled headers
#cmakedefine LIBKET_USE_PCH

// Enable Python interpreter
#cmakedefine LIBKET_WITH_PYTHON

// Enable backends
#cmakedefine LIBKET_WITH_AQASM
#cmakedefine LIBKET_WITH_CIRQ
#cmakedefine LIBKET_WITH_CQASM
#cmakedefine LIBKET_WITH_OPENQASM
#cmakedefine LIBKET_WITH_OPENQL
#cmakedefine LIBKET_WITH_QASM
#cmakedefine LIBKET_WITH_QUEST
#cmakedefine LIBKET_WITH_QUIL
#cmakedefine LIBKET_WITH_QX

// Set path to QASM2TEX
#ifdef LIBKET_WITH_QASM
#define QASM2TEX_PY "@PROJECT_SOURCE_DIR@/external/qasm2circ/qasm2tex.py"
#else
#define QASM2TEX_PY ""
#endif

#endif // QCONFIG_H

/** @file libket/QData.hpp

    @brief LibKet quantum data classes

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDATA_HPP
#define QDATA_HPP

#include <cstdio>
#include <exception>
#include <fstream>
#include <functional>
#include <iostream>
#include <regex>
#include <sstream>
#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QUtils.hpp>

namespace LibKet {

/**
   @brief LibKet quantum data class

   The LibKet quantum data class holds the quantum data
*/
template<std::size_t _qubits, QBackendType _qbackend>
class QData;

#ifdef LIBKET_WITH_AQASM
/**
   @brief LibKet quantum data class specialization for AQASM backend

   The LibKet quantum data class implements the quantum data class
   for the AQASM backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::AQASM> : public QBase
{
public:
  /// Default constructor
  QData() = default;

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::AQASM> inline constexpr operator+(
    QData<__qubits, QBackendType::AQASM>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::AQASM>();
  }

  /// Dump QData object to string
  const std::string to_string() const noexcept { return _kernel + "END\n"; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_CIRQ
/**
   @brief LibKet quantum data class specialization for Cirq backend

   The LibKet quantum data class implements the quantum data class
   for the Cirq backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::Cirq> : public QBase
{
public:
  /// Default constructor
  QData() = default;

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::Cirq> inline constexpr operator+(
    QData<__qubits, QBackendType::Cirq>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::Cirq>();
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_CQASM
/**
   @brief LibKet quantum data class specialization for cQASM v1.0 backend

   The LibKet quantum data class implements the quantum data class
   for the cQASM v1.0 backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::cQASMv1> : public QBase
{
public:
  /// Default constructor
  QData() = default;

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::cQASMv1> inline constexpr operator+(
    QData<__qubits, QBackendType::cQASMv1>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::cQASMv1>();
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_OPENQASM
/**
   @brief LibKet quantum data class specialization for OpenQASM v2.0 backend

   The LibKet quantum data class implements the quantum data class
   for the OpenQASM v2.0 backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::OpenQASMv2> : public QBase
{
public:
  /// Default constructor
  QData() = default;

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::OpenQASMv2> inline constexpr
  operator+(QData<__qubits, QBackendType::OpenQASMv2>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::OpenQASMv2>();
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_OPENQL
/**
   @brief LibKet quantum data class specialization for OpenQL backend

   The LibKet quantum data class implements the quantum data class
   for the OpenQL backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::OpenQL> : public QBase
{
public:
  /// Default constructor
  QData(const std::string& config_file = LIBKET_BINARY
        "test_config_default.json")
    : _platform("libket_platform", config_file)
    , _kernel("libket_kernel", _platform, _qubits, _qubits)
  {
#ifndef NDEBUG
    _platform.print_info();
    ql::options::set("log_level", "LOG_DEBUG");
#else
    ql::options::set("log_level", "LOG_NOTHING");
#endif
  }

  /// Constructor: Copy from constant reference
  QData(const QData&)
    : QData()
  {}

  /// Constructor: More from universal reference
  QData(QData&&)
    : QData()
  {}

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::OpenQL> inline constexpr operator+(
    QData<__qubits, QBackendType::OpenQL>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::OpenQL>();
  }

  /// Dump QData object to string
  std::string to_string() const noexcept
  {
    return const_cast<ql::quantum_kernel&>(_kernel).qasm();
  }

  /// Append quantum gate expression to kernel
  QData& append_kernel(std::function<void()> append_gate)
  {
    append_gate();
    return *this;
  }

  /// Get constant reference to kernel
  const ql::quantum_kernel& kernel() const { return _kernel; }

  /// Get reference to kernel
  ql::quantum_kernel& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.get_circuit().clear(); }

  /// Get constant reference to platform
  const ql::quantum_platform& platform() const { return _platform; }

  /// Get reference to platform
  ql::quantum_platform& platform() { return _platform; }

protected:
  ql::quantum_platform _platform;
  ql::quantum_kernel _kernel;
};
#endif

#ifdef LIBKET_WITH_QASM
/**
   @brief LibKet quantum data class specialization for QASM backend

   The LibKet quantum data class implements the quantum data class
   for the QASM backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::QASM> : public QBase
{
public:
  /// Default constructor
  QData() = default;

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::QASM> inline constexpr operator+(
    QData<__qubits, QBackendType::QASM> other) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::QASM>();
    ;
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_QUIL
/**
   @brief LibKet quantum data class specialization for Quil backend

   The LibKet quantum data class implements the quantum data class
   for the Quil backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::Quil> : public QBase
{
public:
  /// Default constructor
  QData() = default;

  /// Constructor: Copy from constant reference
  QData(const QData&) = default;

  /// Constructor: More from universal reference
  QData(QData&&) = default;

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::Quil> inline constexpr operator+(
    QData<__qubits, QBackendType::Quil>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::Quil>();
  }

  /// Dump QData object to string
  const std::string& to_string() const noexcept { return _kernel; }

  /// Append quantum gate expression to kernel
  QData& append_kernel(const std::string& gate)
  {
    _kernel.append(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const std::string& kernel() const { return _kernel; }

  /// Get reference to kernel
  std::string& kernel() { return _kernel; }

  /// Clear kernel
  void clear() { _kernel.clear(); }

private:
  /// Kernel source code
  std::string _kernel;
};
#endif

#ifdef LIBKET_WITH_QUEST
/**
   @brief LibKet quantum data class specialization for QuEST backend

   The LibKet quantum data class implements the quantum data class
   for the QuEST simulator backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::QuEST> : public QBase
{
public:
  /// Default constructor
  QData()
    : _creg{ 0 }
  {
    _env = quest::createQuESTEnv();
    _reg = quest::createQureg(_qubits, _env);
#ifndef NDEBUG
    quest::reportQuESTEnv(_env);
    quest::reportQuregParams(_reg);
#endif
    quest::startRecordingQASM(_reg);
  }

  /// Constructor: Copy from constant reference
  QData(const QData&)
    : QData()
  {}

  /// Constructor: More from universal reference
  QData(QData&&)
    : QData()
  {}

  /// Destructor
  ~QData()
  {
    quest::stopRecordingQASM(_reg);
    quest::destroyQureg(_reg, _env);
    quest::destroyQuESTEnv(_env);
  }

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::QuEST> inline constexpr operator+(
    QData<__qubits, QBackendType::QuEST>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::QuEST>();
  }

  /// Dump QData object to string
  std::string to_string() const noexcept
  {
    // QuEST does not provide functionality to dump the circuit to a
    // string or file. Instead, it can record all actions and dump the
    // generated OpenQASM kernel code to the standard output so that
    // the output stream needs to be captured temporarily.
    std::stringstream buffer;
    std::streambuf* sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());
    quest::printRecordedQASM(_reg);
    std::cout.rdbuf(sbuf);

    quest::reportState(_reg);

    return buffer.str();
  }

  /// Clear kernel
  void clear()
  {
    quest::clearRecordedQASM(_reg);
    quest::initZeroState(_reg);
    _creg = 0;
  }

  /// Get constant reference to quantum register
  const quest::Qureg& reg() const { return _reg; }

  /// Get reference to quantum register
  quest::Qureg& reg() { return _reg; }

  /// Get constant reference to classical register
  const QBitArray<_qubits>& creg() const { return _creg; }

  /// Get reference to classical register
  QBitArray<_qubits>& creg() { return _creg; }

  /// Reset quantum register
  void reset()
  {
    quest::initZeroState(_reg);
    _reg = 0;
  }

private:
  quest::QuESTEnv _env;
  quest::Qureg _reg;
  QBitArray<_qubits> _creg;
};
#endif

#ifdef LIBKET_WITH_QX
/**
   @brief LibKet quantum data class specialization for QX backend

   The LibKet quantum data class implements the quantum data class
   for the QX simulator backend.
*/
template<std::size_t _qubits>
class QData<_qubits, QBackendType::QX> : public QBase
{
public:
  /// Default constructor
  QData()
    : _circuit(_qubits)
    , _reg(_qubits)
  {
    xpu::init();
  }

  /// Constructor: Copy from constant reference
  QData(const QData&)
    : QData()
  {}

  /// Constructor: More from universal reference
  QData(QData&&)
    : QData()
  {}

  /// Destructor
  ~QData() { xpu::clean(); }

  /// Copy assignment operator
  QData& operator=(const QData&) = default;

  /// Move assignment operator
  QData& operator=(QData&&) = default;

  /// Operator+: Concatenate two QData objects
  template<std::size_t __qubits>
  QData<_qubits + __qubits, QBackendType::QX> inline constexpr operator+(
    QData<__qubits, QBackendType::QX>) const noexcept
  {
    return QData<_qubits + __qubits, QBackendType::QX>();
  }

  /// Dump QData object to string
  std::string to_string() const noexcept
  {
    // QX, unfortunately, does not define output routines as const,
    // which makes the use of const_cast necessary. Moreover, the
    // dump() method prints its output to std::cout so that the
    // output stream needs to be captured temporarily
    std::stringstream buffer;
    std::streambuf* sbuf = std::cout.rdbuf();
    std::cout.rdbuf(buffer.rdbuf());
    const_cast<qx::circuit&>(_circuit).dump();
    std::cout.rdbuf(sbuf);

    return buffer.str();
  }

  /// Append quantum gate expression to kernel
  QData& append_kernel(qx::gate* gate)
  {
    _circuit.add(gate);
    return *this;
  }

  /// Get constant reference to kernel
  const qx::circuit& kernel() const { return _circuit; }

  /// Get reference to kernel
  qx::circuit& kernel() { return _circuit; }

  /// Clear kernel
  void clear() { _circuit.clear(); }

  /// Get constant reference to circuit
  const qx::circuit& circuit() const { return _circuit; }

  /// Get reference to circuit
  qx::circuit& circuit() { return _circuit; }

  /// Get constant reference to quantum register
  const qx::qu_register& reg() const { return _reg; }

  /// Get reference to circuit
  qx::qu_register& reg() { return _reg; }

  /// Reset quantum register
  void reset() { _reg.reset(); }

private:
  qx::circuit _circuit;
  qx::qu_register _reg;
};
#endif

/// Serialize operator
template<std::size_t _qubits, QBackendType _qbackend>
std::ostream&
operator<<(std::ostream& os, const QData<_qubits, _qbackend>& data)
{
  os << data.to_string();
  return os;
}

/**
   @brief LibKet show gate type - specialization for QData objects
*/
template<std::size_t level = 1, std::size_t _qubits, QBackendType _qbackend>
inline static auto
show(const QData<_qubits, _qbackend>& data,
     std::ostream& os = std::cout,
     const std::string& prefix = "")
{
  os << "QData\n";
  if (level > 0) {
    os << prefix << "|   qubits = " << utils::to_string(_qubits) << std::endl;
    os << prefix << "| backend = " << utils::to_string((int)_qbackend)
       << std::endl;
  }

  return data;
}

} // namespace LibKet

#endif // QDATA_HPP

/** @file libket/QDevice.hpp

    @brief LibKet quantum device classes

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_HPP
#define QDEVICE_HPP

#include <QBase.hpp>
#include <QJob.hpp>
#include <QStream.hpp>

namespace LibKet {

/**
   @brief LibKet quantum device types

   The LibKet quantum device enumerator defines the supported quantum device
   types
*/
enum class QDeviceType
{
  // clang-format off
  generic                                   = 0x00000000, /* Generic type                         */

  pyquil_visualizer                         = 0x00000001, /* PyQuil circuit visualizer            */
  qasm2tex_visualizer                       = 0x00000002, /* QASM2TEX circuit visualizer          */
  qiskit_visualizer                         = 0x00000003, /* Qiskit circuit visualizer            */

  /**
     @brief Atos Quantum Learning Machine (QLM)
  */
  atos_qlm_feynman_simulator                = 0x01000001, /* Feynman integral path simulator      */
  atos_qlm_linalg_simulator                 = 0x01000002, /* Linear algebra-based  simulator      */
  atos_qlm_stabs_simulator                  = 0x01000003, /* Stabilizer-based simulator */
  atos_qlm_mps_simulator                    = 0x01000004, /* Matrix product state-based simulator */

  /**
     @brief Quantum Inspire
  */
  qi_26_simulator                           = 0x02000001, /* Single-node simulator (26 qubits)    */
  qi_31_simulator                           = 0x02000002, /* Single-node simulator (31 qubits)    */

  qi_spin2                                  = 0x02010001, /* Spin-2 processor  (2 qubits)         */
  qi_starmon5                               = 0x02010002, /* Starmon-5 processor  (5 qubits)      */

  /**
     @brief QisKit local simulators
  */
  qiskit_qasm_simulator                     = 0x03000001, /* Universal local simulator            */
  qiskit_16_melbourne_simulator             = 0x03000002, /* 15-qubit local simulator             */
  qiskit_20_poughkeepsie_simulator          = 0x03000003, /* 20-qubit local simulator             */
  qiskit_20_tokio_simulator                 = 0x03000004, /* 20-qubit local simulator             */
  qiskit_5_yorktown_simulator               = 0x03000005, /*  5-qubit local simulator             */
  qiskit_armonk_simulator                   = 0x03000006, /*  1-qubit local simulator             */
  qiskit_bogota_simulator                   = 0x03000007, /*  5-qubit local simulator             */
  qiskit_burlington_simulator               = 0x03000008, /*  5-qubit local simulator             */
  qiskit_essex_simulator                    = 0x03000009, /*  5-qubit local simulator             */
  qiskit_london_simulator                   = 0x0300000A, /*  5-qubit local simulator             */
  qiskit_ourense_simulator                  = 0x0300000B, /*  5-qubit local simulator             */
  qiskit_rome_simulator                     = 0x0300000C, /*  5-qubit local simulator             */
  qiskit_santiago_simulator                 = 0x0300000D, /*  5-qubit local simulator             */
  qiskit_valencia_simulator                 = 0x0300000E, /*  5-qubit local simulator             */
  qiskit_vigo_simulator                     = 0x0300000F, /*  5-qubit local simulator             */
  qiskit_x2_simulator                       = 0x03000010, /*  5-qubit local simulator             */

  qiskit_statevector_simulator              = 0x03010001, /* Ideal local simulator                */
  qiskit_unitary_simulator                  = 0x03010002, /* Ideal local simulator                */
  qiskit_pulse_simulator                    = 0x03010003, /* Pulse local simulator                */

  /**
     @brief IBM Q Experience
  */
  ibmq_qasm_simulator                       = 0x04000001, /* Universal remote simulator           */
  ibmq_16_melbourne_simulator               = 0x04000002, /* 15-qubit remote simulator            */
  ibmq_20_poughkeepsie_simulator            = 0x04000003, /* 20-qubit remote simulator            */
  ibmq_20_tokio_simulator                   = 0x04000004, /* 20-qubit remote simulator            */
  ibmq_5_yorktown_simulator                 = 0x04000005, /*  5-qubit remote simulator            */
  ibmq_armonk_simulator                     = 0x04000006, /*  1-qubit remote simulator            */
  ibmq_bogota_simulator                     = 0x04000007, /*  5-qubit remote simulator            */
  ibmq_burlington_simulator                 = 0x04000008, /*  5-qubit remote simulator            */
  ibmq_essex_simulator                      = 0x04000009, /*  5-qubit remote simulator            */
  ibmq_london_simulator                     = 0x0400000A, /*  5-qubit remote simulator            */
  ibmq_ourense_simulator                    = 0x0400000B, /*  5-qubit remote simulator            */
  ibmq_rome_simulator                       = 0x0400000C, /*  5-qubit remote simulator            */
  ibmq_santiago_simulator                   = 0x0400000D, /*  5-qubit remote simulator            */  
  ibmq_valencia_simulator                   = 0x0400000E, /*  5-qubit remote simulator            */
  ibmq_vigo_simulator                       = 0x0400000F, /*  5-qubit remote simulator            */
  ibmq_x2_simulator                         = 0x04000010, /*  5-qubit remote simulator            */

  ibmq_16_melbourne                         = 0x04010002, /* 15-qubit processor                   */
  ibmq_20_poughkeepsie                      = 0x04010003, /* 20-qubit processor                   */
  ibmq_20_tokio                             = 0x04010004, /* 20-qubit processor                   */
  ibmq_5_yorktown                           = 0x04010005, /*  5-qubit processor                   */
  ibmq_armonk                               = 0x04010006, /*  1-qubit processor                   */
  ibmq_bogota                               = 0x04010007, /*  5-qubit processor                   */
  ibmq_burlington                           = 0x04010008, /*  5-qubit processor                   */
  ibmq_essex                                = 0x04010009, /*  5-qubit processor                   */
  ibmq_london                               = 0x0401000A, /*  5-qubit processor                   */
  ibmq_ourense                              = 0x0401000B, /*  5-qubit processor                   */
  ibmq_rome                                 = 0x0401000C, /*  5-qubit processor                   */
  ibmq_santiago                             = 0x0401000D, /*  5-qubit processor                   */  
  ibmq_valencia                             = 0x0401000E, /*  5-qubit processor                   */
  ibmq_vigo                                 = 0x0401000F, /*  5-qubit processor                   */
  ibmq_x2                                   = 0x04010010, /*  5-qubit processor                   */

  /**
     @brief OpenQL
  */
  openql_cc_light_compiler                  = 0x05000001, /* OpenQL compiler for CC-Light          */
  openql_cc_light17_compiler                = 0x05000002, /* OpenQL compiler for CC-Light17        */
  openql_qx_compiler                        = 0x05000003, /* OpenQL compiler for QX simulator      */

  /**
     @brief Rigetti
  */
  rigetti_aspen_8_simulator                 = 0x06000001, /* Aspen-8 simulator                    */

  rigetti_9q_square_simulator               = 0x06010001, /* 9Q-square simulator                  */
  rigetti_Xq_simulator                      = 0x06010002, /* XQ simulator with reasonable X       */

  rigetti_aspen_8                           = 0x06020001, /* Aspen-8 processor                    */

  /**
     @brief QuEST: Quantum Exact Simulation Toolkit
  */
  quest                                     = 0x07000000, /* QuEST backend                        */

  /**
     @brief QX-simulator
  */
  qx                                        = 0x08000000, /* QX backend                           */

  /**
     @brief Cirq
  */
  cirq_simulator                            = 0x09000001, /* Cirq simulator                       */
  cirq_bristlecone_simulator                = 0x09000002, /* Cirq Bristlecone simulator           */
  cirq_foxtail_simulator                    = 0x09000003, /* Cirq Foxtail simulator               */
  cirq_sycamore_simulator                   = 0x09000004, /* Cirq Sycamore simulator              */
  cirq_sycamore23_simulator                 = 0x09000005  /* Cirq Sycamore23 simulator            */
  // clang-format on
};

/**
   @brief LibKet quanntum result type

   The LibKet quantum result enumerator defines the supported quantum result
   types
*/
enum class QResultType
{
  best,      /* Best state     */
  duration,  /* Execution time */
  histogram, /* Histogram      */
  id,        /* Job ID         */
  status,    /* Job status     */
  timestamp, /* Time stamp     */
};

/**
 @namespace LibKet::device

 @brief
 The LibKet::device namespace, containing device-specific
 functionality of the LibKet projec.

 The LibKet::device namespace contains device-specific functionality
 of the LibKet project that is not exposed to the
 end-user. Functionality in this namespace can change without
 notice.
*/
namespace device {

/**
   @brief Type trait providing information about device properties
*/
template<QDeviceType>
struct QDeviceProperty;

/// Preprocessor macro for defining device properties
#define QDevicePropertyDefine(_type, _name, _qubits, _simulator, _endianness)  \
  template<>                                                                   \
  struct QDeviceProperty<_type>                                                \
  {                                                                            \
  public:                                                                      \
    static const std::string name;                                             \
    static const std::size_t qubits = _qubits;                                 \
    static const bool simulator = _simulator;                                  \
    static const QEndianness endianness = _endianness;                         \
  };                                                                           \
  const std::string QDeviceProperty<_type>::name = _name;

QDevicePropertyDefine(QDeviceType::generic,
                      "generic",
                      0,
                      true,
                      QEndianness::lsb)

} // namespace device

/// Forward declaration
template<QDeviceType _type,
         std::size_t _qubits = device::QDeviceProperty<_type>::qubits,
         bool _simulator = device::QDeviceProperty<_type>::simulator,
         enum QEndianness _endianness =
           device::QDeviceProperty<_type>::endianness>
class QDevice;

/// Dummy device
class QDevice_Dummy
{
public:
  template<typename... Args>
  QDevice_Dummy(Args... args)
  {
    throw std::runtime_error("Unsupported backend");
  }

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Dummy& operator()(const Expr& expr)
  {
    throw std::runtime_error("Unsupported backend");
    return *this;
  }

  /// Execute quantum circuit remotely on IBM Q Experience asynchronously
  QJob<QJobType::Python>* execute_async(std::size_t = 0,
                                        QStream<QJobType::Python>* = NULL)
  {
    throw std::runtime_error("Unsupported backend");
    return NULL;
  }

  /// Execute quantum circuit remotely on IBM Q Experience synchronously
  utils::json execute(std::size_t = 0, QStream<QJobType::Python>* = NULL)
  {
    return execute_async(0, NULL)->get();
  }
};

} // namespace LibKet

#include <devices/QDevice_AQLM.hpp>
#include <devices/QDevice_Cirq.hpp>
#include <devices/QDevice_IBMQ.hpp>
#include <devices/QDevice_OpenQL.hpp>
#include <devices/QDevice_QX.hpp>
#include <devices/QDevice_Qiskit.hpp>
#include <devices/QDevice_QuEST.hpp>
#include <devices/QDevice_QuantumInspire.hpp>
#include <devices/QDevice_Rigetti.hpp>

#include <devices/QDevice_visualizer.hpp>

#endif // QDEVICE_HPP

/** @file libket/QFunctor.hpp

@brief LibKet quantum functor class

@copyright This file is part of the LibKet library

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#pragma once
#ifndef QFUNCTOR_HPP
#define QFUNCTOR_HPP

#include <QUtils.hpp>

namespace LibKet {

/**
   @brief LibKet quantum functor class

   The LibKet quantum functor class can be used to inject a quantum
   expression as template argument into other classes and structures

*/
template<std::size_t _hash, bool _isExpr>
class QFunctor;

/**
   @brief LibKet quantum functor type macro

   The LibKet quantum functor type macro returns the type of the
   specialized quantum functor class for the given expression
 */

#define QFunctor_t_2(_expression, _hash)                                       \
  QFunctor<_hash, std::is_base_of<QBase, decltype(_expression)>::value>
#define QFunctor_t_1(_expression)                                              \
  QFunctor_t_2(_expression, utils::hash(#_expression))
#define QFunctor_t(...) CONCAT(QFunctor_t_, VARGS(__VA_ARGS__))(__VA_ARGS__)

/**
   @brief LibKet quantum functor class specialization macro

   The LibKet quantum functor class specialization macro creates a
   quantum functor class specialization for the given expression and
   assigns it a unique hash of the stringified expression
*/
#define QFunctor_class_2(_expression, _hash)                                   \
  template<>                                                                   \
  class QFunctor_t(_expression, _hash)                                         \
  {                                                                            \
  public:                                                                      \
    static constexpr bool isExpr =                                             \
      std::is_base_of<QBase, decltype(_expression)>::value;                    \
    static constexpr decltype(_hash) hash = _hash;                             \
    inline auto operator()() const noexcept { return _expression; }            \
  };
#define QFunctor_class_1(_expression)                                          \
  QFunctor_class_2(_expression, utils::hash(#_expression))
#define QFunctor_class(...)                                                    \
  CONCAT(QFunctor_class_, VARGS(__VA_ARGS__))(__VA_ARGS__)

/**
 *@brief LibKet quantum functor alias macro

 The LibKet quantum functor alias macro creates a quantum functor
 class specialization for the given expression, assigns it a unique
 hash of the stringified expression and creates a user-defined alias
 */
#define QFunctor_alias(_alias, ...)                                            \
  QFunctor_class(__VA_ARGS__);                                                 \
  using _alias = QFunctor_t(__VA_ARGS__);

/**
   @brief LibKet quantum functor object macro

   The LibKet quantum functor object macro instantiates a quantum
   functor type for the given expression
 */
#define QFunctor_2(_expression, _hash)                                         \
  QFunctor_t(_expression, _hash) {}
#define QFunctor_1(_expression)                                                \
  QFunctor_1(_expression, utils::hash(#_expression))
#define QFunctor(...) CONCAT(QFunctor_, VARGS(__VA_ARGS__))(__VA_ARGS__)

} // namespace LibKet

#endif // QFUNCTOR_HPP

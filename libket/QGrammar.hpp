/** @file libket/QGrammar.hpp

    @brief LibKet quantum expression grammar classes

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGRAMMAR_HPP
#define QGRAMMAR_HPP

#include <tao/pegtl.hpp>
namespace pegtl = tao::TAO_PEGTL_NAMESPACE;

namespace LibKet {

/**
   @namespace LibKet::grammar

   @brief The LibKet::grammar namespace, containing quantum
   expression grammar functionality of the LibKet project

   The LibKet::grammar namespace contains internal utility
   functionality of the LibKet project that is not exposed to the
   end-user. Functionality in this namespace can change without
   notice.
*/

namespace grammar {} // namespace grammar

} // namespace LibKet

#endif // QGRAMMAR_HPP

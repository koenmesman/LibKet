/** @file libket/QStream.hpp

    @brief LibKet quantum stream class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef QSTREAM_HPP
#define QSTREAM_HPP

#include <deque>
#include <functional>

#include <QJob.hpp>

namespace LibKet {

/**
     @brief LibKet quantum stream class

     The LibKet quantum stream class implements a stream execution unit with
     support of synchronous and asynchronous execution of jobs
  */
template<QJobType>
class QStream;

/**
   @brief LibKet quantum stream class specialization for the Python execution
   unit

   The LibKet quantum stream class specialization for the Python
   execution unit supports synchronous and asynchronous execution of
   Python jobs using the C++11 std::async feature
*/
template<>
class QStream<QJobType::Python>
{
#ifdef LIBKET_WITH_PYTHON
private:
  // Python module and global/local dictionary objects
  PyObject* _module = NULL;
  PyObject* _global = NULL;
  PyObject* _local = NULL;

  // QJob queue
  std::deque<QJob<QJobType::Python>*> _stream;

  // Global counter
  static std::size_t _counter;

  // Global Python thread state
  static PyThreadState* _state;

  // Shuts down Python interpreter gracefully
  static void shutdown()
  {
    PyEval_RestoreThread(_state);
    Py_Finalize();
  }

public:
  /// Default constructor
  QStream()
  {
    // Check if Python interpreter has been started
    if (Py_IsInitialized() == 0) {
      // Initialize the Python interpreter
      Py_Initialize();

      // Create GIL (global interpreter lock)/enable threads
      PyEval_InitThreads();

      // Release the global interpreter lock and reset the thread state to NULL,
      // returning the previous thread state (which is not NULL).
      _state = PyEval_SaveThread();

      // Register Py_Finalize to be performed on exit
      // if (std::atexit(shutdown) != 0)
      //  throw std::runtime_error("An error occured: Py_Finalize could not be
      //  registered for being called on exit!");
    }

    // Ensure that the current thread is ready to call the Python C API
    // regardless of the current state of Python, or of the global interpreter
    // lock.
    PyGILState_STATE gilState;
    gilState = PyGILState_Ensure();

    // Create global dictionary with builtins
    _global = PyDict_New();
    PyDict_SetItemString(_global, "__builtins__", PyEval_GetBuiltins());

    // Create a new module object
    std::string module = "libket" + std::to_string(_counter++);
    _module = PyModule_New(module.c_str());
    PyModule_AddStringConstant(_module, "__file__", "");

    // Get the local dictionary object
    _local = PyModule_GetDict(_module);

    // Release any resources previously acquired
    PyGILState_Release(gilState);
  }

  /// Destructor
  ~QStream()
  {
    while (!_stream.empty()) {
      delete _stream.front();
      _stream.pop_front();
    }

    // Ensure that the current thread is ready to call the Python C API
    // regardless of the current state of Python, or of the global interpreter
    // lock.
    PyGILState_STATE gilState;
    gilState = PyGILState_Ensure();

    // Decrease references
    Py_DECREF(_local);
    Py_DECREF(_global);
    Py_DECREF(_module);

    // Release any resources previously acquired
    PyGILState_Release(gilState);
  }

  /// Creates a new quantum job and appends it to the current stream
  QJob<QJobType::Python>* run(const std::string& py_script,
                              const std::string& py_method_run = "run",
                              const std::string& py_method_wait = "wait",
                              const std::string& py_method_query = "query")
  {
    QJob<QJobType::Python>* qjob;

    if (_stream.empty())
      qjob = new QJob<QJobType::Python>(py_script,
                                        py_method_run,
                                        py_method_wait,
                                        py_method_query,
                                        _module,
                                        _global,
                                        _local,
                                        NULL);
    else
      qjob = new QJob<QJobType::Python>(py_script,
                                        py_method_run,
                                        py_method_wait,
                                        py_method_query,
                                        _module,
                                        _global,
                                        _local,
                                        _stream.back());

    _stream.push_back(qjob);
    return qjob;
  }

  /// Returns the number of quantum jobs in the stream
  std::size_t size() const { return _stream.size(); }

  /// Waits until all quantum jobs in the current stream have
  /// completed
  const QStream* wait() const
  {
    _stream.back()->wait();
    return this;
  }

  /// Returns true if all quantum jobs in the current stream have
  /// completed, or false if not
  bool query() const { return _stream.back()->query(); }
#else
  /// Default constructor
  QStream()
  {
    throw std::runtime_error("This feature requires -DLIBKET_WITH_PYTHON");
  }

  /// Creates a new quantum job and appends it to the current stream
  QJob<QJobType::Python>* run(const std::string& py_script,
                              const std::string& py_method = "run",
                              const std::string& py_method_wait = "wait",
                              const std::string& py_method_query = "query")
  {
    throw std::runtime_eception("This feature requires -DLIBKET_WITH_PYTHON");

    return nullptr;
  }

  /// Returns the number of jobs in the stream
  std::size_t size() const
  {
    throw std::runtime_error("This feature requires -DLIBKET_WITH_PYTHON");

    return 0;
  }

  /// Waits until all quantum jobs in the current stream have
  /// completed
  void wail() const
  {
    throw std::runtime_error("This feature requires -DLIBKET_WITH_PYTHON");
  }

  /// Returns true if all operations in the current stream have
  /// completed, or false if not
  bool query() const
  {
    throw std::runtime_error("This feature requires -DLIBKET_WITH_PYTHON");

    return false;
  }
#endif // LIBKET_WITH_PYTHON

  // Returns iterator to the beginning
  std::deque<QJob<QJobType::Python>*>::iterator begin() noexcept
  {
    return _stream.begin();
  }

  // Returns constant iterator to the beginning
  std::deque<QJob<QJobType::Python>*>::const_iterator begin() const noexcept
  {
    return _stream.begin();
  }

  // Returns iterator to the end
  std::deque<QJob<QJobType::Python>*>::iterator end() noexcept
  {
    return _stream.end();
  }

  // Returns constant iterator to the end
  std::deque<QJob<QJobType::Python>*>::const_iterator end() const noexcept
  {
    return _stream.end();
  }

  // Returns constant iterator to the beginning
  std::deque<QJob<QJobType::Python>*>::const_iterator cbegin() const noexcept
  {
    return _stream.cbegin();
  }

  // Returns constant iterator to the end
  std::deque<QJob<QJobType::Python>*>::const_iterator cend() const noexcept
  {
    return _stream.cend();
  }

  // Returns reverse iterator to the beginning
  std::deque<QJob<QJobType::Python>*>::reverse_iterator rbegin() noexcept
  {
    return _stream.rbegin();
  }

  // Returns constant reverse iterator to the beginning
  std::deque<QJob<QJobType::Python>*>::const_reverse_iterator rbegin() const noexcept
  {
    return _stream.rbegin();
  }

  // Returns reverse iterator to the end
  std::deque<QJob<QJobType::Python>*>::reverse_iterator rend() noexcept
  {
    return _stream.rend();
  }

  // Returns constant reverse iterator to the end
  std::deque<QJob<QJobType::Python>*>::const_reverse_iterator rend() const noexcept
  {
    return _stream.rend();
  }

  // Returns constant reverse iterator to the beginning
  std::deque<QJob<QJobType::Python>*>::const_reverse_iterator crbegin() const noexcept
  {
    return _stream.crbegin();
  }

  // Returns constant reverse iterator to the end
  std::deque<QJob<QJobType::Python>*>::const_reverse_iterator crend() const noexcept
  {
    return _stream.crend();
  }
};

// Initialization of static class members
std::size_t QStream<QJobType::Python>::_counter = 0;
PyThreadState* QStream<QJobType::Python>::_state = NULL;

/**
   @brief LibKet quantum stream class specialization for the C++ execution unit

   The LibKet quantum stream class specialization for the Python
   execution unit supports synchronous and asynchronous execution of
   Python jobs using the C++11 std::async feature
*/
template<>
class QStream<QJobType::CXX>
{
private:
  // QJob queue
  std::deque<QJob<QJobType::CXX>*> _stream;

public:
  /// Default constructor
  QStream() {}

  /// Destructor
  ~QStream()
  {
    while (!_stream.empty()) {
      delete _stream.front();
      _stream.pop_front();
    }
  }

  /// Creates a new quantum job and appends it to the current stream
  QJob<QJobType::CXX>* run(std::function<void(void)> exec)
  {
    QJob<QJobType::CXX>* qjob;

    if (_stream.empty())
      qjob = new QJob<QJobType::CXX>(exec);
    else
      qjob = new QJob<QJobType::CXX>(exec, _stream.back());

    _stream.push_back(qjob);
    return qjob;
  }

  /// Returns the number of quantum jobs in the stream
  std::size_t size() const { return _stream.size(); }

  /// Waits until all quantum jobs in the current stream have
  /// completed
  const QStream* wait() const
  {
    _stream.back()->wait();
    return this;
  }

  /// Returns true if all quantum jobs in the current stream have
  /// completed, or false if not
  bool query() const { return _stream.back()->query(); }

  /// Returns the duration of all quantum jobs in the current stream
  template<class Rep = double, class Period = std::ratio<1>>
  const std::chrono::duration<Rep, Period> duration() const noexcept
  {
    std::chrono::duration<Rep, Period> _duration;

    for (auto it = this->cbegin(); it != this->cbegin(); it++)
      _duration += (*it)->duration();

    return _duration;
  }

  // Returns iterator to the beginning
  std::deque<QJob<QJobType::CXX>*>::iterator begin() noexcept
  {
    return _stream.begin();
  }

  // Returns constant iterator to the beginning
  std::deque<QJob<QJobType::CXX>*>::const_iterator begin() const noexcept
  {
    return _stream.begin();
  }

  // Returns iterator to the end
  std::deque<QJob<QJobType::CXX>*>::iterator end() noexcept
  {
    return _stream.end();
  }

  // Returns constant iterator to the end
  std::deque<QJob<QJobType::CXX>*>::const_iterator end() const noexcept
  {
    return _stream.end();
  }

  // Returns constant iterator to the beginning
  std::deque<QJob<QJobType::CXX>*>::const_iterator cbegin() const noexcept
  {
    return _stream.cbegin();
  }

  // Returns constant iterator to the end
  std::deque<QJob<QJobType::CXX>*>::const_iterator cend() const noexcept
  {
    return _stream.cend();
  }

  // Returns reverse iterator to the beginning
  std::deque<QJob<QJobType::CXX>*>::reverse_iterator rbegin() noexcept
  {
    return _stream.rbegin();
  }

  // Returns constant reverse iterator to the beginning
  std::deque<QJob<QJobType::CXX>*>::const_reverse_iterator rbegin() const noexcept
  {
    return _stream.rbegin();
  }

  // Returns reverse iterator to the end
  std::deque<QJob<QJobType::CXX>*>::reverse_iterator rend() noexcept
  {
    return _stream.rend();
  }

  // Returns constant reverse iterator to the end
  std::deque<QJob<QJobType::CXX>*>::const_reverse_iterator rend() const noexcept
  {
    return _stream.rend();
  }

  // Returns constant reverse iterator to the beginning
  std::deque<QJob<QJobType::CXX>*>::const_reverse_iterator crbegin() const noexcept
  {
    return _stream.crbegin();
  }

  // Returns constant reverse iterator to the end
  std::deque<QJob<QJobType::CXX>*>::const_reverse_iterator crend() const noexcept
  {
    return _stream.crend();
  }
};

/**
  @brief LibKet default quantum streams

  The default quantum streams are used whenever quantum jobs are not sceduled to
  a dedicated quantum stream object. The default quantum streams might be
  compared with the default streams in NVIDIA's CUDA SDK.
  */
static QStream<QJobType::Python> _qstream_python;
static QStream<QJobType::CXX> _qstream_cxx;

} // namespace LibKet

#endif // QSTREAM_HPP

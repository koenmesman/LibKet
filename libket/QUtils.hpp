/** @file libket/QUtils.hpp

    @brief LibKet utility classes

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QUTILS_HPP
#define QUTILS_HPP

#include <sys/stat.h>
#include <sys/types.h>

#include <array>
#include <complex>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <sstream>
#include <tuple>
#include <type_traits>
#include <utility>

#if defined _WIN32
#include <ShlObj.h>
#include <direct.h>
#include <windows.h>
#else
#include <dirent.h>
#include <dlfcn.h>
#include <pwd.h>
#include <unistd.h>
#endif

#include <QConfig.h>

namespace LibKet {

namespace {

const std::string&
_getValidPathSeparators()
{
#if defined _WIN32 || defined __CYGWIN__
  static const std::string ps("\\/");
#else
  static const std::string ps("/");
#endif
  return ps;
}

char
_getNativePathSeparator()
{
  return _getValidPathSeparators()[0];
}

// sets last character to the native path seperator
// special case "" gets "./"
void
_makePath(std::string& final_result)
{
  if (final_result.length() == 0)
    final_result.push_back('.');
  if (final_result[final_result.length() - 1] != _getNativePathSeparator())
    final_result.push_back(_getNativePathSeparator());
}
} // anonymous namespace

/**
 @namespace LibKet::utils

 @brief
 The LibKet::utils namespace, containing utility functionality of the LibKet
 project

 The LibKet::utils namespace contains internal utility
 functionality of the LibKet project that is not exposed to the
 end-user. Functionality in this namespace can change without
 notice.
*/

namespace utils {

// Make JSON available
using json = nlohmann::json;

// This is the type which holds sequences
template<std::size_t... Ns>
struct sequence
{
  sequence()
    : _sequence{ Ns... }
  {}

  // Returns iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::iterator begin() noexcept
  {
    return _sequence.begin();
  }

  // Returns constant iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_iterator begin() const noexcept
  {
    return _sequence.begin();
  }

  // Returns iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::iterator end() noexcept
  {
    return _sequence.end();
  }

  // Returns constant iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_iterator end() const noexcept
  {
    return _sequence.end();
  }

  // Returns constant iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_iterator cbegin() const noexcept
  {
    return _sequence.cbegin();
  }

  // Returns constant iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_iterator cend() const noexcept
  {
    return _sequence.cend();
  }

  // Returns reverse iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::reverse_iterator rbegin() noexcept
  {
    return _sequence.rbegin();
  }

  // Returns constant reverse iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator rbegin() const noexcept
  {
    return _sequence.rbegin();
  }

  // Returns reverse iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::reverse_iterator rend() noexcept
  {
    return _sequence.rend();
  }

  // Returns constant reverse iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator rend() const noexcept
  {
    return _sequence.rend();
  }

  // Returns constant reverse iterator to the beginning
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator crbegin() const noexcept
  {
    return _sequence.crbegin();
  }

  // Returns constant reverse iterator to the end
#if __cplusplus >= 201402L
  constexpr
#endif
  typename std::array<std::size_t, sizeof...(Ns)>::const_reverse_iterator crend() const noexcept
  {
    return _sequence.crend();
  }

private:
  std::array<std::size_t, sizeof...(Ns)> _sequence;
};

// First define the template signature
template<std::size_t... Ns>
struct seq_gen;

// Recursion case
template<std::size_t I0, std::size_t I, std::size_t... Ns>
struct seq_gen<I0, I, Ns...>
{
  // Take front most number of sequence,
  // decrement it, and prepend it twice.
  // First I - 1 goes into the counter,
  // Second I - 1 goes into the sequence.
  using type = typename seq_gen<I0, I - 1, I - 1, Ns...>::type;
};

// Recursion abort
template<std::size_t I0, std::size_t... Ns>
struct seq_gen<I0, I0, Ns...>
{
  using type = sequence<Ns...>;
};

template<std::size_t N, std::size_t M = 0>
using sequence_t = typename seq_gen<M >= N ? N : 0, M >= N ? M + 1 : N>::type;

template<std::size_t N, std::size_t _N, std::size_t... Ns>
std::ostream&
operator<<(std::ostream& os, const sequence<N, _N, Ns...>&) noexcept
{
  os << N << ", " << sequence<_N, Ns...>();
  return os;
}

template<std::size_t N>
std::ostream&
operator<<(std::ostream& os, const sequence<N>&) noexcept
{
  os << N << "\n";
  return os;
}

/**
   @brief Compile-time variant of std::forward_as_tuple

   The std::forward_as_tuple function cannot be used within constant
   expressions. This variant can be used within constant expressions.
  */
template<typename... Ts>
constexpr std::tuple<Ts...>
forward_as_tuple(Ts&&... ts) noexcept
{
  return std::tuple<Ts...>{ std::forward<Ts>(ts)... };
}

/**
@namespace LibKet::utils::detail

@brief
The LibKet::utils::detail namespace, containing internal
implementation details of the utility functionality of the
LibKet project

The LibKet::utils::detail namespace contains internal
implementation details of the utility functionality of the
LibKet project that are not exposed to the end-user.
Functionality in this namespace can change without notice.
*/
namespace detail {

/**
@brief
Converts strings into types

@note
This is a simplified version of the 'typestring' header file
developed by George Makrydakis <george@irrequietus.eu>.

The header of the original 'typestring' header file reads as follows:

@copyright Copyright (C) 2015, 2016 George Makrydakis
<george@irrequietus.eu>

The 'typestring' header is a single header C++ library for
creating types to use as type parameters in template
instantiations, repository available at
https://github.com/irrequietus/typestring. Conceptually
stemming from own implementation of the same thing (but in a
more complicated manner to be revised) in 'clause':
https://github.com/irrequietus/clause.

File subject to the terms and conditions of the Mozilla
Public License v 2.0. If a copy of the MPLv2 license text
was not distributed with this file, you can obtain it at:
http://mozilla.org/MPL/2.0/.
*/
template<int N, int M>
constexpr char
tygrab(char const (&c)[M]) noexcept
{
  return c[N < M ? N : M - 1];
}

/**
   @brief Compile-time for-loop (implementation)
*/
template<index_t for_start,
         index_t for_end,
         index_t for_step,
         index_t for_index,
         template<index_t start, index_t end, index_t step, index_t index>
         class functor,
         typename functor_return_type,
         typename... functor_types>
struct static_for_impl
{
  // Terminal
  template<bool is_terminal = ((for_step > 0) ? (for_index > for_end)
                                              : (for_index < for_end)),
           typename std::enable_if<is_terminal>::type* = nullptr>
  inline auto operator()(functor_return_type&& functor_return_arg,
                         functor_types&&... functor_args)
  {
#ifdef LIBKET_GEN_PROFILING
    QInfo << "static_for:" << std::to_string(for_start) << ","
          << std::to_string(for_end) << "," << std::to_string(for_step)
          << std::endl;
#endif

    return std::forward<functor_return_type>(functor_return_arg);
  }

  // Recursion
  template<bool is_terminal = ((for_step > 0) ? (for_index > for_end)
                                              : (for_index < for_end)),
           typename std::enable_if<!is_terminal>::type* = nullptr>
  inline auto operator()(functor_return_type&& functor_return_arg,
                         functor_types&&... functor_args)
  {
    auto arg = functor<for_start, for_end, for_step, for_index>()(
      std::forward<functor_return_type>(functor_return_arg),
      std::forward<functor_types>(functor_args)...);
    return static_for_impl<for_start,
                           for_end,
                           for_step,
                           for_index + for_step,
                           functor,
                           decltype(arg),
                           functor_types...>()(
      std::forward<decltype(arg)>(arg),
      std::forward<functor_types>(functor_args)...);
  }
};

#ifndef DOXYGEN
#include <specializations/static_for.hpp>
#endif

} // namespace detail

/**
   @brief Compile-time for-loop
*/
template<index_t for_start,
         index_t for_end,
         index_t for_step,
         template<index_t start, index_t end, index_t step, index_t index>
         class functor,
         typename functor_return_type,
         typename... functor_types>
inline auto
static_for(functor_return_type&& functor_return_arg,
           functor_types&&... functor_args)
{
  return detail::static_for_impl<for_start,
                                 for_end,
                                 for_step,
                                 for_start,
                                 functor,
                                 functor_return_type,
                                 functor_types...>()(
    std::forward<functor_return_type>(functor_return_arg),
    std::forward<functor_types>(functor_args)...);
}

/**
@brief
Compile-time constant string type

The compile-time constant string type makes it possible to
encode objects of type std::string into types so that the
content of the string can be extracted from the type and does
not need an instantiated object.

The string_t type is be used to store, e.g., parameter values
of quantum gates in the LibKet::QGate classes
*/
template<char... Cs>
struct string_t final
{
private:
  /// Capture leading character
  template<char C_, char... Cs_>
  struct lchar_t
  {
    static constexpr char value = C_;
  };

public:
  /// Default constructor
  constexpr string_t() = default;

  /// Returns value as string
  inline static std::string to_string()
  {
    std::string val;
    int unpack[]{ 0, (val += Cs, 0)... };
    static_cast<void>(unpack);

    // Remove trailing '\x00' symbols
    val.erase(std::remove(val.begin(), val.end(), '\x00'), val.end());

    return val;
  }

  /// Returns type as string
  inline static std::string to_type()
  {
    if (sizeof...(Cs) <= 1)
      return "QConst1_t(" + to_string() + ")";
    else if (sizeof...(Cs) <= 2)
      return "QConst2_t(" + to_string() + ")";
    else if (sizeof...(Cs) <= 4)
      return "QConst4_t(" + to_string() + ")";
    else if (sizeof...(Cs) <= 8)
      return "QConst8_t(" + to_string() + ")";
    else if (sizeof...(Cs) <= 16)
      return "QConst16_t(" + to_string() + ")";
    else if (sizeof...(Cs) <= 32)
      return "QConst32_t(" + to_string() + ")";
    else if (sizeof...(Cs) <= 64)
      return "QConst64_t(" + to_string() + ")";
    else if (sizeof...(Cs) <= 128)
      return "QConst128_t(" + to_string() + ")";
    else
      return "QConst256_t(" + to_string() + ")";
  }

  /// Returns object as string
  inline static std::string to_obj()
  {
    if (sizeof...(Cs) <= 1)
      return "QConst1(" + to_string() + ")";
    else if (sizeof...(Cs) <= 2)
      return "QConst2(" + to_string() + ")";
    else if (sizeof...(Cs) <= 4)
      return "QConst4(" + to_string() + ")";
    else if (sizeof...(Cs) <= 8)
      return "QConst8(" + to_string() + ")";
    else if (sizeof...(Cs) <= 16)
      return "QConst16(" + to_string() + ")";
    else if (sizeof...(Cs) <= 32)
      return "QConst32(" + to_string() + ")";
    else if (sizeof...(Cs) <= 64)
      return "QConst64(" + to_string() + ")";
    else if (sizeof...(Cs) <= 128)
      return "QConst128(" + to_string() + ")";
    else
      return "QConst256(" + to_string() + ")";
  }

  /// Returns value as float
  inline static float to_float() { return std::stof(to_string()); }

  /// Returns value as double
  inline static float to_double() { return std::stod(to_string()); }

  /// Returns value as long double
  inline static float to_long_double() { return std::stold(to_string()); }

  /// Returns value as real-valued number
  inline static real_t value()
  {
    return (std::is_same<real_t, float>::value
              ? std::stof(to_string())
              : (std::is_same<real_t, double>::value
                   ? std::stod(to_string())
                   : std::stold(to_string())));
  }
};

namespace detail {
// Strip last character in string_t type
template<char... Cs>
struct strip_string_t;

template<char C>
struct strip_string_t<C>
{
  using type = string_t<>;
};

template<typename, typename>
struct concat_string_t
{};

template<char... Cs, char... Ds>
struct concat_string_t<string_t<Cs...>, string_t<Ds...>>
{
  using type = string_t<Cs..., Ds...>;
};

template<char C, char... Cs>
struct strip_string_t<C, Cs...>
{
  using type =
    typename concat_string_t<string_t<C>,
                             typename strip_string_t<Cs...>::type>::type;
};

} // namespace detail

/// Operator+()
template<char C, char... Cs>
inline static auto
operator+(const string_t<C, Cs...>&)
{
  return string_t<C, Cs...>{};
}

/// Operator-()
template<char C, char... Cs>
inline static auto
operator-(const string_t<C, Cs...>&)
{
  return typename detail::strip_string_t<'-', C, Cs...>::type{};
}

/// Operator-()
template<char... Cs>
inline static auto
operator-(const string_t<'-', Cs...>&)
{
  return string_t<Cs..., '\000'>{};
}

/// Operator==()
template<char... Cs, char... Ds>
inline static bool
operator==(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() == string_t<Ds...>::value());
}

/// Operator!=()
template<char... Cs, char... Ds>
inline static bool
operator!=(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() != string_t<Ds...>::value());
}

/// Operator<=()
template<char... Cs, char... Ds>
inline static bool
operator<=(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() <= string_t<Ds...>::value());
}

/// Operator>=()
template<char... Cs, char... Ds>
inline static bool
operator>=(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() >= string_t<Ds...>::value());
}

/// Operator<()
template<char... Cs, char... Ds>
inline static bool
operator<(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() < string_t<Ds...>::value());
}

/// Operator>()
template<char... Cs, char... Ds>
inline static bool
operator>(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (string_t<Cs...>::value() > string_t<Ds...>::value());
}

/// Tolerance function: true of abs(first argument) > abs(second argument)
template<char... Cs, char... Ds>
inline static bool
tolerance(const string_t<Cs...>&, const string_t<Ds...>&)
{
  return (std::abs(string_t<Cs...>::value()) >
          std::abs(string_t<Ds...>::value()));
}

/// Tolerance function: true of abs(value) > abs(second argument)
template<char... Ds>
inline static bool
tolerance(const real_t& value, const string_t<Ds...>&)
{
  return (std::abs(value) > std::abs(string_t<Ds...>::value()));
}

template<char... Cs>
std::ostream&
operator<<(std::ostream& os, const string_t<Cs...>& obj)
{
  os << obj.to_obj();
  return os;
}

template<typename T>
std::string
to_string(const T& obj)
{
  return std::to_string(obj);
}

template<>
std::string
to_string(const float& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<float>::digits10)
     << obj;
  return ss.str();
}

template<>
std::string
to_string(const double& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<double>::digits10)
     << obj;
  return ss.str();
}

template<>
std::string
to_string(const long double& obj)
{
  std::stringstream ss;
  ss << std::fixed
     << std::setprecision(std::numeric_limits<long double>::digits10) << obj;
  return ss.str();
}

template<>
std::string
to_string(const std::complex<float>& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<float>::digits10)
     << "(" << obj.real() << "," << obj.imag() << ")";
  return ss.str();
}

template<>
std::string
to_string(const std::complex<double>& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<double>::digits10)
     << "(" << obj.real() << "," << obj.imag() << ")";
  return ss.str();
}

template<>
std::string
to_string(const std::complex<long double>& obj)
{
  std::stringstream ss;
  ss << std::fixed
     << std::setprecision(std::numeric_limits<long double>::digits10) << "("
     << obj.real() << "," << obj.imag() << ")";
  return ss.str();
}

#ifdef LIBKET_WITH_QUEST
template<>
std::string
to_string(const quest::Complex& obj)
{
  std::stringstream ss;
  ss << std::fixed << std::setprecision(std::numeric_limits<qreal>::digits10)
     << "(" << obj.real << "," << obj.imag << ")";
  return ss.str();
}
#endif

/**
   @brief: Auto-detect temp directory

   @note This is a simplified version of the getTempPath function from
   the 'gsFileManager.h' header file taken from the G+Smo library

   @copyright Copyright (C) Stefan Takacs and Angelos Mantzaflaris
 */
std::string
getTempPath()
{
#if defined(_WIN32)
  TCHAR _temp[MAX_PATH];
  (void)GetTempPath(MAX_PATH, // length of the buffer
                    _temp);   // buffer for path
  return std::string(_temp);
#else

  // Typically, we should consider TMPDIR
  //   http://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap08.html#tag_08_03
  //   https://en.wikipedia.org/wiki/TMPDIR&oldid=728654758
  char* _temp = std::getenv("TMPDIR");
  // getenv returns NULL ptr if the variable is unknown
  // (http://en.cppreference.com/w/cpp/utility/program/getenv). If it is an
  // empty string, we should also exclude it.
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    _makePath(_path);
    return _path;
  }

  // Okey, if first choice did not work, try this:
  _temp = std::getenv("TEMP");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    _makePath(_path);
    return _path;
  }

  // And as third choice, use just current directory
  // http://man7.org/linux/man-pages/man2/getcwd.2.html
  _temp = getcwd(NULL, 0);
  if (NULL == _temp)
    throw std::runtime_error("getcwd returned NULL.");
  std::string _path(_temp);
  // The string is allocated using malloc, see the reference above
  std::free(_temp);
  _makePath(_path);
  return _path;
#endif
}

/**
   @brief: Auto-detect LibKet directory

   @note This is a simplified version of the getTempPath function from
   the 'gsFileManager.h' header file taken from the G+Smo library

   @copyright Copyright (C) Stefan Takacs and Angelos Mantzaflaris
 */
std::string
getLibKetPath()
{
  // Check if LIBKET_DIR is defined
  char* _temp = std::getenv("LIBKET_DIR");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    _makePath(_path);
    return _path;
  }

#if defined(_WIN32)
  // Okey, if first choice did not work, try this:
  _temp = std::getenv("USERPROFILE");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    _path.append("\\.libket");
    _makePath(_path);
    return _path;
  }

  // Okey, if second coice did not work, try this:
  char* _drive = std::getenv("HOMEDRIVE");
  char* _home = std::getenv("HOMEPATH");
  if (_drive != NULL && _drive[0] != '\0' && _home != NULL &&
      _home[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_drive);
    _path.append(std::string(_home));
    _path.append("\\.libket");
    _makePath(_path);
    return _path;
  }
#else
  // Okey, if first choice did not work, try this:
  _temp = std::getenv("HOME");
  if (_temp != NULL && _temp[0] != '\0') {
    // note: env variable needs no free
    std::string _path(_temp);
    std::cout << _path << std::endl;
    _path.append("/.libket");
    _makePath(_path);
    return _path;
  }
#endif

  // And as last choice, use just current directory
  // http://man7.org/linux/man-pages/man2/getcwd.2.html
  _temp = getcwd(NULL, 0);
  if (NULL == _temp)
    throw std::runtime_error("getcwd returned NULL.");
  std::string _path(_temp);
  // The string is allocated using malloc, see the reference above
  std::free(_temp);
  _makePath(_path);
  return _path;
}

/**
   @brief Check if directory exists
*/
int
dirExists(const char* path)
{
  struct stat info;

  if (stat(path, &info) != 0)
    return 0; // does not exist
  else if (info.st_mode & S_IFDIR)
    return 1; // is directory
  else
    return 2; // is file
}

/**
   @brief Compile-time hash function
*/
constexpr inline unsigned long
hash(const char* str)
{
  unsigned long hash = 5381;
  int c = 0;
  while ((c = *str++))
    hash = ((hash << 5) + hash) + c;
  // same as: hash = hash * 33 + c ;
  // but faster
  return hash;
}

template<typename T, T v>
struct const_expr_value
{
  static constexpr const T value = v;
};

// remove_reference + remove_cv
template<typename T>
struct remove_cvref
{
  using type =
    typename std::remove_cv<typename std::remove_reference<T>::type>::type;
};

//// literal_char_caster, literal_string_caster

// template class to replace partial function specialization and avoid overload
// over different return types
template<typename CharT>
struct literal_char_caster;

template<>
struct literal_char_caster<char>
{
  inline static constexpr char cast_from(char ach,
                                         wchar_t wch,
                                         char16_t char16ch,
                                         char32_t char32ch)
  {
    return ach;
  }
};

template<>
struct literal_char_caster<wchar_t>
{
  inline static constexpr wchar_t cast_from(char ach,
                                            wchar_t wch,
                                            char16_t char16ch,
                                            char32_t char32ch)
  {
    return wch;
  }
};

template<>
struct literal_char_caster<char16_t>
{
  inline static constexpr char16_t cast_from(char ach,
                                             wchar_t wch,
                                             char16_t char16ch,
                                             char32_t char32ch)
  {
    return char16ch;
  }
};

template<>
struct literal_char_caster<char32_t>
{
  inline static constexpr char32_t cast_from(char ach,
                                             wchar_t wch,
                                             char16_t char16ch,
                                             char32_t char32ch)
  {
    return char32ch;
  }
};

inline static std::string
string_ident(const std::string& str, const std::string& identer)
{
  return str;
}

namespace detail {

template <typename... T>
class zip_helper {
 public:
  class iterator
      : std::iterator<std::forward_iterator_tag,
                      std::tuple<decltype(*std::declval<T>().begin())...>> {
   private:
    std::tuple<decltype(std::declval<T>().begin())...> iters_;

    template <std::size_t... I>
    auto deref(std::index_sequence<I...>) const {
      return typename iterator::value_type{*std::get<I>(iters_)...};
    }

    template <std::size_t... I>
    void increment(std::index_sequence<I...>) {
      auto l = {(++std::get<I>(iters_), 0)...};
    }

   public:
    explicit iterator(decltype(iters_) iters) : iters_{std::move(iters)} {}

    iterator& operator++() {
      increment(std::index_sequence_for<T...>{});
      return *this;
    }

    iterator operator++(int) {
      auto saved{*this};
      increment(std::index_sequence_for<T...>{});
      return saved;
    }

    bool operator!=(const iterator& other) const {
      return iters_ != other.iters_;
    }

    auto operator*() const { return deref(std::index_sequence_for<T...>{}); }
  };

  zip_helper(T&... seqs)
      : begin_{std::make_tuple(seqs.begin()...)},
        end_{std::make_tuple(seqs.end()...)}
  {}

  iterator begin() const { return begin_; }
  iterator end() const { return end_; }

 private:
  iterator begin_;
  iterator end_;
};
  
} // namespace detail

// Sequences must be the same length.
template <typename... T>
auto zip(T&&... seqs) {
  return detail::zip_helper<T...>{seqs...};
}

} // namespace utils

} // namespace LibKet

#define TYPESTRING1(n, x) LibKet::utils::detail::tygrab<0x##n##0>(x)

#define TYPESTRING2(n, x)                                                      \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x)

#define TYPESTRING4(n, x)                                                      \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##2>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##3>(x)

#define TYPESTRING8(n, x)                                                      \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##2>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##3>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##4>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##5>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##6>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##7>(x)

#define TYPESTRING16(n, x)                                                     \
  LibKet::utils::detail::tygrab<0x##n##0>(x),                                  \
    LibKet::utils::detail::tygrab<0x##n##1>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##2>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##3>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##4>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##5>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##6>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##7>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##8>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##9>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##A>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##B>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##C>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##D>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##E>(x),                                \
    LibKet::utils::detail::tygrab<0x##n##F>(x)

#define QConst1_t(value) LibKet::utils::string_t<TYPESTRING1(0, #value)>
#define QConst2_t(value) LibKet::utils::string_t<TYPESTRING2(0, #value)>
#define QConst4_t(value) LibKet::utils::string_t<TYPESTRING4(0, #value)>
#define QConst8_t(value) LibKet::utils::string_t<TYPESTRING8(0, #value)>
#define QConst16_t(value) LibKet::utils::string_t<TYPESTRING16(0, #value)>
#define QConst32_t(value)                                                      \
  LibKet::utils::string_t<TYPESTRING16(0, #value), TYPESTRING16(1, #value)>
#define QConst64_t(value)                                                      \
  LibKet::utils::string_t<TYPESTRING16(0, #value),                             \
                          TYPESTRING16(1, #value),                             \
                          TYPESTRING16(2, #value),                             \
                          TYPESTRING16(3, #value)>
#define QConst128_t(value)                                                     \
  LibKet::utils::string_t<TYPESTRING16(0, #value),                             \
                          TYPESTRING16(1, #value),                             \
                          TYPESTRING16(2, #value),                             \
                          TYPESTRING16(3, #value),                             \
                          TYPESTRING16(4, #value),                             \
                          TYPESTRING16(5, #value),                             \
                          TYPESTRING16(6, #value),                             \
                          TYPESTRING16(7, #value)>
#define QConst256_t(value)                                                     \
  LibKet::utils::string_t<TYPESTRING16(0, #value),                             \
                          TYPESTRING16(1, #value),                             \
                          TYPESTRING16(2, #value),                             \
                          TYPESTRING16(3, #value),                             \
                          TYPESTRING16(4, #value),                             \
                          TYPESTRING16(5, #value),                             \
                          TYPESTRING16(6, #value),                             \
                          TYPESTRING16(7, #value),                             \
                          TYPESTRING16(8, #value),                             \
                          TYPESTRING16(9, #value),                             \
                          TYPESTRING16(10, #value),                            \
                          TYPESTRING16(11, #value),                            \
                          TYPESTRING16(12, #value),                            \
                          TYPESTRING16(13, #value),                            \
                          TYPESTRING16(14, #value),                            \
                          TYPESTRING16(15, #value)>

#define QConst1(value)                                                         \
  QConst1_t(value) {}
#define QConst2(value)                                                         \
  QConst2_t(value) {}
#define QConst4(value)                                                         \
  QConst4_t(value) {}
#define QConst8(value)                                                         \
  QConst8_t(value) {}
#define QConst16(value)                                                        \
  QConst16_t(value) {}
#define QConst32(value)                                                        \
  QConst32_t(value) {}
#define QConst64(value)                                                        \
  QConst64_t(value) {}
#define QConst128(value)                                                       \
  QConst128_t(value) {}
#define QConst256(value)                                                       \
  QConst256_t(value) {}

#ifndef QConst_t
#define QConst_t(value) QConst64_t(value)
#endif

#define QConst(value)                                                          \
  QConst_t(value) {}

// clang-format off
#define QConst_M_1_PI      QConst32(0.31830988618379067154)
#define QConst_M_1_PIl     QConst64(0.318309886183790671537767526745028724)
#define QConst_M_2_PI      QConst32(0.63661977236758134308)
#define QConst_M_2_PIl     QConst64(0.636619772367581343075535053490057448)
#define QConst_M_2_SQRTPI  QConst32(1.12837916709551257390)
#define QConst_M_2_SQRTPIl QConst64(1.128379167095512573896158903121545172)
#define QConst_M_E         QConst32(2.7182818284590452354)
#define QConst_M_El        QConst64(2.718281828459045235360287471352662498)
#define QConst_M_LN10      QConst32(2.30258509299404568402)
#define QConst_M_LN10l     QConst64(2.302585092994045684017991454684364208)
#define QConst_M_LN2       QConst32(0.69314718055994530942)
#define QConst_M_LN2l      QConst64(0.693147180559945309417232121458176568)
#define QConst_M_LOG10E    QConst32(0.43429448190325182765)
#define QConst_M_LOG10El   QConst64(0.434294481903251827651128918916605082)
#define QConst_M_LOG2E     QConst32(1.4426950408889634074)
#define QConst_M_LOG2El    QConst64(1.442695040888963407359924681001892137)
#define QConst_M_PI        QConst32(3.14159265358979323846)
#define QConst_M_PI_2      QConst32(1.57079632679489661923)
#define QConst_M_PI_2l     QConst64(1.570796326794896619231321691639751442)
#define QConst_M_PI_4      QConst32(0.78539816339744830962)
#define QConst_M_PI_4l     QConst64(0.785398163397448309615660845819875721)
#define QConst_M_PIl       QConst64(3.141592653589793238462643383279502884)
#define QConst_M_SQRT1_2   QConst32(0.70710678118654752440)
#define QConst_M_SQRT1_2l  QConst64(0.707106781186547524400844362104849039)
#define QConst_M_SQRT2     QConst32(1.41421356237309504880)
#define QConst_M_SQRT2l    QConst64(1.414213562373095048801688724209698079)
// clang-format on

#endif // QUTILS_HPP

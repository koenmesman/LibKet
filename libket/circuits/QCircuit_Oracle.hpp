/** @file libket/circuits/QCircuit_Oracle.hpp

    @brief LibKet oracle circuit class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers
 */

#pragma once
#ifndef QCIRCUIT_ORACLE_HPP
#define QCIRCUIT_ORACLE_HPP

#include <QFilter.hpp>
#include <QUtils.hpp>
#include <circuits/QCircuit.hpp>
#include <gates/QGates.hpp>
#include <random>

namespace LibKet {

// using namespace filters;
// using namespace gates;

namespace circuits {

/**
@brief LibKet oracle circuit class

The LibKet oracle circuit class implements
the oracle algorithm for an arbitrary number of qubits
*/
template<typename _tol = QConst_t(0.0)>
class QCircuit_Oracle : public QCircuit
{
private:
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

  /// Apply function - used for all backends
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           QBackendType _qbackend>
  inline static QData<_qubits, _qbackend>& apply(
    QData<_qubits, _qbackend>& data) noexcept
  {
    srand(time(NULL));
    int number = rand() % 2;
    if (number == 0) {
      auto expr =
        CX(sel<_filter0::template size<_qubits>() - 2>(tag<0>(_filter0{})),
           tag<1>(_filter1{}));
      return expr(data);
    } else {
      auto expr =
        CX(sel<_filter0::template size<_qubits>() - 1>(tag<0>(_filter0{})),
           sel<0>(gototag<1>(
             CX(sel<_filter0::template size<_qubits>() - 1>(tag<0>(_filter0{})),
                tag<1>(_filter1{})))));
      return expr(data);
    }
  }
};
/* !! Note to self !! I deleted optimize, because I can't see how that would be
 * useful.  */

/**
@brief LibKet oracle circuit creator

This overload of the LibKet::circuits::oracle() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::oracle();
\endcode
*/

template<typename _tol = QConst_t(0.0)>
inline constexpr auto
oracle() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCircuit_Oracle<_tol>>(
    filters::QFilter{}, filters::QFilter{});
}

/**
@brief LibKet oracle circuit creator

This overload of the LibKet::circuits::oracle() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::oracle(expr);
\endcode
*/
template<typename _tol = QConst_t(0.0), typename _expr0, typename _expr1>
inline constexpr auto
oracle(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet oracle circuit creator

This overload of the LibKet::circuits::oracle() function accepts
an expression as universal reference
*/
template<typename _tol = QConst_t(0.0), typename _expr0, typename _expr1>
inline constexpr auto
oracle(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _tol = QConst_t(0.0), typename _expr0, typename _expr1>
inline constexpr auto
oracle(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

template<typename _tol = QConst_t(0.0), typename _expr0, typename _expr1>
inline constexpr auto
oracle(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCircuit_Oracle<_tol>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet oracle circuit creator

Function alias for LibKet::circuits::oracle
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
oracle(Args&&... args)
{
  return oracle<_tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Oracle<_tol>::operator()(const T0& t0, const T1& t1) const noexcept
{
  return oracle<_tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCircuit_Oracle<_tol>::operator()(T0&& t0, T1&& t1) const noexcept
{
  return oracle<_tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for oracle objects
*/
template<std::size_t level = 1, typename _tol = QConst_t(0.0)>
inline static auto
show(const QCircuit_Oracle<_tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "oracle\n";

  return circuit;
}

} // namespace circuits
} // namespace LibKet

#endif // QCIRCUIT_ORACLE_HPP

/** @file libket/circuits/QCircuit_QFT.hpp

    @brief LibKet quantum Fourier transform circuit class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QCIRCUIT_QFT_HPP
#define QCIRCUIT_QFT_HPP

#include <QFilter.hpp>
#include <QUtils.hpp>
#include <circuits/QCircuit.hpp>
#include <gates/QGates.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {

/**
 @brief LibKet quantum Fourier transform circuit versions

 The LibKet quantum Fourier transform circuit version enumerator
 defines the different versions of the QFT circuit
*/
enum class QFTMode
{
  /**
     @brief Standard QFT (including all-swap)
  */
  standard,

  /**
     @brief QFT without all-swap
  */
  noswap
};

// Forward declaration
enum class QFTdagMode;
template<QFTdagMode _qftdag, typename _tol>
class QCircuit_QFTdag;

/**
@brief LibKet quantum Fourier transform circuit class

The LibKet quantum Fourier transform (QFT) circuit class implements
the QFT algorithm for an arbitrary number of qubits
*/
template<QFTMode _qft = QFTMode::standard, typename _tol = QConst_t(0.0)>
class QCircuit_QFT : public QCircuit
{
private:
  // Inner loop: Realizes the controlled phase shift gates \f$R_k$\f
  template<index_t start, index_t end, index_t step, index_t index>
  struct qft_loop_inner
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return crk<index - start + 2, _tol>(sel<index>(gototag<0>(expr0)),
                                          sel<start - 1>(gototag<0>(expr1)));
    }
  };

  // Outer loop: Realizes the outer loop
  template<index_t start, index_t end, index_t step, index_t index>
  struct qft_loop_outer
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return utils::static_for<index + 1, end, 1, qft_loop_inner>(
        h(sel<index>(gototag<0>(expr0))), gototag<0>(expr1));
    }
  };

public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function - used for all backends
  template<std::size_t _qubits,
           typename _filter,
           enum QFTMode __qft = _qft,
           QBackendType _qbackend>
  inline static auto apply(QData<_qubits, _qbackend>& data) noexcept ->
    typename std::enable_if<__qft == QFTMode::standard,
                            QData<_qubits, _qbackend>>::type&

  {
    auto expr = allswap(gototag<0>(
      utils::static_for<0,
                        (index_t)_filter::template size<_qubits>() - 1,
                        1,
                        qft_loop_outer>(tag<0>(_filter{}), tag<0>(_filter{}))));
    return expr(data);
  }

  /// Apply function - used for all backends
  template<std::size_t _qubits,
           typename _filter,
           enum QFTMode __qft = _qft,
           QBackendType _qbackend>
  inline static auto apply(QData<_qubits, _qbackend>& data) noexcept ->
    typename std::enable_if<__qft == QFTMode::noswap,
                            QData<_qubits, _qbackend>>::type&

  {
    auto expr = gototag<0>(
      utils::static_for<0,
                        (index_t)_filter::template size<_qubits>() - 1,
                        1,
                        qft_loop_outer>(tag<0>(_filter{}), tag<0>(_filter{})));
    return expr(data);
  }
};

#ifdef LIBKET_OPTIMIZE_GATES

/**
   @brief LibKet QFT circuit creator

   This overload of the LibKet::circuits::qft() function eliminates
   the application of the QFT circuit to its adjoint, the QFT dagger
   circuit, for the case that QFT and QFT dagger are either both of
   standard or noswap type
*/

template<QFTMode _qft = QFTMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qft(const UnaryQGate<_expr,
                     QCircuit_QFTdag<(enum QFTdagMode)_qft, _tol>,
                     typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
 @brief LibKet QFT circuit creator

 This overload of the LibKet::circuits::qft() function eliminates
 the application of the QFT circuit to its adjoint, the QFT dagger
 circuit, for the case that QFT and QFT dagger are either both of
 standard or noswap type
*/

template<QFTMode _qft = QFTMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qft(UnaryQGate<_expr,
               QCircuit_QFTdag<(enum QFTdagMode)_qft, _tol>,
               typename filters::getFilter<_expr>::type>&& expr) noexcept
{
  return expr.expr;
}
#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet QFT circuit creator

This overload of the LibKet::circuits::qft() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qft();
\endcode
*/
template<QFTMode _qft = QFTMode::standard, typename _tol = QConst_t(0.0)>
inline constexpr auto
qft() noexcept
{
  return UnaryQGate<filters::QFilter, QCircuit_QFT<_qft, _tol>>(
    filters::QFilter{});
}

/**
@brief LibKet QFT circuit creator

This overload of the LibKet::circuits::qft() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qft(expr);
\endcode
*/
template<QFTMode _qft = QFTMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qft(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_QFT<_qft, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet QFT circuit creator

This overload of the LibKet::circuits::qft() function accepts
an expression as universal reference
*/
template<QFTMode _qft = QFTMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qft(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_QFT<_qft, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet QFT circuit creator

Function alias for LibKet::circuits::qft
*/
template<QFTMode _qft = QFTMode::standard,
         typename _tol = QConst_t(0.0),
         typename... Args>
inline constexpr auto
QFT(Args&&... args)
{
  return qft<_qft, _tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<QFTMode _qft, typename _tol>
template<typename T>
inline constexpr auto
QCircuit_QFT<_qft, _tol>::operator()(const T& t) const noexcept
{
  return qft<_qft, _tol>(std::forward<T>(t));
}

/// Operator() - by universal reference
template<QFTMode _qft, typename _tol>
template<typename T>
inline constexpr auto
QCircuit_QFT<_qft, _tol>::operator()(T&& t) const noexcept
{
  return qft<_qft, _tol>(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QFT objects
*/
template<std::size_t level = 1,
         enum QFTMode _qft,
         typename _tol = QConst_t(0.0)>
inline static auto
show(const QCircuit_QFT<_qft, _tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QFT\n";

  return circuit;
}

} // namespace circuits

} // namespace LibKet

#endif // QCIRCUIT_QFT_HPP

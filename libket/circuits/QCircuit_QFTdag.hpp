/** @file libket/circuits/QCircuit_QFTdag.hpp

    @brief LibKet inverse quantum Fourier transform circuit class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers
 */

#pragma once
#ifndef QCIRCUIT_QFTDAG_HPP
#define QCIRCUIT_QFTDAG_HPP

#include <QFilter.hpp>
#include <QUtils.hpp>
#include <circuits/QCircuit.hpp>
#include <gates/QGates.hpp>

namespace LibKet {

using namespace filters;
using namespace gates;

namespace circuits {

/**
 @brief LibKet inverse quantum Fourier transform circuit versions

 The LibKet inverse quantum Fourier transform circuit version enumerator
 defines the different versions of the QFT dagger circuit
*/
enum class QFTdagMode
{
  /**
     @brief Standard QFTdag (including all-swap)
  */
  standard,

  /**
     @brief QFTdag without all-swap
  */
  noswap
};

// Forward declaration
enum class QFTMode;
template<QFTMode _qft, typename _tol>
class QCircuit_QFT;

/**
@brief LibKet inverse quantum Fourier transform circuit class

The LibKet inverse quantum Fourier transform (QFT dagger) circuit
class implements the QFTdag algorithm for an arbitrary number of
qubits
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0)>
class QCircuit_QFTdag : public QCircuit
{
private:
  // Inner loop: Realizes the controlled phase shift gates \f$R_k$\f
  template<index_t start, index_t end, index_t step, index_t index>
  struct qftdag_loop_inner
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return crkdag<index - end + 2, _tol>(sel<index>(gototag<0>(expr0)),
                                           sel<end - 1>(gototag<0>(expr1)));
    }
  };

  // Outer loop: Realizes the outer loop
  template<index_t start, index_t end, index_t step, index_t index>
  struct qftdag_loop_outer
  {
    template<typename Expr0, typename Expr1>
    inline constexpr auto operator()(Expr0&& expr0, Expr1&& expr1) noexcept
    {
      return utils::static_for<start, index, -1, qftdag_loop_inner>(
        h(sel<index>(gototag<0>(expr0))), gototag<0>(expr1));
    }
  };

public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function - used for all backends
  template<std::size_t _qubits,
           typename _filter,
           enum QFTdagMode __qftdag = _qftdag,
           QBackendType _qbackend>
  inline static auto apply(QData<_qubits, _qbackend>& data) noexcept ->
    typename std::enable_if<__qftdag == QFTdagMode::standard,
                            QData<_qubits, _qbackend>>::type&
  {
    auto expr = gototag<0>(h(sel<0>(gototag<0>(
      utils::static_for<(index_t)_filter::template size<_qubits>() - 1,
                        1,
                        -1,
                        qftdag_loop_outer>(
        gototag<0>(allswap(tag<0>(_filter{}))), tag<0>(_filter{}))))));
    return expr(data);
  }

  /// Apply function - used for all backends
  template<std::size_t _qubits,
           typename _filter,
           enum QFTdagMode __qftdag = _qftdag,
           QBackendType _qbackend>
  inline static auto apply(QData<_qubits, _qbackend>& data) noexcept ->
    typename std::enable_if<__qftdag == QFTdagMode::noswap,
                            QData<_qubits, _qbackend>>::type&

  {
    auto expr = gototag<0>(h(sel<0>(gototag<0>(
      utils::static_for<(index_t)_filter::template size<_qubits>() - 1,
                        1,
                        -1,
                        qftdag_loop_outer>(tag<0>(_filter{}),
                                           tag<0>(_filter{}))))));
    return expr(data);
  }
};

#ifdef LIBKET_OPTIMIZE_GATES

/**
   @brief LibKet QFT dagger circuit creator

   This overload of the LibKet::circuits::qftdag() function eliminates
   the application of the QFT dagger circuit to its adjoint, the QFT
   circuit, for the case that QFT and QFT dagger are either both of
   standard or noswap type
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(
  const UnaryQGate<_expr,
                   QCircuit_QFT<(enum QFTMode)_qftdag, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
 @brief LibKet QFT dagger circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the QFT dagger circuit to its adjoint, the QFT
 circuit, for the case that QFT and QFT dagger are either both of
 standard or noswap type
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(UnaryQGate<_expr,
                  QCircuit_QFT<(enum QFTMode)_qftdag, _tol>,
                  typename filters::getFilter<_expr>::type>&& expr) noexcept
{
  return expr.expr;
}

/**
 @brief LibKet QFT dagger circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the QFT dagger circuit to its adjoint, the QFT
 circuit, for the special case that QFT dagger is of standard type
 and QFT is of noswap type so that the optimized circuite is simple
 the swap from the QFT dagger
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(
  const UnaryQGate<_expr,
                   QCircuit_QFT<QFTMode::noswap, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::standard,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}

/**
 @brief LibKet QFT dagger circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the QFT dagger circuit to its adjoint, the QFT
 circuit, for the special case that QFT dagger is of standard type
 and QFT is of noswap type so that the optimized circuite is simple
 the swap from the QFT dagger
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(UnaryQGate<_expr,
                  QCircuit_QFT<QFTMode::noswap, _tol>,
                  typename filters::getFilter<_expr>::type>&& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::standard,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}

/**
 @brief LibKet QFT dagger circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the QFT dagger circuit to its adjoint, the QFT
 circuit, for the special case that QFT dagger is of noswap type
 and QFT is of standard type so that the optimized circuite is simple
 the swap from the QFT dagger
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(
  const UnaryQGate<_expr,
                   QCircuit_QFT<QFTMode::standard, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::noswap,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}

/**
 @brief LibKet QFT dagger circuit creator

 This overload of the LibKet::circuits::qftdag() function eliminates
 the application of the QFT dagger circuit to its adjoint, the QFT
 circuit, for the special case that QFT dagger is of noswap type
 and QFT is of standard type so that the optimized circuite is simple
 the swap from the QFT dagger
*/

template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(UnaryQGate<_expr,
                  QCircuit_QFT<QFTMode::standard, _tol>,
                  typename filters::getFilter<_expr>::type>&& expr) noexcept ->
  typename std::enable_if<_qftdag == QFTdagMode::noswap,
                          decltype(allswap(expr.expr))>::type
{
  return allswap(expr.expr);
}
#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet QFT dagger circuit creator

This overload of the LibKet::circuits::qftdag() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qftdag();
\endcode
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0)>
inline constexpr auto
qftdag() noexcept
{
  return UnaryQGate<filters::QFilter, QCircuit_QFTdag<_qftdag, _tol>>(
    filters::QFilter{});
}

/**
@brief LibKet QFT dagger circuit creator

This overload of the LibKet::circuits::qftdag() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto qcirc = circuits::qft(expr);
\endcode
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_QFTdag<_qftdag, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet QFT dagger circuit creator

This overload of the LibKet::circuits::qftdag() function accepts
an expression as universal reference
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename _expr>
inline constexpr auto
qftdag(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QCircuit_QFTdag<_qftdag, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet QFT dagger circuit creator

Function alias for LibKet::circuits::qft
*/
template<QFTdagMode _qftdag = QFTdagMode::standard,
         typename _tol = QConst_t(0.0),
         typename... Args>
inline constexpr auto
QFTdag(Args&&... args)
{
  return qftdag<_qftdag, _tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<QFTdagMode _qftdag, typename _tol>
template<typename T>
inline constexpr auto
QCircuit_QFTdag<_qftdag, _tol>::operator()(const T& t) const noexcept
{
  return qftdag(std::forward<T>(t));
}

/// Operator() - by universal reference
template<QFTdagMode _qftdag, typename _tol>
template<typename T>
inline constexpr auto
QCircuit_QFTdag<_qftdag, _tol>::operator()(T&& t) const noexcept
{
  return qftdag(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QFT dagger objects
*/
template<std::size_t level = 1,
         enum QFTdagMode _qftdag,
         typename _tol = QConst_t(0.0)>
inline static auto
show(const QCircuit_QFTdag<_qftdag, _tol>& circuit,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QFTdag\n";

  return circuit;
}

} // namespace circuits

} // namespace LibKet

#endif // QCIRCUIT_QFTdag_HPP

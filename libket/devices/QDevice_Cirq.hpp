/** @file libket/devices/QDevice_Cirq.hpp

    @brief LibKet Cirq device class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_CIRQ_HPP
#define QDEVICE_CIRQ_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_CIRQ
/**
   @brief LibKet Cirq simulator device class

   This class executes quantum circuits locally on the Cirq
   simulator. It adopts the Cirq quantum assembly language.

*/
template<std::size_t _qubits>
class QDevice_Cirq_simulator : public QData<_qubits, QBackendType::Cirq>
{
private:
  const std::string backend;
  const std::size_t shots;

  using Base = QData<_qubits, QBackendType::Cirq>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_Cirq_simulator(
    const std::string& backend = LibKet::getenv("CIRQ_BACKEND", "simulator"),
    const std::size_t& shots = std::atoi(LibKet::getenv("CIRQ_SHOTS", "1024")))
    : backend(backend)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_Cirq_simulator(const utils::json& config)
    : QDevice_Cirq_simulator(
        config.find("backend") != config.end()
          ? config["backend"]
          : LibKet::getenv("CIRQ_BACKEND", "simulator"),
        config.find("shots") != config.end()
          ? config["shots"].get<size_t>()
          : std::atoi(LibKet::getenv("CIRQ_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Cirq_simulator& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Cirq_simulator& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit locally on Cirq simulator
  /// asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    ss << "\timport json\n"
       << "\timport cirq\n"
       << "\tq = [cirq.LineQubit(x) for x in range(" +
            utils::to_string(_qubits) + ")]\n"
       << "\tprogram = '''\n"
       << Base::to_string() << "'''\n"
       << "\tcircuit = "
          "cirq.Circuit(eval(program.replace('\\n',',').strip(',')))\n";

    if (!backend.compare("bristlecone"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Bristlecone, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_xmon(circuit=circuit, "
            "new_device=cirq.google.Bristlecone, "
            "qubit_map=lambda i: line[i.x])\n";

    if (!backend.compare("foxtail"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Foxtail, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_xmon(circuit=circuit, "
            "new_device=cirq.google.Foxtail, "
            "qubit_map=lambda i: line[i.x])\n";

    if (!backend.compare("sycamore"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Sycamore, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_sycamore(circuit=circuit, "
            "new_device=cirq.google.Sycamore, "
            "qubit_map=lambda i: line[i.x])\n";

    if (!backend.compare("sycamore23"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Sycamore23, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_sycamore(circuit=circuit, "
            "new_device=cirq.google.Sycamore23, "
            "qubit_map=lambda i: line[i.x])\n";

    ss << "\tsimulator = cirq.Simulator()\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    ss << "\tresult = simulator.run(circuit, repetitions="
       << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    ss << "\treturn cirq.to_json(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit locally on Cirq simulator
  /// synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit locally on Cirq simulator
  /// synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

  /// Export circuit to LaTeX using the Q-circuit package
  std::string to_latex(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    ss << "\timport json\n"
       << "\timport cirq\n"
       << "\tq = [cirq.LineQubit(x) for x in range(" +
            utils::to_string(_qubits) + ")]\n"
       << "\tprogram = '''\n"
       << Base::to_string() << "'''\n"
       << "\tcircuit = "
          "cirq.Circuit(eval(program.replace('\\n',',').strip(',')))\n";

    if (!backend.compare("bristlecone"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Bristlecone, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_xmon(circuit=circuit, "
            "new_device=cirq.google.Bristlecone, "
            "qubit_map=lambda i: line[i.x])\n";

    if (!backend.compare("foxtail"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Foxtail, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_xmon(circuit=circuit, "
            "new_device=cirq.google.Foxtail, "
            "qubit_map=lambda i: line[i.x])\n";

    if (!backend.compare("sycamore"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Sycamore, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_sycamore(circuit=circuit, "
            "new_device=cirq.google.Sycamore, "
            "qubit_map=lambda i: line[i.x])\n";

    if (!backend.compare("sycamore23"))
      ss << "\tline = cirq.google.line_on_device(cirq.google.Sycamore23, "
            "length = " +
              utils::to_string(_qubits) + ")\n"
         << "\tcircuit = cirq.google.optimized_for_sycamore(circuit=circuit, "
            "new_device=cirq.google.Sycamore23, "
            "qubit_map=lambda i: line[i.x])\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    ss << "\tresult = cirq.contrib.circuit_to_latex_using_qcircuit(circuit)\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    ss << "\treturn cirq.to_json(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();

      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Export circuit to Quirk URL
  std::string to_quirk(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    ss << "\timport json\n"
       << "\timport cirq\n"
       << "\tq = [cirq.LineQubit(x) for x in range(" +
            utils::to_string(_qubits) + ")]\n"
       << "\tprogram = '''\n"
       << Base::to_string() << "'''\n"
       << "\tcircuit = "
          "cirq.Circuit(eval(program.replace('\\n',',').strip(',')))\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    ss << "\tresult - cirq.contrib.quirk.circuit_to_quirk_url(circuit)\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    ss << "\treturn cirq.to_json(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();

      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _histogram = get<QResultType::histogram>(result);
    std::size_t _value = 0;
    std::size_t _key = 0;

    for (std::size_t i = 0; i < _histogram.size(); ++i) {
      if (_histogram[i] > _value) {
        _value = _histogram[i];
        _key = i;
      }
    }

    return _key;
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(0);
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _measurements = result["measurements"];

    std::array<std::size_t, _qubits> _values;
    for (auto& _item : _measurements.items()) {
      bool _binary = _item.value()["binary"].get<bool>();
      std::string _digits = _item.value()["packed_digits"].get<std::string>();
      _values[stoi(_item.key())] = stoi(_digits, 0, _binary ? 16 : 10);
    }

    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;
    std::size_t _shots = _measurements["0"]["shape"][0].get<std::size_t>();

    for (std::size_t _shot = 0; _shot < _shots; ++_shot) {
      std::bitset<_qubits> _value;
      for (std::size_t i = 0; i < _qubits; ++i)
        _value[i] = ((_values[i] >> _shot) & 1);
      _histogram[_value.to_ulong()]++;
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return std::string("0");
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return (result["cirq_type"].get<std::string>().compare("TrialResult") == 0);
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));
    return std::time(nullptr);
  }
};

#else

/**
   @brief LibKet Cirq simulator device class

   This class executes quantum circuits locally on the Cirq
   simulator. It adopts the Cirq quantum assembly language.

*/
template<std::size_t _qubits>
class QDevice_Cirq_simulator : public QDevice_Dummy
{};

#endif

#define QDeviceDefineCirq(_type)                                               \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_Cirq_simulator<_qubits>                                   \
  {                                                                            \
  public:                                                                      \
    QDevice(const std::size_t& shots = std::atoi(LibKet::getenv("CIRQ_SHOTS",  \
                                                                "1024")))      \
      : QDevice_Cirq_simulator<_qubits>(device::QDeviceProperty<_type>::name,  \
                                        shots)                                 \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("CIRQ_SHOTS", "1024")))           \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

namespace device {

QDevicePropertyDefine(QDeviceType::cirq_simulator,
                      "simulator",
                      1024,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::cirq_bristlecone_simulator,
                      "bristlecone",
                      72,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::cirq_foxtail_simulator,
                      "foxtail",
                      22,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::cirq_sycamore_simulator,
                      "sycamore",
                      54,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::cirq_sycamore23_simulator,
                      "sycamore",
                      23,
                      true,
                      QEndianness::lsb);

} // namespace device

QDeviceDefineCirq(QDeviceType::cirq_simulator);
QDeviceDefineCirq(QDeviceType::cirq_bristlecone_simulator);
QDeviceDefineCirq(QDeviceType::cirq_foxtail_simulator);
QDeviceDefineCirq(QDeviceType::cirq_sycamore_simulator);
QDeviceDefineCirq(QDeviceType::cirq_sycamore23_simulator);

} // namespace LibKet

#endif // QDEVICE_CIRQ_HPP

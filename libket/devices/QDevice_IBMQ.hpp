/** @file libket/devices/QDevice_IBMQ.hpp

    @brief LibKet IBM Quantum Experience device class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_IBMQ_HPP
#define QDEVICE_IBMQ_HPP

#include <ctime>
#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_OPENQASM
/**
   @brief LibKet IBM Quantum Experience physical device class

   This class executes quantum circuits remotely on physical quantum
   devices made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.
*/
template<std::size_t _qubits>
class QDevice_IBMQ : public QData<_qubits, QBackendType::OpenQASMv2>
{
private:
  const std::string API_token;
  const std::string backend;
  const std::size_t shots;

  using Base = QData<_qubits, QBackendType::OpenQASMv2>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_IBMQ(
    const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),
    const std::string& backend = LibKet::getenv("IBMQ_BACKEND", "ibmq_armonk"),
    std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
    : API_token(API_token)
    , backend(backend)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_IBMQ(const utils::json& config)
    : QDevice_IBMQ(config.find("API_token") != config.end()
                     ? config["API_token"]
                     : LibKet::getenv("IBMQ_API_TOKEN"),
                   config.find("backend") != config.end()
                     ? config["backend"]
                     : LibKet::getenv("IBMQ_BACKEND", "ibmq_armonk"),
                   config.find("shots") != config.end()
                     ? config["shots"].get<size_t>()
                     : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_IBMQ& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_IBMQ& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tdef convert(o):\n"
       << "\t\timport numpy\n"
       << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
       << "\t\traise TypeError\n"
       << "\timport json\n"
       << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, execute\n";

    // Authentication to IBMQ
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_accounts()\n";
    else
      ss << "\tprovider = IBMQ.enable_account('" << API_token << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Execute circuit remotely
    ss << "\tjob = execute(qc, backend=backend, shots="
       << utils::to_string(shots > 0 ? shots : this->shots) << ")\n"
       << "\tresult = job.result()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result.to_dict(), default=convert)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

  /// Export circuit to LaTeX using the XYZ package
  std::string to_latex(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
          "QuantumCircuit\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    // Authentication to IBMQ
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_accounts()\n";
    else
      ss << "\tprovider = IBMQ.enable_account('" << API_token << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Transpile circuit
    ss << "\tqc = transpile(qc, backend=backend)\n";

    // Draw circuit to latex source
    ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    std::size_t _value = 0;
    std::string _key = "0x0";

    for (auto& _item : _counts.items()) {
      if (_item.value() > _value) {
        _value = _item.value();
        _key = _item.key();
      }
    }

    return stoi(_key, 0, 16);
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["time_taken"].get<Rep>());
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (auto& _item : _counts.items()) {
      _histogram[stoi(_item.key(), 0, 16)] = (T)_item.value();
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return result["job_id"].get<std::string>();
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return result["success"].get<bool>();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    struct tm tm;
    strptime(
      result["date"].get<std::string>().c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
    return mktime(&tm);
  }
};

/**
   @brief LibKet IBM Quantum Experience simulator device class

   This class executes quantum circuits remotely on IBM's quantum
   simulator made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.
*/
template<std::size_t _qubits>
class QDevice_IBMQ_simulator : public QData<_qubits, QBackendType::OpenQASMv2>
{
private:
  std::string API_token;
  std::string backend;
  std::size_t shots;

  using Base = QData<_qubits, QBackendType::OpenQASMv2>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_IBMQ_simulator(
    const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),
    const std::string& backend = LibKet::getenv("IBMQ_BACKEND",
                                                "ibmq_qasm_simulator"),
    std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
    : API_token(API_token)
    , backend(backend)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_IBMQ_simulator(const utils::json& config)
    : QDevice_IBMQ_simulator(
        config.find("API_token") != config.end()
          ? config["API_token"]
          : LibKet::getenv("IBMQ_API_TOKEN"),
        config.find("backend") != config.end()
          ? config["backend"]
          : LibKet::getenv("IBMQ_BACKEND", "ibmq_qasm_simulator"),
        config.find("shots") != config.end()
          ? config["shots"].get<size_t>()
          : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_IBMQ_simulator& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_IBMQ_simulator& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tdef convert(o):\n"
       << "\t\timport numpy\n"
       << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
       << "\t\traise TypeError\n"
       << "\timport json\n"
       << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, execute\n";

    // Import noise model for hardware simulators
    if (backend.compare("ibmq_qasm_simulator"))
      ss << "\tfrom qiskit.providers.aer import noise\n";

    // Authentication to IBMQ
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_accounts()\n";
    else
      ss << "\tprovider = IBMQ.enable_account('" << API_token << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // Prepare noise model for hardware simulator
    if (backend.compare("ibmq_qasm_simulator")) {
      ss
        << "\tproperties = backend.properties()\n"
        << "\tcoupling_map = backend.configuration().coupling_map\n"
        << "\tnoise_model = noise.device.basic_device_noise_model(properties)\n"
        << "\tbasis_gates = noise_model.basis_gates\n"
        << "\tbackend_simulator = "
           "provider.get_backend('ibmq_qasm_simulator')\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute noisy circuit remotely
      ss << "\tjob = execute(qc, backend_simulator, coupling_map=coupling_map, "
            "noise_model=noise_model, basis_gates=basis_gates, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute circuit remotely
      ss << "\tjob = execute(qc, backend=backend, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    }
    ss << "\tresult = job.result()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result.to_dict(), default=convert)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return pointer to jbo
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on IBM's Quantum Experience
  /// service synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

  /// Export circuit to LaTeX using the XYZ package
  std::string to_latex(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tfrom qiskit import IBMQ, QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    // Import noise model for hardware simulators
    if (backend.compare("ibmq_qasm_simulator"))
      ss << "\tfrom qiskit.providers.aer import noise\n";

    // Authentication to IBMQ
    if (API_token.empty())
      ss << "\tprovider = IBMQ.load_accounts()\n";
    else
      ss << "\tprovider = IBMQ.enable_account('" << API_token << "')\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tbackend = provider.get_backend('" << backend << "')\n"
       << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    // Prepare noise model for hardware simulator
    if (backend.compare("ibmq_qasm_simulator")) {
      ss
        << "\tproperties = backend.properties()\n"
        << "\tcoupling_map = backend.configuration().coupling_map\n"
        << "\tnoise_model = noise.device.basic_device_noise_model(properties)\n"
        << "\tbasis_gates = noise_model.basis_gates\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Transpile circuit
      ss << "\tqc = transpile(qc, backend, coupling_map=coupling_map, "
            "basis_gates=basis_gates)\n";

      // Draw circuit to latex source
      ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Draw circuit to latex source
      ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";
    }

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    std::size_t _value = 0;
    std::string _key = "0x0";

    for (auto& _item : _counts.items()) {
      if (_item.value() > _value) {
        _value = _item.value();
        _key = _item.key();
      }
    }

    return stoi(_key, 0, 16);
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["time_taken"].get<Rep>());
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (auto& _item : _counts.items()) {
      _histogram[stoi(_item.key(), 0, 16)] = (T)_item.value();
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return result["job_id"].get<std::string>();
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return result["success"].get<bool>();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    struct tm tm;
    strptime(
      result["date"].get<std::string>().c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
    return mktime(&tm);
  }
};

#else

/**
   @brief LibKet IBM Quantum Experience physical device class

   This class executes quantum circuits remotely on physical quantum
   devices made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.
*/
template<std::size_t _qubits>
class QDevice_IBMQ : public QDevice_Dummy
{};

/**
   @brief LibKet IBM Quantum Experience simulator device class

   This class executes quantum circuits remotely on IBM's quantum
   simulator made accessible through IBM's Quantum Experience cloud
   services. It adopts the OpenQASM v2.0 quantum assembly language.
*/
template<std::size_t _qubits>
class QDevice_IBMQ_simulator : public QDevice_Dummy
{};
#endif

#define QDeviceDefineIBMQ(_type)                                               \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_IBMQ<_qubits>                                             \
  {                                                                            \
  public:                                                                      \
    QDevice(const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),   \
            std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS",         \
                                                         "1024")))             \
      : QDevice_IBMQ<_qubits>(API_token,                                       \
                              device::QDeviceProperty<_type>::name,            \
                              shots)                                           \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("API_token") != config.end()                       \
                  ? config["API_token"]                                        \
                  : LibKet::getenv("IBMQ_API_TOKEN"),                          \
                config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))           \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

#define QDeviceDefineIBMQ_simulator(_type)                                     \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_IBMQ_simulator<_qubits>                                   \
  {                                                                            \
  public:                                                                      \
    QDevice(const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),   \
            std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS",         \
                                                         "1024")))             \
      : QDevice_IBMQ_simulator<_qubits>(API_token,                             \
                                        device::QDeviceProperty<_type>::name,  \
                                        shots)                                 \
    {}                                                                         \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("API_token") != config.end()                       \
                  ? config["API_token"]                                        \
                  : LibKet::getenv("IBMQ_API_TOKEN"),                          \
                config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))           \
    {}                                                                         \
  };

namespace device {

// IBM Quantum Experience hardware devices
QDevicePropertyDefine(QDeviceType::ibmq_x2,
                      "ibmqx2",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_16_melbourne,
                      "ibmq_16_melbourne",
                      15,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_vigo,
                      "ibmq_vigo",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_ourense,
                      "ibmq_ourense",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_london,
                      "ibmq_london",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_burlington,
                      "ibmq_burlington",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_essex,
                      "ibmq_essex",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_armonk,
                      "ibmq_armonk",
                      1,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_rome,
                      "ibmq_rome",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_santiago,
                      "ibmq_santiago",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_bogota,
                      "ibmq_bogota",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_valencia,
                      "ibmq_valencia",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_5_yorktown,
                      "ibmq_5_yorktown",
                      5,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_20_tokio,
                      "ibmq_20_tokio",
                      20,
                      false,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_20_poughkeepsie,
                      "ibmq_20_poughkeepsie",
                      20,
                      false,
                      QEndianness::lsb);

// IBM Quantum Experience simulator devices
QDevicePropertyDefine(QDeviceType::ibmq_qasm_simulator,
                      "ibmq_qasm_simulator",
                      32,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_x2_simulator,
                      "ibmqx2",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_16_melbourne_simulator,
                      "ibmq_16_melbourne",
                      15,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_vigo_simulator,
                      "ibmq_vigo",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_ourense_simulator,
                      "ibmq_ourense",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_london_simulator,
                      "ibmq_london",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_burlington_simulator,
                      "ibmq_burlington",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_essex_simulator,
                      "ibmq_essex",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_armonk_simulator,
                      "ibmq_armonk",
                      1,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_rome_simulator,
                      "ibmq_rome",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_santiago_simulator,
                      "ibmq_santiago",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_bogota_simulator,
                      "ibmq_bogota",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_valencia_simulator,
                      "ibmq_valencia",
                      5,
                      true,
                      QEndianness::lsb);
    QDevicePropertyDefine(QDeviceType::ibmq_5_yorktown_simulator,
                      "ibmq_5_yorktown",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_20_tokio_simulator,
                      "ibmq_20_tokio",
                      20,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::ibmq_20_poughkeepsie_simulator,
                      "ibmq_20_poughkeepsie",
                      20,
                      true,
                      QEndianness::lsb);

} // namespace device

// IBM Quantum Experience hardware devices
QDeviceDefineIBMQ(QDeviceType::ibmq_16_melbourne);
QDeviceDefineIBMQ(QDeviceType::ibmq_20_poughkeepsie);
QDeviceDefineIBMQ(QDeviceType::ibmq_20_tokio);
QDeviceDefineIBMQ(QDeviceType::ibmq_5_yorktown);
QDeviceDefineIBMQ(QDeviceType::ibmq_armonk);
QDeviceDefineIBMQ(QDeviceType::ibmq_bogota);
QDeviceDefineIBMQ(QDeviceType::ibmq_burlington);
QDeviceDefineIBMQ(QDeviceType::ibmq_essex);
QDeviceDefineIBMQ(QDeviceType::ibmq_london);
QDeviceDefineIBMQ(QDeviceType::ibmq_ourense);
QDeviceDefineIBMQ(QDeviceType::ibmq_rome);
QDeviceDefineIBMQ(QDeviceType::ibmq_santiago);
QDeviceDefineIBMQ(QDeviceType::ibmq_valencia);
QDeviceDefineIBMQ(QDeviceType::ibmq_vigo);
QDeviceDefineIBMQ(QDeviceType::ibmq_x2);

// IBM Quantum Experience simulator devices
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_16_melbourne_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_20_poughkeepsie_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_20_tokio_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_5_yorktown_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_armonk_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_bogota_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_burlington_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_essex_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_london_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_ourense_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_rome_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_santiago_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_valencia_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_vigo_simulator);
QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_x2_simulator);

QDeviceDefineIBMQ_simulator(QDeviceType::ibmq_qasm_simulator);

} // namespace LibKet

#endif // QDEVICE_IBMQ_HPP

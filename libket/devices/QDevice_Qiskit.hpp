/** @file libket/devices/QDevice_Qiskit.hpp

    @brief LibKet Qiskit device class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_QISKIT_HPP
#define QDEVICE_QISKIT_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_OPENQASM
/**
   @brief LibKet Qiskit device class
*/
template<std::size_t _qubits>
class QDevice_Qiskit : public QData<_qubits, QBackendType::OpenQASMv2>
{
private:
  const std::string API_token;
  const std::string backend;
  const std::size_t shots;

  using Base = QData<_qubits, QBackendType::OpenQASMv2>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_Qiskit(
    const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),
    const std::string& backend = LibKet::getenv("IBMQ_BACKEND",
                                                "qasm_simulator"),
    std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
    : API_token(API_token)
    , backend(backend)
    , shots(shots)
  {
    // Qiskit PulseSimulator is not supported
    if (!backend.compare("pulse_simulator"))
      throw std::runtime_error("Qiskit PulseSimulator is not supported");
  }

  /// Constructor from JSON object
  QDevice_Qiskit(const utils::json& config)
    : QDevice_Qiskit(config.find("API_token") != config.end()
                       ? config["API_token"]
                       : LibKet::getenv("IBMQ_API_TOKEN"),
                     config.find("backend") != config.end()
                       ? config["backend"]
                       : LibKet::getenv("IBMQ_BACKEND", "qasm_simulator"),
                     config.find("shots") != config.end()
                       ? config["shots"].get<size_t>()
                       : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Qiskit& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Qiskit& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit locally on Qiskit simulator
  /// asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\tdef convert(o):\n"
       << "\t\timport numpy\n"
       << "\t\tif isinstance(o, numpy.int32): return int(o)\n"
       << "\t\traise TypeError\n"
       << "\timport json\n"
       << "\tfrom qiskit import QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, Aer, execute\n";

    if (backend.compare("qasm_simulator") *
        backend.compare("statevector_simulator") *
        backend.compare("unitary_simulator")) {
      // Import IBMQ to retrieve hardware configurations and noise model
      ss << "\tfrom qiskit import IBMQ\n"
         << "\tfrom qiskit.providers.aer import noise\n";

      // Authentication to IBMQ
      if (API_token.empty())
        ss << "\tprovider = IBMQ.load_accounts()\n";
      else
        ss << "\tprovider = IBMQ.enable_account('" << API_token << "')\n";

      // Select hardware backend to simulate
      ss << "\tbackend = provider.get_backend('" << backend << "')\n";
    } else {
      // Import Aer for idealistic simulation
      ss << "\tbackend = Aer.get_backend('" << backend << "')\n";
    }

    // Prepare quantum circuit
    ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    if (backend.compare("qasm_simulator") *
        backend.compare("statevector_simulator") *
        backend.compare("unitary_simulator")) {
      // Prepare noise model for hardware simulator
      ss
        << "\tproperties = backend.properties()\n"
        << "\tcoupling_map = backend.configuration().coupling_map\n"
        << "\tnoise_model = noise.device.basic_device_noise_model(properties)\n"
        << "\tbasis_gates = noise_model.basis_gates\n"
        << "\tbackend_simulator = Aer.get_backend('qasm_simulator')\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute noisy circuit locally
      ss << "\tjob = execute(qc, backend_simulator, coupling_map=coupling_map, "
            "noise_model=noise_model, basis_gates=basis_gates, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Execute circuit locally
      ss << "\tjob = execute(qc, backend=backend, shots="
         << utils::to_string(shots > 0 ? shots : this->shots) << ")\n";
    }
    ss << "\tresult = job.result()\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result.to_dict(), default=convert)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit locally on Qiskit simulator
  /// synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit locally on Qiskit simulator
  /// synchronously and return result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

  /// Export circuit to LaTeX using the XYZ package
  std::string to_latex(const std::string& script_init = "",
                       const std::string& script_before = "",
                       const std::string& script_after = "",
                       QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\timport json\n"
       << "\tfrom qiskit import QuantumRegister, ClassicalRegister, "
          "QuantumCircuit, Aer, transpile\n"
       << "\tfrom qiskit.tools.visualization import circuit_drawer\n";

    if (backend.compare("qasm_simulator") *
        backend.compare("statevector_simulator") *
        backend.compare("unitary_simulator")) {
      // Import IBMQ to retrieve hardware configurations and noise model
      ss << "\tfrom qiskit import IBMQ\n"
         << "\tfrom qiskit.providers.aer import noise\n";

      // Authentication to IBMQ
      if (API_token.empty())
        ss << "\tprovider = IBMQ.load_accounts()\n";
      else
        ss << "\tprovider = IBMQ.enable_account('" << API_token << "')\n";

      // Select hardware backend to simulate
      ss << "\tbackend = provider.get_backend('" << backend << "')\n";
    } else {
      // Import Aer for idealistic simulation
      ss << "\tbackend = Aer.get_backend('" << backend << "')\n";
    }

    // Prepare quantum circuit
    ss << "\tqreg = QuantumRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tcreg = ClassicalRegister(" << utils::to_string(_qubits) << ")\n"
       << "\tqc = QuantumCircuit(qreg, creg)\n"
       << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tqc = qc.from_qasm_str(qasm)\n";

    if (backend.compare("qasm_simulator") *
        backend.compare("statevector_simulator") *
        backend.compare("unitary_simulator")) {
      // Prepare noise model for hardware simulator
      ss
        << "\tproperties = backend.properties()\n"
        << "\tcoupling_map = backend.configuration().coupling_map\n"
        << "\tnoise_model = noise.device.basic_device_noise_model(properties)\n"
        << "\tbasis_gates = noise_model.basis_gates\n";

      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");

      // Transpile circuit
      ss << "\tqc = transpile(qc, backend, coupling_map=coupling_map, "
            "basis_gates=basis_gates)\n";
    } else {
      // User-defined script to be performed righty before execution
      if (!script_before.empty())
        ss << utils::string_ident(script_before, "\t");
    }

    // Draw circuit to latex source
    ss << "\tresult = circuit_drawer(qc, output='latex_source')\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result)\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return (stream->run(ss.str(), "run", "", "")->get()).get<std::string>();
      else
        return (_qstream_python.run(ss.str(), "run", "", "")->get())
          .get<std::string>();
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    std::size_t _value = 0;
    std::string _key = "0x0";

    for (auto& _item : _counts.items()) {
      if (_item.value() > _value) {
        _value = _item.value();
        _key = _item.key();
      }
    }

    return stoi(_key, 0, 16);
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>(result));
    return std::chrono::duration<Rep, Period>(result["time_taken"].get<Rep>());
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    auto _counts = result["results"][0]["data"]["counts"];
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (auto& _item : _counts.items()) {
      _histogram[stoi(_item.key(), 0, 16)] = (T)_item.value();
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return result["job_id"].get<std::string>();
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return result["success"].get<bool>();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    struct tm tm;
    strptime(
      result["date"].get<std::string>().c_str(), "%Y-%m-%dT%H:%M:%S", &tm);
    return mktime(&tm);
  }
};

#else

/**
   @brief LibKet Qiskit device class
*/
template<std::size_t _qubits>
class QDevice_Qiskit : public QDevice_Dummy
{};

#endif

#define QDeviceDefineQiskit(_type)                                             \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_Qiskit<_qubits>                                           \
  {                                                                            \
  public:                                                                      \
    QDevice(const std::string& API_token = LibKet::getenv("IBMQ_API_TOKEN"),   \
            std::size_t shots = std::atoi(LibKet::getenv("IBMQ_SHOTS",         \
                                                         "1024")))             \
      : QDevice_Qiskit<_qubits>(API_token,                                     \
                                device::QDeviceProperty<_type>::name,          \
                                shots)                                         \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("API_token") != config.end()                       \
                  ? config["API_token"]                                        \
                  : LibKet::getenv("IBMQ_API_TOKEN"),                          \
                config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("IBMQ_SHOTS", "1024")))           \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

namespace device {

QDevicePropertyDefine(QDeviceType::qiskit_qasm_simulator,
                      "qasm_simulator",
                      32,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_x2_simulator,
                      "ibmqx2",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_16_melbourne_simulator,
                      "ibmq_16_melbourne",
                      15,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_vigo_simulator,
                      "ibmq_vigo",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_ourense_simulator,
                      "ibmq_ourense",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_london_simulator,
                      "ibmq_london",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_burlington_simulator,
                      "ibmq_burlington",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_essex_simulator,
                      "ibmq_essex",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_armonk_simulator,
                      "ibmq_armonk",
                      1,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_rome_simulator,
                      "ibmq_rome",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_santiago_simulator,
                      "ibmq_santiago",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_bogota_simulator,
                      "ibmq_bogota",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_valencia_simulator,
                      "ibmq_valencia",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_5_yorktown_simulator,
                      "ibmq_5_yorktown",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_20_tokio_simulator,
                      "ibmq_20_tokia",
                      5,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_20_poughkeepsie_simulator,
                      "ibmq_20_poughkeepsie",
                      20,
                      true,
                      QEndianness::lsb);

QDevicePropertyDefine(QDeviceType::qiskit_statevector_simulator,
                      "statevector_simulator",
                      32,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::qiskit_unitary_simulator,
                      "unitary_simulator",
                      16,
                      true,
                      QEndianness::lsb);

} // namespace device

QDeviceDefineQiskit(QDeviceType::qiskit_qasm_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_16_melbourne_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_20_poughkeepsie_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_20_tokio_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_5_yorktown_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_armonk_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_bogota_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_burlington_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_essex_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_london_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_ourense_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_rome_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_santiago_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_valencia_simulator);  
QDeviceDefineQiskit(QDeviceType::qiskit_vigo_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_x2_simulator);

QDeviceDefineQiskit(QDeviceType::qiskit_statevector_simulator);
QDeviceDefineQiskit(QDeviceType::qiskit_unitary_simulator);

} // namespace LibKet

#endif // QDEVICE_QUANTUMINSPIRE_HPP

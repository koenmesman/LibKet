/** @file libket/devices/QDevice_QuEST.hpp

    @brief LibKet Quantum Exact Simulation Toolkit device class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_QUEST_HPP
#define QDEVICE_QUEST_HPP

#include <string>
#include <vector>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_QUEST

namespace detail {
/**
   @brief LibKet generic object wrapper
*/
template<typename _device>
class _Type
{
public:
  virtual _device& operator()(_device& device) = 0;
};

/**
   @brief LibKet generic object container
 */
template<typename _device, typename T>
class __Type : public _Type<_device>
{
public:
  __Type(T obj)
    : _obj(new T(obj))
  {}

  __Type(T* obj)
    : _obj(obj)
  {}

  virtual _device& operator()(_device& device) override
  {
    static_cast<T*>(_obj)->operator()(
      *reinterpret_cast<typename _device::Base*>(&device));
    return device;
  }

private:
  T* _obj;
};

/**
   @brief LibKet generic object container - specialization for type std::string
 */
template<typename _device>
class __Type<_device, std::string> : public _Type<_device>
{
public:
  __Type(std::string obj)
    : _obj(new std::string(obj))
  {}

  __Type(std::string* obj)
    : _obj(obj)
  {}

  virtual _device& operator()(_device& device) override
  {
    gen_expression(*static_cast<std::string*>(_obj),
                   *reinterpret_cast<typename _device::Base*>(&device));
    return device;
  }

private:
  std::string* _obj;
};
} // end namespace detail

/**
   @brief LibKet QuEST device class
*/
template<std::size_t _qubits>
class QDevice_QuEST : public QData<_qubits, QBackendType::QuEST>
{
public:
  using Base = QData<_qubits, QBackendType::QuEST>;

private:
  using _Type = detail::_Type<QDevice_QuEST>;
  template<typename T>
  using __Type = detail::__Type<QDevice_QuEST, T>;

private:
  const std::size_t shots;
  std::vector<_Type*> _expr;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_QuEST(std::size_t shots = std::atoi(LibKet::getenv("QUEST_SHOTS",
                                                             "1024")))
    : shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_QuEST(const utils::json& config)
    : QDevice_QuEST(config.find("shots") != config.end()
                      ? config["shots"].get<size_t>()
                      : std::atoi(LibKet::getenv("QUEST_SHOTS", "1024")))
  {}

  /// Destructor
  ~QDevice_QuEST()
  {
    for (auto it = _expr.begin(); it != _expr.end(); ++it)
      delete *it;
  }

  /// Apply expression to base type
  template<typename Expr>
  QDevice_QuEST& operator()(const Expr& expr)
  {
    _expr.push_back(new __Type<Expr>(expr));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_QuEST& operator()(const std::string& expr)
  {
    _expr.push_back(new __Type<std::string>(expr));
    return *this;
  }

  /// Return the complex amplitudes of the 2^N incides of the state vector
  QArray<(1 << _qubits), quest::Complex, QEndianness::lsb> amplitudes()
  {
    QArray<(1 << _qubits), quest::Complex, QEndianness::lsb> _amp;
    for (index_t index = 0; index < (1 << _qubits); ++index)
      _amp[index] = quest::getAmp(this->reg(), index);
    return _amp;
  }

  /// Return the probabilities of the 2^N indices of the state vector
  QArray<(1 << _qubits), qreal, QEndianness::lsb> probabilities()
  {
    QArray<(1 << _qubits), qreal, QEndianness::lsb> _prob;
    for (index_t index = 0; index < (1 << _qubits); ++index)
      _prob[index] = quest::getProbAmp(this->reg(), index);
    return _prob;
  }

  /// Return the probabilities of the N indices of the state vector to equal the
  /// given value
  template<QEndianness _endianness = QEndianness::lsb>
  QArray<_qubits, qreal, QEndianness::lsb> outcome(
    const QBitArray<_qubits, _endianness>& bitarray =
      QBitArray<_qubits, _endianness>(0))
  {
    QArray<_qubits, qreal, QEndianness::lsb> _out;
    for (index_t index = 0; index < _qubits; ++index)
      _out[_qubits - index - 1] =
        quest::calcProbOfOutcome(this->reg(), index, bitarray[index]);
    return _out;
  }

  /// Execute quantum circuit locally on QuEST device asynchronously
  /// and return pointer to job
  QJob<QJobType::CXX>* execute_async(
    std::size_t shots = 0,
    std::function<void(QDevice_QuEST*)> func_init = NULL,
    std::function<void(QDevice_QuEST*)> func_before = NULL,
    std::function<void(QDevice_QuEST*)> func_after = NULL,
    QStream<QJobType::CXX>* stream = NULL)
  {
    auto runner = [&, shots]() -> void {
      if (func_init)
        func_init(this);
      if (func_before)
        func_before(this);
      for (auto it = _expr.begin(); it != _expr.end(); ++it)
        (*it)->operator()(*this);
      if (func_after)
        func_after(this);
    };

    if (stream != NULL)
      return stream->run(runner);
    else
      return _qstream_cxx.run(runner);
  }

  /// Execute quantum circuit locally on QuEST device synchronously
  /// and return pointer to job
  QJob<QJobType::CXX>* execute(
    std::size_t shots = 0,
    std::function<void(QDevice_QuEST*)> func_init = NULL,
    std::function<void(QDevice_QuEST*)> func_before = NULL,
    std::function<void(QDevice_QuEST*)> func_after = NULL,
    QStream<QJobType::CXX>* stream = NULL)
  {
    return execute_async(shots, func_init, func_before, func_after, stream)
      ->wait();
  }

  /// Execute quantum circuit locally on QuEST device synchronously
  /// and return result
  quest::Qureg& eval(std::size_t shots = 0,
                     std::function<void(QDevice_QuEST*)> func_init = NULL,
                     std::function<void(QDevice_QuEST*)> func_before = NULL,
                     std::function<void(QDevice_QuEST*)> func_after = NULL,
                     QStream<QJobType::CXX>* stream = NULL)
  {
    execute_async(shots, func_init, func_before, func_after, stream)->wait();
    return this->reg();
  }

public:
  /// Get state with highest probability from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>());

    return 0;
  }

  /// Get duration from internal data object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get() ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    assert(get<QResultType::status>());
    return std::chrono::duration<Rep, Period>(0);
  }

  /// Get histogram from internal data object
  template<QResultType _type, class T = std::size_t>
  static auto get() ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>());
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    return _histogram;
  }

  /// Get unique identifier from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>());
    return std::string("0");
  }

  /// Get success status from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return true;
  }

  /// Get time stamp from internal data object
  template<QResultType _type>
  static auto get() ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>());
    return std::time(nullptr);
  }
};

#else

/**
   @brief LibKet QuEST device class
*/
template<std::size_t _qubits>
class QDevice_QuEST : public QDevice_Dummy
{};

#endif

#define QDeviceDefineQuEST(_type)                                              \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_QuEST<_qubits>                                            \
  {                                                                            \
  public:                                                                      \
    QDevice(std::size_t shots = std::atoi(LibKet::getenv("QEEST_SHOTS",        \
                                                         "1024")))             \
      : QDevice_QuEST<_qubits>(shots)                                          \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("QUEST_SHOTS", "1024")))          \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

namespace device {

QDevicePropertyDefine(QDeviceType::quest, "QuEST", 26, true, QEndianness::lsb);

} // namespace device

QDeviceDefineQuEST(QDeviceType::quest);

} // namespace LibKet

#endif // QDEVICE_QUEST_HPP

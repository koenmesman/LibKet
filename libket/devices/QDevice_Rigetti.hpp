/** @file libket/devices/QDevice_Rigetti.hpp

    @brief LibKet Rigetti device class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QDEVICE_RIGETTI_HPP
#define QDEVICE_RIGETTI_HPP

#include <string>

#include <QArray.hpp>
#include <QBase.hpp>
#include <QDevice.hpp>
#include <QUtils.hpp>

namespace LibKet {

#ifdef LIBKET_WITH_QUIL
/**
   @brief LibKet Rigetti Quantum Cloud Servive (QCS) physical device class

   This class executes quantum circuits remotely on physical quantum
   devices made accessible through Rigetti's Quantum Cloud Service
   (QCS). It adopts Rigetti's Quantum Instruction Language
*/
template<std::size_t _qubits>
class QDevice_Rigetti : public QData<_qubits, QBackendType::Quil>
{
private:
  const std::string backend;
  const std::size_t shots;

  using Base = QData<_qubits, QBackendType::Quil>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_Rigetti(
    const std::string& backend = LibKet::getenv("RIGETTI_BACKEND", "1q-qvm"),
    std::size_t shots = std::atoi(LibKet::getenv("RIGETTI_SHOTS", "1024")))
    : backend(backend)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_Rigetti(const utils::json& config)
    : QDevice_Rigetti(config.find("backend") != config.end()
                        ? config["backend"]
                        : LibKet::getenv("RIGETTI_BACKEND", "1q-qvm"),
                      config.find("shots") != config.end()
                        ? config["shots"].get<size_t>()
                        : std::atoi(LibKet::getenv("RIGETTI_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Rigetti& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Rigetti& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on Rigetti's Quantum Cloud
  /// Service (QCS) asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    const std::string& script_init = "",
    const std::string& script_before = "",
    const std::string& script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\timport json\n"
       << "\tfrom pyquil import Program, get_qc\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tp = Program(qasm)\n"
       << "\tp.wrap_in_numshots_loop("
       << utils::to_string(shots > 0 ? shots : this->shots) << ")\n"
       << "\tqc = get_qc('" << backend << "')\n"
       << "\tep = qc.compile(p)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Execute circuit remotely
    ss << "\tresult = qc.run(ep)\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result.tolist())\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on Rigetti's Quantum Cloud
  /// Service (QCS) synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  const std::string& script_init = "",
                                  const std::string& script_before = "",
                                  const std::string& script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on Rigetti's Quantum Cloud
  /// Service (QCS) synchronously and result
  utils::json eval(std::size_t shots = 0,
                   const std::string& script_init = "",
                   const std::string& script_before = "",
                   const std::string& script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _histogram = get<QResultType::histogram>(result);
    std::size_t _value = 0;
    std::size_t _key = 0;

    for (std::size_t i = 0; i < _histogram.size(); ++i) {
      if (_histogram[i] > _value) {
        _value = _histogram[i];
        _key = i;
      }
    }

    return _key;
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    return std::chrono::duration<Rep, Period>(0);
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (std::size_t _shot = 0; _shot < result.size(); ++_shot) {
      std::bitset<_qubits> _value;
      for (std::size_t i = 0; i < _qubits; ++i)
        _value[i] = result[_shot][i].get<int>();
      _histogram[_value.to_ulong()]++;
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return std::string("0");
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return !result.empty();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    return std::time(nullptr);
  }
};

/**
   @brief LibKet Rigetti Quantum Virtual Machine (QVM) simulator device class

   This class executes quantum circuits locally on Rigetti's Quantum
   Virtual Machine (QVM). It adopts Rigetti's Quantum Instruction
   Language
*/
template<std::size_t _qubits>
class QDevice_Rigetti_simulator : public QData<_qubits, QBackendType::Quil>
{
private:
  std::string backend;
  std::size_t shots;

  using Base = QData<_qubits, QBackendType::Quil>;

public:
  /// Constructors from base class
  using Base::Base;

  /// Constructor from parameter list
  QDevice_Rigetti_simulator(
    const std::string& backend = LibKet::getenv("RIGETTI_BACKEND", "9q_square"),
    std::size_t shots = std::atoi(LibKet::getenv("RIGETTI_SHOTS", "1024")))
    : backend(backend)
    , shots(shots)
  {}

  /// Constructor from JSON object
  QDevice_Rigetti_simulator(const utils::json& config)
    : QDevice_Rigetti_simulator(
        config.find("backend") != config.end()
          ? config["backend"]
          : LibKet::getenv("RIGETTI_BACKEND", "9q_square"),
        config.find("shots") != config.end()
          ? config["shots"].get<size_t>()
          : std::atoi(LibKet::getenv("RIGETTI_SHOTS", "1024")))
  {}

  /// Apply expression to base type
  template<typename Expr>
  QDevice_Rigetti_simulator& operator()(const Expr& expr)
  {
    expr(*reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Apply string-based expression to base type
  QDevice_Rigetti_simulator& operator()(const std::string& expr)
  {
    gen_expression(expr, *reinterpret_cast<Base*>(this));
    return *this;
  }

  /// Execute quantum circuit remotely on Rigetti's Quantum Virtual
  /// Machine (QVM) asynchronously and return pointer to job
  QJob<QJobType::Python>* execute_async(
    std::size_t shots = 0,
    std::string script_init = "",
    std::string script_before = "",
    std::string script_after = "",
    QStream<QJobType::Python>* stream = NULL)
  {
    std::stringstream ss;

    ss << "def run():\n";

    // User-defined script to be performed before initialization
    if (!script_init.empty())
      ss << utils::string_ident(script_init, "\t");

    // Standard imports
    ss << "\timport json\n"
       << "\tfrom pyquil import Program, get_qc\n";

    // Select hardware backend and prepare quantum circuit
    ss << "\tqasm = '''\n"
       << Base::to_string() << "'''\n"
       << "\tp = Program(qasm)\n"
       << "\tp.wrap_in_numshots_loop("
       << utils::to_string(shots > 0 ? shots : this->shots) << ")\n"
       << "\tqc = get_qc('" << backend << "', as_qvm=True)\n"
       << "\tep = qc.compile(p)\n";

    // User-defined script to be performed righty before execution
    if (!script_before.empty())
      ss << utils::string_ident(script_before, "\t");

    // Execute circuit remotely
    ss << "\tresult = qc.run(ep)\n";

    // User-defined script to be performed right after execution
    if (!script_after.empty())
      ss << utils::string_ident(script_after, "\t");

    // Return result
    ss << "\treturn json.dumps(result.tolist())\n";

    QDebug << ss.str();

    try {
      if (stream != NULL)
        return stream->run(ss.str(), "run", "", "");
      else
        return _qstream_python.run(ss.str(), "run", "", "");
    } catch (std::exception& e) {
      QInfo << e.what() << std::endl;
      return NULL;
    }
  }

  /// Execute quantum circuit remotely on Rigetti's Quantum Virtual
  /// Machine (QVM) synchronously and return pointer to job
  QJob<QJobType::Python>* execute(std::size_t shots = 0,
                                  std::string script_init = "",
                                  std::string script_before = "",
                                  std::string script_after = "",
                                  QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->wait();
  }

  /// Execute quantum circuit remotely on Rigetti's Quantum Virtual
  /// Machine (QVM) synchronously and result
  utils::json eval(std::size_t shots = 0,
                   std::string script_init = "",
                   std::string script_before = "",
                   std::string script_after = "",
                   QStream<QJobType::Python>* stream = NULL)
  {
    return execute_async(
             shots, script_init, script_before, script_after, stream)
      ->get();
  }

public:
  /// Get state with highest probability from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::best, std::size_t>::type
  {
    assert(get<QResultType::status>(result));
    auto _histogram = get<QResultType::histogram>(result);
    std::size_t _value = 0;
    std::size_t _key = 0;

    for (std::size_t i = 0; i < _histogram.size(); ++i) {
      if (_histogram[i] > _value) {
        _value = _histogram[i];
        _key = i;
      }
    }

    return _key;
  }

  /// Get duration from JSON object
  template<QResultType _type, class Rep = double, class Period = std::ratio<1>>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::duration,
                            std::chrono::duration<Rep, Period>>::type
  {
    return std::chrono::duration<Rep, Period>(0);
  }

  /// Get histogram from JSON object
  template<QResultType _type, class T = std::size_t>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::histogram,
                            QArray<(1 << _qubits), T, QEndianness::lsb>>::type
  {
    assert(get<QResultType::status>(result));
    QArray<(1 << _qubits), T, QEndianness::lsb> _histogram;

    for (std::size_t _shot = 0; _shot < result.size(); ++_shot) {
      std::bitset<_qubits> _value;
      for (std::size_t i = 0; i < _qubits; ++i)
        _value[i] = result[_shot][i].get<int>();
      _histogram[_value.to_ulong()]++;
    }

    return _histogram;
  }

  /// Get unique identifier from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::id, std::string>::type
  {
    assert(get<QResultType::status>(result));
    return std::string("0");
  }

  /// Get success status from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::status, bool>::type
  {
    return !result.empty();
  }

  /// Get time stamp from JSON object
  template<QResultType _type>
  static auto get(const utils::json& result) ->
    typename std::enable_if<_type == QResultType::timestamp, std::time_t>::type
  {
    assert(get<QResultType::status>(result));

    return std::time(nullptr);
  }
};

#else

/**
   @brief LibKet Rigetti Quantum Cloud Servive (QCS) physical device class

   This class executes quantum circuits remotely on physical quantum
   devices made accessible through Rigetti's Quantum Cloud Service
   (QCS). It adopts Rigetti's Quantum Instruction Language
*/
template<std::size_t _qubits>
class QDevice_Rigetti : public QDevice_Dummy
{};

/**
   @brief LibKet Rigetti Quantum Virtual Machine (QVM) simulator device class

   This class executes quantum circuits locally on Rigetti's Quantum
   Virtual Machine (QVM). It adopts Rigetti's Quantum Instruction
   Language
*/
template<std::size_t _qubits>
class QDevice_Rigetti_simulator : public QDevice_Dummy
{};
#endif

#define QDeviceDefineRigetti(_type)                                            \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_Rigetti<_qubits>                                          \
  {                                                                            \
  public:                                                                      \
    QDevice(std::size_t shots = std::atoi(LibKet::getenv("RIGETTI_SHOTS",      \
                                                         "1024")))             \
      : QDevice_Rigetti<_qubits>(device::QDeviceProperty<_type>::name, shots)  \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("RIGETTI_SHOTS", "1024")))        \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

#define QDeviceDefineRigetti_simulator(_type)                                  \
  template<std::size_t _qubits>                                                \
  class QDevice<_type,                                                         \
                _qubits,                                                       \
                device::QDeviceProperty<_type>::simulator,                     \
                device::QDeviceProperty<_type>::endianness>                    \
    : public QDevice_Rigetti_simulator<_qubits>                                \
  {                                                                            \
  public:                                                                      \
    QDevice(std::size_t shots = std::atoi(LibKet::getenv("RIGETTI_SHOTS",      \
                                                         "1024")))             \
      : QDevice_Rigetti_simulator<_qubits>(                                    \
          device::QDeviceProperty<_type>::name,                                \
          shots)                                                               \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
                                                                               \
    QDevice(const utils::json& config)                                         \
      : QDevice(config.find("shots") != config.end()                           \
                  ? config["shots"].get<size_t>()                              \
                  : std::atoi(LibKet::getenv("RIGETTI_SHOTS", "1024")))        \
    {                                                                          \
      static_assert(_qubits <= device::QDeviceProperty<_type>::qubits,         \
                    "#qubits exceeds device capacity");                        \
    }                                                                          \
  };

namespace device {

// Rigetti Quantum Cloud Service (QCS) hardware devices
QDevicePropertyDefine(QDeviceType::rigetti_aspen_8,
                      "Aspen-8",
                      30,
                      false,
                      QEndianness::lsb);

// Rigetti Quantum Virtual Machine (QVM) simulator devices
QDevicePropertyDefine(QDeviceType::rigetti_aspen_8_simulator,
                      "Aspen-8",
                      30,
                      true,
                      QEndianness::lsb);

QDevicePropertyDefine(QDeviceType::rigetti_9q_square_simulator,
                      "9q-square",
                      9,
                      true,
                      QEndianness::lsb);
QDevicePropertyDefine(QDeviceType::rigetti_Xq_simulator,
                      "Xq",
                      31,
                      true,
                      QEndianness::lsb);

} // namespace device

// Rigetti Quantum Cloud Service (QCS) hardware devices
QDeviceDefineRigetti(QDeviceType::rigetti_aspen_8);

// Rigetti Quantum Virtual Machine (QVM) simulator devices
QDeviceDefineRigetti_simulator(QDeviceType::rigetti_aspen_8_simulator);

QDeviceDefineRigetti_simulator(QDeviceType::rigetti_9q_square_simulator);
QDeviceDefineRigetti_simulator(QDeviceType::rigetti_Xq_simulator);

} // namespace LibKet

#endif // QDEVICE_Rigetti_HPP

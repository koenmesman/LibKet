/** @file libket/gates/QGate_CPhaseKdag.hpp

    @brief LibKet quantum CPHASEKDAG (inverse controlled phase shift with pi/2^k
   angle) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Merel Schalkers
 */

#pragma once
#ifndef QGATE_CPHASEKDAG_HPP
#define QGATE_CPHASEKDAG_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) gate
class

The LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) gate
class implements the quantum controlled phase shift with pi/2^k angle
gate for an arbitrary number of quantum bits
*/

template<std::size_t k, typename _tol = QConst_t(0.0)>
class QCPhaseKdag : public QGate
{
public:
  /// Rotation angle 2*PI/2^k
  real_t static constexpr angle = 2 * M_PI / (1 << k);

  /// Inverse Rotation angle -2*PI/2^k
  real_t static constexpr inv_angle = -angle;

  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        data.append_kernel("CTRL(PH[" +
                           utils::to_string(inv_angle) + "]) q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "]\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        data.append_kernel("cirq.CZPowGate(exponent=" +
                           utils::to_string(inv_angle) + ").on(q[" +
                           utils::to_string(std::get<0>(i)) + "],q[" +
                           utils::to_string(std::get<1>(i)) + "])\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      std::string _expr = "cr q[";
      for (auto i : _filter0::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
      for (auto i : _filter1::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter1::range(data).end() - 1) ? "," : "], ");
      _expr += utils::to_string(inv_angle) + "\n";
      data.append_kernel(_expr);
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        data.append_kernel("cu1(" +
                           utils::to_string(inv_angle) + ") q[" +
                           utils::to_string(std::get<0>(i)) + "], q[" +
                           utils::to_string(std::get<1>(i)) + "];\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    std::cerr << "The CR/CPHASEK gate is not implemented for OpenQL!!!\n";
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        data.append_kernel(
                           [&]() { data.kernel().controlled_rz(std::get<0>(i),
                                                               std::get<1>(i),
                                                               inv_angle); });
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        data.append_kernel("\tcphase" +
                           utils::to_string(k) + " q" +
                           utils::to_string(std::get<0>(i)) + ",q" +
                           utils::to_string(std::get<1>(i)) + " # -2pi/2^" +
                           utils::to_string(k) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        data.append_kernel("CPHASE (" +
                           utils::to_string(inv_angle) + ") " +
                           utils::to_string(std::get<0>(i)) + " " +
                           utils::to_string(std::get<1>(i)) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        quest::controlledPhaseShift(data.reg(),
                                    std::get<0>(i),
                                    std::get<1>(i),
                                    (qreal)inv_angle);
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    static_assert(_filter0::template size<_qubits>() ==
                    _filter1::template size<_qubits>(),
                  "CPHASEKDAG gate can only be applied to quantum objects of "
                  "the same size");
    if (tolerance(angle, _tol{})) {
      for (auto i : utils::zip(_filter0::range(data),
                               _filter1::range(data)))
        data.append_kernel(
                           new qx::ctrl_phase_shift(std::get<0>(i),
                                                    std::get<1>(i),
                                                    (double)inv_angle));
    }
    return data;
  }
#endif
};

// Complete type definition
template<std::size_t k, typename _tol>
real_t constexpr QCPhaseKdag<k, _tol>::angle;
  
/// Serialize operator
template<std::size_t k, typename _tol = QConst_t(0.0)>
std::ostream&
operator<<(std::ostream& os, const QCPhaseKdag<k, _tol>& gate)
{
  os << "crkdag<" << std::to_string(k) << "," << _tol::to_type() << ">(";
  return os;
}

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) gate creator

This overload of the LibKet::gates::cphasekdag() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cphasekdag();
\endcode
*/
template<std::size_t k, typename _tol = QConst_t(0.0)>
inline constexpr auto
cphasekdag() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCPhaseKdag<k, _tol>>(
    filters::QFilter{}, filters::QFilter{});
}


namespace detail {
template<typename _tol, std::size_t k, typename _expr0, typename _expr1, typename = void>
struct cphasekdag_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QCPhaseKdag<k, _tol>,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
  template<typename _tol, std::size_t k,
           typename __expr0_0,
           typename __expr0_1,
           typename __filter0,
           typename __expr1_0,
           typename __expr1_1,
           typename __filter1>
  struct cphasekdag_impl<_tol, k,
    BinaryQGate<__expr0_0, __expr0_1, QCPhaseKdag<k, _tol>, __filter0>,
    BinaryQGate<__expr1_0, __expr1_1, QCPhaseKdag<k, _tol>, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
                                     const BinaryQGate<__expr0_0, __expr0_1, QCPhaseKdag<k, _tol>, __filter0>& expr0,
                                     const BinaryQGate<__expr1_0, __expr1_1, QCPhaseKdag<k, _tol>, __filter1>& expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return cphasekdag_impl<_tol, k, decltype(typename filters::getFilter<__filter0>::type{}(
                       all(expr1.expr1(all(
                         expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                     typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return cphasekdag_impl<_tol, k, typename filters::getFilter<__filter0>::type,
                     decltype(typename filters::getFilter<__filter1>::type{}(
                       all(expr0.expr0(all(
                         expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cphasekdag_impl<_tol, k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cphasekdag_impl<_tol, k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cphasekdag_impl<_tol, k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cphasekdag_impl<_tol, k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cphasekdag_impl<_tol, k,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cphasekdag_impl<_tol, k,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cphasekdag_impl<_tol, k,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cphasekdag_impl<_tol, k,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cphasekdag_impl<_tol, k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCPhaseKdag here */
    !std::is_same<typename gates::getGate<_expr0>::type, QCPhaseKdag>::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cphasekdag_impl<_tol, k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cphasekdag_impl<_tol, k,
  _expr0,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CPhaseKdag gate is handled explicitly */
    (!std::is_base_of<
      QCPhaseKdag<k, _tol>,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cphasekdag_impl<_tol, k,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCPhaseKdag here */
    !std::is_same<typename gates::getGate<_expr1>::type, QCPhaseKdag<k, _tol>>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cphasekdag_impl<_tol, k,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

   This overload of the LibKet::gates::cphasekdag() function
   eliminates the double-application of the CPhaseKdag gate
*/
template<typename _tol, std::size_t k, typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cphasekdag_impl<_tol, k,
  BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CPhaseKdag gate is handled explicitly */
    (!std::is_base_of<
      QCPhaseKdag<k, _tol>,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCPhaseKdag<k, _tol>, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CPhaseKdag gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

This overload of the LibKet::gates::cphasekdag() function accepts
two expressions as constant reference
*/
template<std::size_t k, typename _tol = QConst_t(0.0),
         typename _expr0, typename _expr1>
inline constexpr auto
cphasekdag(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return detail::cphasekdag_impl<_tol, k, _expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

This overload of the LibKet::gates::cphasekdag() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<std::size_t k, typename _tol = QConst_t(0.0),
         typename _expr0, typename _expr1>
inline constexpr auto
cphasekdag(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return detail::cphasekdag_impl<_tol, k, _expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

This overload of the LibKet::gates::cphasekdag() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<std::size_t k, typename _tol = QConst_t(0.0),
         typename _expr0, typename _expr1>
inline constexpr auto
cphasekdag(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return detail::cphasekdag_impl<_tol, k, _expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) creator

This overload of the LibKet::gates::cphasekdag() function accepts
two expression as universal reference
*/
template<std::size_t k, typename _tol = QConst_t(0.0),
         typename _expr0, typename _expr1>
inline constexpr auto
cphasekdag(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return detail::cphasekdag_impl<_tol, k, _expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) gate creator

Function alias for LibKet::gates::cphasek
*/
template<std::size_t k, typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CPHASEKDAG(Args&&... args)
{
  return cphasekdag<k, _tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) gate creator

Function alias for LibKet::gates::cphasek
*/
template<std::size_t k, typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
crkdag(Args&&... args)
{
  return cphasekdag<k, _tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet CPHASEKDAG (inverse controlled phase shift with pi/2^k angle) gate creator

Function alias for LibKet::gates::cphasek
*/
template<std::size_t k, typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
CRKDAG(Args&&... args)
{
  return cphasekdag<k, _tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseKdag<k, _tol>::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cphasekdag<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseKdag<k, _tol>::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cphasekdag<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseKdag<k, _tol>::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cphasekdag<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<std::size_t k, typename _tol>
template<typename T0, typename T1>
inline constexpr auto
QCPhaseKdag<k, _tol>::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cphasekdag<k, _tol>(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCPhaseKdag objects
*/
template<std::size_t level = 1, std::size_t k, typename _tol>
inline static auto
show(const QCPhaseKdag<k, _tol>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QCPhaseKdag pi/2^" << utils::to_string(k) << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CPHASEKDAG_HPP

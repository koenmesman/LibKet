/** @file libket/gates/QGate_CUnitary2.hpp

    @brief LibKet 2x2 controlled unitary gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CUNITARY2_HPP
#define QGATE_CUNITARY2_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet 2x2 controlled unitary gate class

The 2x2 controlled unitary gate accepts an arbitrary \f$2\times 2\f$
unitary matrix \f$U\f$ as input and performs the ZYZ decomposition
\f[
U = e^{i\Phi} R_z(\alpha)R_y(\beta)R_z(\gamma)
\f]
Here, \f$\Phi,\alpha,\beta,\gamma\f$ are rotation angles.
*/
template<typename _functor>
class QCUnitary2 : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function
  template<std::size_t _qubits,
           typename _filter0,
           typename _filter1,
           QBackendType _qbackend>
  inline static QData<_qubits, _qbackend>& apply(
    QData<_qubits, _qbackend>& data)
  {
    auto u = _functor{}();
    decltype(u) x = { 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0,
                      0.0, 0.0, 1.0, 1.0, 0.0, 1.0 };

    if (!optim::de(x,
                   QCUnitary2<_functor>::controlled_unitary_fn<
                     typename decltype(u)::elem_type>,
                   &u))
      throw std::runtime_error(
        "QCUnitary2: An error occured in the decomposition");

    QInfo << x << std::endl;
    // @Merel: Please add your code here

    gen_expression("gototag<0>(i(all(tag<0>())))", data);
    return data;
  }

private:
  template<typename T = real_t>
  static T controlled_unitary_fn(const arma::Col<T>& vals_inp,
                                 arma::Col<T>* grad_out,
                                 void* opt_data)
  {
    // Get the matrix values out of the input vector
    arma::Col<T> u_data = *(arma::Col<T>*)(opt_data);

    T u_1 = u_data(0);
    T u_2 = u_data(1);
    T u_3 = u_data(2);
    T u_4 = u_data(3);

    // Name the seperate components of the to find output vector to
    // make the calculation more readable and a one-on-one copy of the
    // original way of writing it out.
    T a_1 = vals_inp(0);
    T a_2 = vals_inp(1);
    T a_3 = vals_inp(2);
    T a_4 = vals_inp(3);

    T b_1 = vals_inp(4);
    T b_2 = vals_inp(5);
    T b_3 = vals_inp(6);
    T b_4 = vals_inp(7);

    T c_1 = vals_inp(8);
    T c_2 = vals_inp(9);
    T c_3 = vals_inp(10);
    T c_4 = vals_inp(11);

    T alpha = vals_inp(12);

    // Define the necessary imaginary number
    std::complex<T> mycomplex(0, 1.0);

    using namespace std;
    T obj_val = abs(exp(imag(mycomplex) * alpha) *
                      ((a_2 * b_2 * c_1) + (a_1 * b_4 * c_1) +
                       (a_2 * b_1 * c_3) + (a_1 * b_3 * c_3)) -
                    u_1) +
                abs(exp(imag(mycomplex) * alpha) *
                      ((a_2 * b_2 * c_2) + (a_1 * b_3 * c_2) +
                       (a_2 * b_1 * c_4) + (a_1 * b_3 * c_4)) -
                    u_2) +
                abs(exp(imag(mycomplex) * alpha) *
                      ((a_4 * b_2 * c_1) + (a_3 * b_4 * c_1) +
                       (a_4 * b_1 * c_3) + (a_3 * b_3 * c_3)) -
                    u_3) +
                abs(exp(imag(mycomplex) * alpha) *
                      ((a_4 * b_2 * c_2) + (a_3 * b_4 * c_2) +
                       (a_4 * b_1 * c_4) + (a_3 * b_3 * c_4)) -
                    u_4) +
                abs(a_1 * b_1 * c_1 + a_2 * b_3 * c_1 + a_1 * b_2 * c_3 +
                    a_2 * b_4 * c_3 - 1.0) +
                abs(a_1 * b_1 * c_2 + a_2 * b_3 * c_2 + a_1 * b_2 * c_4 +
                    a_2 * b_4 * c_4) +
                abs(a_3 * b_1 * c_1 + a_4 * b_3 * c_1 + a_3 * b_2 * c_3 +
                    a_4 * b_4 * c_3) +
                abs(a_3 * b_1 * c_2 + a_4 * b_3 * c_2 + a_3 * b_2 * c_4 +
                    a_4 * b_4 * c_4 - 1.0);

    return obj_val;
  }
};

/**
@brief LibKet 2x2 controlled unitary gate creator

This overload of the LibKet::gates::cunitary2() function can be used
as terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cunitary2();
\endcode
*/
template<typename _functor>
inline constexpr auto
cunitary2() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCUnitary2<_functor>>(
    filters::QFilter{}, filters::QFilter{});
}

/**
@brief LibKet 2x2 controlled unitary gate creator

This overload of the LibKet::gates::cunitary2() function accepts two
expressions as constant reference
*/
template<typename _functor, typename _expr0, typename _expr1>
inline constexpr auto
cunitary2(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCUnitary2<_functor>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet 2x2 controlled unitary gate creator

This overload of the LibKet::gates::cunitary2() function accepts the
first expression as constant reference and the second expression as
universal reference
*/
template<typename _functor, typename _expr0, typename _expr1>
inline constexpr auto
cunitary2(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCUnitary2<_functor>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet 2x2 controlled unitary gate creator

This overload of the LibKet::gates::cunitary2() function accepts the
first expression as universal reference and the second expression as
constant reference
*/
template<typename _functor, typename _expr0, typename _expr1>
inline constexpr auto
cunitary2(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCUnitary2<_functor>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet 2x2 controlled unitary gate creator

This overload of the LibKet::gates::cunitary2() function accepts two
expression as universal reference
*/
template<typename _functor, typename _expr0, typename _expr1>
inline constexpr auto
cunitary2(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return BinaryQGate<_expr0,
                     _expr1,
                     QCUnitary2<_functor>,
                     decltype(typename filters::getFilter<_expr0>::type{}
                              << typename filters::getFilter<_expr1>::type{})>(
    expr0, expr1);
}

/**
@brief LibKet 2x2 controlled unitary gate creator

Function alias for LibKet::gates::cunitary2
*/
template<typename _functor, typename... Args>
inline constexpr auto
cu2(Args&&... args)
{
  return cunitary2<_functor>(std::forward<Args>(args)...);
}

/**
@brief LibKet 2x2 controlled unitary gate creator

Function alias for LibKet::gates::cunitary2
*/
template<typename _functor, typename... Args>
inline auto
CUNITARY2(Args&&... args)
{
  return cunitary2<_functor>(std::forward<Args>(args)...);
}

/**
@brief LibKet 2x2 controlled unitary gate creator

Function alias for LibKet::gates::cunitary2
*/
template<typename _functor, typename... Args>
inline auto
CU2(Args&&... args)
{
  return cunitary2<_functor>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _functor>
template<typename T>
inline constexpr auto
QCUnitary2<_functor>::operator()(const T& t) const noexcept
{
  return cunitary2<_functor>(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename _functor>
template<typename T>
inline constexpr auto
QCUnitary2<_functor>::operator()(T&& t) const noexcept
{
  return cunitary2<_functor>(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QCUnitary2 objects
*/
template<std::size_t level = 1, typename _functor>
inline static auto
show(const QCUnitary2<_functor>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QCUnitary2<" << _functor::hash << ">\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_UNITARY2_HPP

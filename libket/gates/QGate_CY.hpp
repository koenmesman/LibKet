/** @file libket/gates/QGate_CY.hpp

    @brief LibKet quantum CY (controlled-Y gate) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CY_HPP
#define QGATE_CY_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>
#include <gates/QGate_CNOT.hpp>
#include <gates/QGate_S.hpp>
#include <gates/QGate_Sdag.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CY (controlled-Y) gate class

The LibKet CY (controlled-Y) gate class implements the quantum
controlled-Y gate for an arbitrary number of quantum bits
*/

class QCY : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("CTRL(Y) q[" +
                         utils::to_string(std::get<0>(i)) + "],q[" +
                         utils::to_string(std::get<1>(i)) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("cirq.ControlledGate(cirq.Y).on(q[" +
                         utils::to_string(std::get<0>(i)) + "],q[" +
                         utils::to_string(std::get<1>(i)) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    auto expr = S(filters::gototag<1>(
      CNOT(filters::tag<0>(_filter0{}), Sdag(filters::tag<1>(_filter1{})))));
    return expr(data);
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("cy q[" +
                         utils::to_string(std::get<0>(i)) + "], q[" +
                         utils::to_string(std::get<1>(i)) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    auto expr = S(filters::gototag<1>(
      CNOT(filters::tag<0>(_filter0{}), Sdag(filters::tag<1>(_filter1{})))));
    return expr(data);
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    for (auto i : _filter0::range(data))
      data.append_kernel("\tcy q" +
                         utils::to_string(std::get<0>(i)) + ",q" +
                         utils::to_string(std::get<1>(i)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("CY " +
                         utils::to_string(std::get<0>(i)) + " " +
                         utils::to_string(std::get<1>(i)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      quest::controlledPauliY(data.reg(),
                              std::get<0>(i),
                              std::get<1>(i));

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CY gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel(new qx::bin_ctrl(std::get<0>(i),
                                          new qx::pauli_y(std::get<1>(i))));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QCY& gate)
{
  os << "cy(";
  return os;
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cy();
\endcode
*/
inline constexpr auto
cy() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCY>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
template<typename _expr0, typename _expr1, typename = void>
struct cy_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QCY,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct cy_impl<
  BinaryQGate<__expr0_0, __expr0_1, QCY, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QCY, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QCY, __filter0>& expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QCY, __filter1>& expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return cy_impl<decltype(typename filters::getFilter<__filter0>::type{}(
                       all(expr1.expr1(all(
                         expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                     typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return cy_impl<typename filters::getFilter<__filter0>::type,
                     decltype(typename filters::getFilter<__filter1>::type{}(
                       all(expr0.expr0(all(
                         expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cy_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cy_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cy_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cy_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cy_impl<
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cy_impl<
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cy_impl<
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cy_impl<
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cy_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCY here */
    !std::is_same<typename gates::getGate<_expr0>::type, QCY>::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cy_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cy_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CY gate is handled explicitly */
    (!std::is_base_of<
      QCY,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cy_impl<
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCY here */
    !std::is_same<typename gates::getGate<_expr1>::type, QCY>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cy_impl<
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief LibKet CY (controlled-Y) gate creator

   This overload of the LibKet::gates::cy() function
   eliminates the double-application of the CY gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cy_impl<
  BinaryQGate<__expr0, __expr1, QCY, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CY gate is handled explicitly */
    (!std::is_base_of<
      QCY,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCY, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CY  gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts
two expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return detail::cy_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return detail::cy_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return detail::cy_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

This overload of the LibKet::gates::cy() function accepts
two expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cy(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return detail::cy_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CY (controlled-Y) gate creator

Function alias for LibKet::gates::cy
*/
template<typename... Args>
inline constexpr auto
CY(Args&&... args)
{
  return cy(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename T0, typename T1>
inline constexpr auto
QCY::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cy(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCY objects
*/
template<std::size_t level = 1>
inline static auto
show(const QCY& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QCY"
     << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CY_HPP

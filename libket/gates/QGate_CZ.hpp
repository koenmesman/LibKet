/** @file libket/gates/QGate_CZ.hpp

    @brief LibKet quantum CZ (controlled-Z gate) class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_CZ_HPP
#define QGATE_CZ_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>
#include <gates/QGate_CNOT.hpp>
#include <gates/QGate_Hadamard.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet CZ (controlled-Z) gate class

The LibKet CZ (controlled-Z) gate class implements the quantum
controlled-Z gate for an arbitrary number of quantum bits
*/

class QCZ : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, const T1& t1) const noexcept;

  /// Operator() - by constant and universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(const T0& t0, T1&& t1) const noexcept;

  /// Operator() - by universal and constant reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, const T1& t1) const noexcept;

  /// Operator() - by universal reference
  template<typename T0, typename T1>
  inline constexpr auto operator()(T0&& t0, T1&& t1) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("CTRL(Z) q[" +
                         utils::to_string(std::get<0>(i)) + "],q[" +
                         utils::to_string(std::get<1>(i)) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("cirq.CZ.on(q[" +
                         utils::to_string(std::get<0>(i)) + "],q[" +
                         utils::to_string(std::get<1>(i)) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    std::string _expr = "cz q[";
    for (auto i : _filter0::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter0::range(data).end() - 1) ? "," : "], q[");
    for (auto i : _filter1::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter1::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("cz q[" +
                         utils::to_string(std::get<0>(i)) + "], q[" +
                         utils::to_string(std::get<1>(i)) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel([&]() { data.kernel().cz(std::get<0>(i),
                                                  std::get<1>(i)); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("\tcz q" +
                         utils::to_string(std::get<0>(i)) + ",q" +
                         utils::to_string(std::get<1>(i)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel("CZ " +
                         utils::to_string(std::get<0>(i)) + " " +
                         utils::to_string(std::get<1>(i)) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    auto expr = H(filters::gototag<1>(
      CNOT(filters::tag<0>(_filter0{}), H(filters::tag<1>(_filter1{})))));
    return expr(data);
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter0, typename _filter1>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    static_assert(
      _filter0::template size<_qubits>() == _filter1::template size<_qubits>(),
      "CZ gate can only be applied to quantum objects of the same size");
    for (auto i : utils::zip(_filter0::range(data),
                             _filter1::range(data)))
      data.append_kernel(new qx::bin_ctrl(std::get<0>(i),
                                          new qx::pauli_z(std::get<1>(i))));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QCZ& gate)
{
  os << "cz(";
  return os;
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function can be used as
terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::cz();
\endcode
*/
inline constexpr auto
cz() noexcept
{
  return BinaryQGate<filters::QFilter, filters::QFilter, QCZ>(
    filters::QFilter{}, filters::QFilter{});
}

namespace detail {
template<typename _expr0, typename _expr1, typename = void>
struct cz_impl
{
  inline static constexpr auto apply(const _expr0& expr0, const _expr1& expr1)
  {
    return BinaryQGate<_expr0,
                       _expr1,
                       QCZ,
                       decltype(typename filters::getFilter<_expr0>::type{} <<
                                typename filters::getFilter<_expr1>::type{})>(
      expr0, expr1);
  }
};
} // namespace detail

#ifdef LIBKET_OPTIMIZE_GATES

namespace detail {

/**
   @brief LibKet CZ (controlled-z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0_0,
         typename __expr0_1,
         typename __filter0,
         typename __expr1_0,
         typename __expr1_1,
         typename __filter1>
struct cz_impl<
  BinaryQGate<__expr0_0, __expr0_1, QCZ, __filter0>,
  BinaryQGate<__expr1_0, __expr1_1, QCZ, __filter1>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<__expr0_0>::type,
                   typename filters::getFilter<__expr1_0>::type>::value &&
      std::is_same<typename filters::getFilter<__expr0_1>::type,
                   typename filters::getFilter<__expr1_1>::type>::value))>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0_0, __expr0_1, QCZ, __filter0>& expr0,
    const BinaryQGate<__expr1_0, __expr1_1, QCZ, __filter1>& expr1)
  {
#ifdef LIBKET_L2R_EVALUATION
    return cz_impl<decltype(typename filters::getFilter<__filter0>::type{}(
                       all(expr1.expr1(all(
                         expr1.expr0(all(expr0.expr1(all(expr0.expr0))))))))),
                     typename filters::getFilter<__filter1>::type>::
      apply(typename filters::getFilter<__filter0>::type{}(all(expr1.expr1(
              all(expr1.expr0(all(expr0.expr1(all(expr0.expr0)))))))),
            typename filters::getFilter<__filter1>::type{});
#else
    return cz_impl<typename filters::getFilter<__filter0>::type,
                     decltype(typename filters::getFilter<__filter1>::type{}(
                       all(expr0.expr0(all(
                         expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))))>::
      apply(typename filters::getFilter<__filter0>::type{},
            typename filters::getFilter<__filter1>::type{}(all(expr0.expr0(
              all(expr0.expr1(all(expr1.expr0(all(expr1.expr1)))))))));
#endif
  }
};

/**
   @brief LibKet CZ (controlled-z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cz_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{});
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cz_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr0)));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cz_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<_expr0>::type{} <<
             typename filters::getFilter<__filter>::type{})(all(expr1.expr1)));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cz_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr0>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return (typename filters::getFilter<_expr0>::type{}
            << typename filters::getFilter<__filter>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr1.expr1(all(expr1.expr0)))
#else
      all(expr1.expr0(all(expr1.expr1)))
#endif
    );
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cz_impl<
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<filters::QFilter,
                  typename std::template decay<__expr0>::type>::value&& std::
         is_base_of<filters::QFilter,
                    typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{});
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cz_impl<
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value)>::
    type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr0)));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cz_impl<
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr0.expr1)));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cz_impl<
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<filters::QFilter,
                     typename std::template decay<_expr1>::type>::value&& std::
       is_base_of<QGate, typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate, typename std::template decay<__expr1>::type>::
           value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return (typename filters::getFilter<__filter>::type{}
            << typename filters::getFilter<_expr1>::type{})(
#ifdef LIBKET_L2R_EVALUATION
      all(expr0.expr1(all(expr0.expr0)))
#else
      all(expr0.expr0(all(expr0.expr1)))
#endif
    );
  }
};

#ifdef LIBKET_L2R_EVALUATION

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cz_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCZ here */
    !std::is_same<typename gates::getGate<_expr0>::type, QCZ>::value>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(all(expr0)));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cz_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr0(all(expr0)))));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename _expr0, typename __expr0, typename __expr1, typename __filter>
struct cz_impl<
  _expr0,
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr0>::type,
                   typename filters::getFilter<__expr0>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr1>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr0>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CZ gate is handled explicitly */
    (!std::is_base_of<
      QCZ,
      typename std::template decay<_expr0>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const _expr0& expr0,
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<_expr0>::type{}
             << typename filters::getFilter<__filter>::type{})(
      all(expr1.expr1(all(expr0)))));
  }
};

#else // not LIBKET_L2R_EVALUATION

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cz_impl<
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<filters::QFilter,
                         typename std::template decay<__expr1>::type>::value) &&
    /* Exclude QCZ here */
    !std::is_same<typename gates::getGate<_expr1>::type, QCZ>::value>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(all(expr1)));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cz_impl<
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<QGate, typename std::template decay<__expr0>::type>::
         value&& std::is_base_of<
           filters::QFilter,
           typename std::template decay<__expr1>::type>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr0(all(expr1)))));
  }
};

/**
   @brief LibKet CZ (controlled-Z) gate creator

   This overload of the LibKet::gates::cz() function
   eliminates the double-application of the CZ gate
*/
template<typename __expr0, typename __expr1, typename __filter, typename _expr1>
struct cz_impl<
  BinaryQGate<__expr0, __expr1, QCZ, __filter>,
  _expr1,
  typename std::enable_if<
    /* Check for valid selection of qubits */
    ((std::is_same<typename filters::getFilter<_expr1>::type,
                   typename filters::getFilter<__expr1>::type>::value &&
      std::is_same<typename filters::getFilter<__filter>::type,
                   typename filters::getFilter<__expr0>::type>::value)) &&
    /* Check for combination of expressions */
    (std::is_base_of<QGate, typename std::template decay<_expr1>::type>::value&&
       std::is_base_of<filters::QFilter,
                       typename std::template decay<__expr0>::type>::value&&
         std::is_base_of<QGate,
                         typename std::template decay<__expr1>::type>::value) &&
    /* CZ gate is handled explicitly */
    (!std::is_base_of<
      QCZ,
      typename std::template decay<_expr1>::type::gate_t>::value)>::type>
{
  inline static constexpr auto apply(
    const BinaryQGate<__expr0, __expr1, QCZ, __filter>& expr0,
    const _expr1& expr1)
  {
    // Restore filter to the state that would result from the
    // double-application of the CZ  gate
    return ((typename filters::getFilter<__filter>::type{}
             << typename filters::getFilter<_expr1>::type{})(
      all(expr0.expr1(all(expr1)))));
  }
};

#endif // LIBKET_L2R_EVALUATION

} // namespace details

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts
two expressions as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(const _expr0& expr0, const _expr1& expr1) noexcept
{
  return detail::cz_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts the
first expression as constant reference and the second
expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(const _expr0& expr0, _expr1&& expr1) noexcept
{
  return detail::cz_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts the
first expression as universal reference and the second
expression as constant reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(_expr0&& expr0, const _expr1& expr1) noexcept
{
  return detail::cz_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

This overload of the LibKet::gates::cz() function accepts
two expression as universal reference
*/
template<typename _expr0, typename _expr1>
inline constexpr auto
cz(_expr0&& expr0, _expr1&& expr1) noexcept
{
  return detail::cz_impl<_expr0, _expr1>::apply(expr0, expr1);
}

/**
@brief LibKet CZ (controlled-Z) gate creator

Function alias for LibKet::gates::cz
*/
template<typename... Args>
inline constexpr auto
CZ(Args&&... args)
{
  return cz(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(const T0& t0, const T1& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by constant and universal reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(const T0& t0, T1&& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal and constant reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(T0&& t0, const T1& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/// Operator() - by universal reference
template<typename T0, typename T1>
inline constexpr auto
QCZ::operator()(T0&& t0, T1&& t1) const noexcept
{
  return cz(std::forward<T0>(t0), std::forward<T1>(t1));
}

/**
   @brief LibKet show gate type - specialization for QCZ objects
*/
template<std::size_t level = 1>
inline static auto
show(const QCZ& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QCZ"
     << "\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_CZ_HPP

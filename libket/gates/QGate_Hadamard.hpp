/** @file libket/gates/QGate_Hadamard.hpp

    @brief LibKet quantum Hadamard class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_HADAMARD_HPP
#define QGATE_HADAMARD_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Hadamard gate class

The LibKet Hadamard gate class implements the quantum Hadamard
gate for an arbitrary number of quantum bits
*/

class QHadamard : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("H q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("cirq.H.on(q[" + utils::to_string(i) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "h q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("h q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().hadamard(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\th q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("H " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    for (auto i : _filter::range(data))
      quest::hadamard(data.reg(), i);

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::hadamard(i));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QHadamard& gate)
{
  os << "h(";
  return os;
}

/**
@brief LibKet Hadamard gate creator

This overload of the LibKet::gates::hadamard() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::hadamard();
\endcode
*/
inline constexpr auto
hadamard() noexcept
{
  return UnaryQGate<filters::QFilter, QHadamard>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Hadamard gate creator

This overload of the LibKet::gates::hadamard() function
eliminates the double-application of the Hadamard gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
hadamard(
  const UnaryQGate<_expr, QHadamard, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Hadamard gate creator

This overload of the LibKet::gates::hadamard() function
eliminates the double-application of the Hadamard gate
*/
template<typename _expr>
inline constexpr auto
hadamard(
  UnaryQGate<_expr, QHadamard, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Hadamard gate creator

This overload of the LibKet::gates::hadamard() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
hadamard(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QHadamard, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet Hadamard gate creator

This overload of the LibKet::gates::hadamard() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
hadamard(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QHadamard, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet Hadamard gate creator

Function alias for LibKet::gates::hadamard
*/
template<typename... Args>
inline constexpr auto
HADAMARD(Args&&... args)
{
  return hadamard(std::forward<Args>(args)...);
}

/**
@brief LibKet Hadamard gate creator

Function alias for LibKet::gates::hadamard
*/
template<typename... Args>
inline constexpr auto
h(Args&&... args)
{
  return hadamard(std::forward<Args>(args)...);
}

/**
@brief LibKet Hadamard gate creator

Function alias for LibKet::gates::hadamard
*/
template<typename... Args>
inline constexpr auto
H(Args&&... args)
{
  return hadamard(std::forward<Args>(args)...);
}

/**
@brief LibKet Hadamarddag gate creator

Function alias for LibKet::gates::hadamarddag
 */
template<typename... Args>
inline constexpr auto
hadamarddag(Args&&... args)
{
  return hadamard(std::forward<Args>(args)...);
}

/**
@brief LibKet Hadamarddag gate creator

Function alias for LibKet::gates::hadamarddag
 */
template<typename... Args>
inline constexpr auto
HADAMARDdag(Args&&... args)
{
  return hadamard(std::forward<Args>(args)...);
}

/**
@brief LibKet Hadamarddag gate creator

Function alias for LibKet::gates::hadamarddag
 */
template<typename... Args>
inline constexpr auto
hdag(Args&&... args)
{
  return hadamard(std::forward<Args>(args)...);
}

/**
@brief LibKet Hadamarddag gate creator

Function alias for LibKet::gates::hadamarddag
 */
template<typename... Args>
inline constexpr auto
Hdag(Args&&... args)
{
  return hadamard(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QHadamard::operator()(const T& t) const noexcept
{
  return hadamard(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QHadamard::operator()(T&& t) const noexcept
{
  return hadamard(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QHadamard objects
*/
template<std::size_t level = 1>
inline static auto
show(const QHadamard& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QHadamard\n";

  return gate;
}

} // namespace gates

/**
@brief LibKet dagger creator

This overload of the LibKet::dagger() function returns the Hadamard gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
dagger(
       const gates::UnaryQGate<_expr, gates::QHadamard, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return expr;
}

} // namespace LibKet

#endif // QGATE_HADAMARD_HPP

/** @file libket/gates/QGate_Hook.hpp

    @brief LibKet quantum 'hook' class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef QGATE_HOOK_HPP
#define QGATE_HOOK_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
   @brief LibKet 'hook' gate class

   The LibKet 'hook' gate class implements a universal quantum
   gate that allows the user to inject code that is executed when
   the gate is applied
*/

template<typename _functor, typename _functor_dag=_functor>
class QHook : public QGate
{
private:
  const _functor     functor;
  const _functor_dag functor_dag;

public:
  QHook()
    : functor(_functor{}),
      functor_dag(_functor_dag{})
  {}

  QHook(const _functor& functor, const _functor_dag& functor_dag)
    : functor(functor), functor_dag(functor_dag)
  {}

  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function - specialization for LibKet expressions
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static auto apply(QData<_qubits, _qbackend>& data) noexcept ->
    typename std::enable_if<_functor::isExpr, QData<_qubits, _qbackend>>::type&
  {
    auto expr = _functor{}();
    return expr(data);
  }

  /// Apply function - specialization for non-LibKet expressions
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static auto apply(QData<_qubits, _qbackend>& data) noexcept ->
    typename std::enable_if<!_functor::isExpr, QData<_qubits, _qbackend>>::type&
  {
    std::string hook = _functor{}();
    for (auto i : _filter::range(data)) {
      for (auto c : hook)
        data.append_kernel((c == '#' ? std::to_string(i) : std::string(1, c)));
    }
    return data;
  }
};

/// Serialize operator
  template<typename _functor, typename _functor_dag>
std::ostream&
  operator<<(std::ostream& os, const QHook<_functor, _functor_dag>& gate)
{
  os << _functor{};
  return os;
}

/**
   @brief LibKet 'hook' gate creator

   This overload of the LibKet::gates::hook() function can be used
   as terminal, i.e. the inner-most gate in a quantum expression

   \code
   auto expr = gates::hook();
   \endcode
*/
  template<typename _functor, typename _functor_dag=_functor>
inline constexpr auto
hook() noexcept
{
  return UnaryQGate<filters::QFilter, QHook<_functor, _functor_dag>>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet 'hook' gate creator

This overload of the LibKet::gates::hook() function
eliminates the double-application of the 'hook' gate and its adjoint
*/
  template<typename _functor, typename _functor_dag, typename _expr, typename _filter>
inline constexpr auto
hook(
     const UnaryQGate<_expr, QHook<_functor_dag, _functor>, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet 'hook' gate creator

This overload of the LibKet::gates::hook() function
eliminates the double-application of the 'hook' gate and its adjoint
*/
  template<typename _functor, typename _functor_dag, typename _expr>
inline constexpr auto
hook(
     UnaryQGate<_expr, QHook<_functor_dag, _functor>, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES
  
/**
   @brief LibKet 'hook' gate creator

   This overload of the LibKet::gates::hook() function accepts an
   expression as constant reference
*/
  template<typename _functor, typename _expr, typename _functor_dag=_functor>
inline constexpr auto
hook(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QHook<_functor, _functor_dag>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
   @brief LibKet 'hook' gate creator

   This overload of the LibKet::gates::hook() function accepts an
   expression as universal reference
*/
  template<typename _functor, typename _expr, typename _functor_dag=_functor>
inline constexpr auto
hook(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QHook<_functor, _functor_dag>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
   @brief LibKet 'hook' gate creator

   Function alias for LibKet::gates::hook
*/
  template<typename _functor, typename _functor_dag, typename... Args>
inline constexpr auto
HOOK(Args&&... args)
{
  return hook<_functor, _functor_dag>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
  template<typename _functor, typename _functor_dag>
template<typename T>
inline constexpr auto
  QHook<_functor, _functor_dag>::operator()(const T& t) const noexcept
{
  return hook<_functor, _functor_dag>(std::forward<T>(t));
}

/// Operator() - by universal reference
  template<typename _functor, typename _functor_dag>
template<typename T>
inline constexpr auto
  QHook<_functor, _functor_dag>::operator()(T&& t) const noexcept
{
  return hook<_functor, _functor_dag>(std::forward<T>(t));
}

/**
   @brief LibKet 'hook'-dagger gate creator

   This overload of the LibKet::gates::hookdag() function accepts an
   expression as constant reference
*/
  template<typename _functor, typename _functor_dag, typename _expr>
inline constexpr auto
hookdag(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QHook<_functor_dag, _functor>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
   @brief LibKet 'hook'-dagger gate creator

   This overload of the LibKet::gates::hookdag() function accepts an
   expression as universal reference
*/
  template<typename _functor, typename _functor_dag, typename _expr>
inline constexpr auto
hookdag(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QHook<_functor_dag, _functor>,
                    typename filters::getFilter<_expr>::type>(expr);
}

  

/**
   @brief LibKet show gate type - specialization for QHook objects
*/
  template<std::size_t level = 1, typename _functor, typename _functor_dag>
inline static auto
  show(const QHook<_functor, _functor_dag>& gate,
     std::ostream& os = std::cout,
     const std::string& prefix = "") ->
  typename std::enable_if<_functor::isExpr, decltype(gate)>::type
{
  os << "QHook<" << _functor::hash << ">\n";
  if (level > 0) {
    os << prefix << "|   hook = ";
    show<level - 1>(_functor{}(), os, prefix + "|          ");
  }

  return gate;
}

  template<std::size_t level = 1, typename _functor, typename _functor_dag>
inline static auto
  show(const QHook<_functor, _functor_dag>& gate,
     std::ostream& os = std::cout,
     const std::string& prefix = "") ->
  typename std::enable_if<!_functor::isExpr, decltype(gate)>::type
{
  os << "QHook<" << _functor::hash << ">\n";
  if (level > 0) {
    os << prefix << "|   hook = \"" << _functor{}() << "\"\n";
  }

  return gate;
}

} // namespace gates

/**
@brief LibKet dagger creator

This overload of the LibKet::dagger() function returns the Hook gate
*/
  template<typename _functor, typename _functor_dag, typename _expr, typename _filter>
inline constexpr auto
dagger(
       const gates::UnaryQGate<_expr, gates::QHook<_functor, _functor_dag>, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return UnaryQGate<_expr,
                    QHook<_functor_dag, _functor>,
                    typename filters::getFilter<_expr>::type>(expr);
}
  
} // namespace LibKet

#endif // QGATE_HOOK_HPP

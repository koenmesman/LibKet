/** @file libket/gates/QGate_Identity.hpp

    @brief LibKet quantum identity class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_IDENTITY_HPP
#define QGATE_IDENTITY_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet identity gate class

The LibKet identity gate class implements the quantum identity
gate for an arbitrary number of quantum bits
*/

class QIdentity : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("I q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("cirq.I.on(q[" + utils::to_string(i) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "i q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("id q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().identity(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\ti q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("I " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    // QuEST does not provide an identity gate
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::identity(i));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QIdentity& gate)
{
  os << "i(";
  return os;
}

/**
@brief LibKet identity gate creator

This overload of the LibKet::gates::identity() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::identity( );
\endcode
*/
inline constexpr auto
identity() noexcept
{
  return UnaryQGate<filters::QFilter, QIdentity>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Identity gate creator

This overload of the LibKet::gates::identity() function
eliminates the double-application of the Identity gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
identity(
  const UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr;
}

/**
@brief LibKet Identity gate creator

This overload of the LibKet::gates::identity() function
eliminates the double-application of the Identity gate
*/
template<typename _expr>
inline constexpr auto
identity(
  UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet identity gate creator

This overload of the LibKet::gates::identity() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
identity(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet identity gate creator

This overload of the LibKet::gates::identity() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
identity(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QIdentity, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet identity gate creator

Function alias for LibKet::gates::identity
*/
template<typename... Args>
inline constexpr auto
IDENTITY(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet identity gate creator

Function alias for LibKet::gates::identity
*/
template<typename... Args>
inline constexpr auto
i(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet identity gate creator

Function alias for LibKet::gates::identity
*/
template<typename... Args>
inline constexpr auto
I(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet Identitydag gate creator

Function alias for LibKet::gates::identitydag
 */
template<typename... Args>
inline constexpr auto
identitydag(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet Identitydag gate creator

Function alias for LibKet::gates::identitydag
 */
template<typename... Args>
inline constexpr auto
IDENTITYdag(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet Identitydag gate creator

Function alias for LibKet::gates::identitydag
 */
template<typename... Args>
inline constexpr auto
idag(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/**
@brief LibKet Identitydag gate creator

Function alias for LibKet::gates::identitydag
 */
template<typename... Args>
inline constexpr auto
Idag(Args&&... args)
{
  return identity(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QIdentity::operator()(const T& t) const noexcept
{
  return identity(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QIdentity::operator()(T&& t) const noexcept
{
  return identity(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QIdentity objects
*/
template<std::size_t level = 1>
inline static auto
show(const QIdentity& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QIdentity\n";

  return gate;
}

} // namespace gates

/**
@brief LibKet dagger creator

This overload of the LibKet::dagger() function returns the Identity gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
dagger(
       const gates::UnaryQGate<_expr, gates::QIdentity, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return expr;
}

} // namespace LibKet

#endif // QGATE_IDENTITY_HPP

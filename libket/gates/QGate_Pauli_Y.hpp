/** @file libket/gates/QGate_Pauli_Y.hpp

    @brief LibKet quantum Pauli_Y class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_PAULI_Y_HPP
#define QGATE_PAULI_Y_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet Pauli_Y gate class

The LibKet Pauli_Y gate class implements the quantum Pauli_Y
gate for an arbitrary number of quantum bits
*/

class QPauli_Y : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("Y q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("cirq.Y.on(q[" + utils::to_string(i) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "y q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("y q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().y(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\ty q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("Y " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    for (auto i : _filter::range(data))
      quest::pauliY(data.reg(), i);

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::pauli_y(i));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QPauli_Y& gate)
{
  os << "y(";
  return os;
}

/**
@brief LibKet Pauli_Y gate creator

This overload of the LibKet::gates::pauli_y() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::pauli_y();
\endcode
*/
inline constexpr auto
pauli_y() noexcept
{
  return UnaryQGate<filters::QFilter, QPauli_Y>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Pauli_Y gate creator

This overload of the LibKet::gates::pauli_y function
eliminates the double-application of the Pauli_y gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
pauli_y(
  const UnaryQGate<_expr, QPauli_Y, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Pauli_Y gate creator

This overload of the LibKet::gates::pauli_y() function
eliminates the double-application of the Pauli_Y gate
*/
template<typename _expr>
inline constexpr auto
pauli_y(UnaryQGate<_expr, QPauli_Y, typename filters::getFilter<_expr>::type>&&
          expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Pauli_Y gate creator

This overload of the LibKet::gates::pauli_y() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
pauli_y(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QPauli_Y, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet Pauli_Y gate creator

This overload of the LibKet::gates::pauli_y() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
pauli_y(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QPauli_Y, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet Pauli_Y gate creator

Function alias for LibKet::gates::pauli_y
*/
template<typename... Args>
inline constexpr auto
PAULI_Y(Args&&... args)
{
  return pauli_y(std::forward<Args>(args)...);
}

/**
@brief LibKet Pauli_Y gate creator

Function alias for LibKet::gates::pauli_y
*/
template<typename... Args>
inline constexpr auto
y(Args&&... args)
{
  return pauli_y(std::forward<Args>(args)...);
}

/**
@brief LibKet Pauli_Y gate creator

Function alias for LibKet::gates::pauli_y
*/
template<typename... Args>
inline constexpr auto
Y(Args&&... args)
{
  return pauli_y(std::forward<Args>(args)...);
}

/**
@brief LibKet Pauli_Y gate creator

Function alias for LibKet::gates::pauli_y
*/
template<typename... Args>
inline constexpr auto
pauli_ydag(Args&&... args)
{
  return pauli_y(std::forward<Args>(args)...);
}

/**
@brief LibKet Pauli_Y gate creator

Function alias for LibKet::gates::pauli_y
*/
template<typename... Args>
inline constexpr auto
PAULI_Ydag(Args&&... args)
{
  return pauli_y(std::forward<Args>(args)...);
}
/**
@brief LibKet Pauli_Y gate creator

Function alias for LibKet::gates::pauli_y
*/
template<typename... Args>
inline constexpr auto
ydag(Args&&... args)
{
  return pauli_y(std::forward<Args>(args)...);
}
/**
@brief LibKet Pauli_Y gate creator

Function alias for LibKet::gates::pauli_y
*/
template<typename... Args>
inline constexpr auto
Ydag(Args&&... args)
{
  return pauli_y(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QPauli_Y::operator()(const T& t) const noexcept
{
  return pauli_y(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QPauli_Y::operator()(T&& t) const noexcept
{
  return pauli_y(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QPauli_Y objects
*/
template<std::size_t level = 1>
inline static auto
show(const QPauli_Y& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QPauli_Y\n";

  return gate;
}

} // namespace gates

/**
@brief LibKet dagger creator

This overload of the LibKet::dagger() function returns the Pauli_Y gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
dagger(
       const gates::UnaryQGate<_expr, gates::QPauli_Y, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return expr;
}

} // namespace LibKet

#endif // QGATE_PAULI_Y_HPP

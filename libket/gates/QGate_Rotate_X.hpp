/** @file libket/gates/QGate_Rotate_X.hpp

    @brief LibKet quantum Rotate_X class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_ROTATE_X_HPP
#define QGATE_ROTATE_X_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
template<typename _angle, typename _tol>
class QRotate_Xdag;

/**
@brief LibKet Rotate_X gate class

The LibKet Rotate_X gate class implements the quantum Rotate_X
gate for an arbitrary number of quantum bits
*/

template<typename _angle, typename _tol = QConst_t(0.0)>
class QRotate_X : public QGate
{
public:
  /// Rotation angle
  using angle = typename std::decay<_angle>::type;

  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("RX[" + _angle::to_string() + "] q[" +
                           utils::to_string(i) + "]\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("cirq.XPowGate(exponent=" + _angle::to_string() +
                           ").on(q[" + utils::to_string(i) + "])\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      std::string _expr = "rx q[";
      for (auto i : _filter::range(data))
        _expr += utils::to_string(i) +
                 (i != *(_filter::range(data).end() - 1) ? "," : "], ");
      _expr += _angle::to_string() + "\n";
      data.append_kernel(_expr);
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("rx(" + _angle::to_string() + ") q[" +
                           utils::to_string(i) + "];\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel([&]() { data.kernel().rx(i, _angle::value()); });
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("\trx q" + utils::to_string(i) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel("RX (" + _angle::to_string() + ") " +
                           utils::to_string(i) + "\n");
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        quest::rotateX(data.reg(), i, _angle::value());
    }
    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    if (tolerance(_angle{}, _tol{})) {
      for (auto i : _filter::range(data))
        data.append_kernel(new qx::rx(i, _angle::value()));
    }
    return data;
  }
#endif
};

/// Serialize operator
template<typename _angle, typename _tol = QConst_t(0.0)>
std::ostream&
operator<<(std::ostream& os, const QRotate_X<_angle, _tol>& gate)
{
  os << "rx<" << _tol::to_type() << ">(" << _angle::to_obj() << ",";
  return os;
}

/**
@brief LibKet Rotate_X gate creator

This overload of the LibKet::gates::rotate_x() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::rotate_x();
\endcode
*/
template<typename _tol = QConst_t(0.0), typename _angle>
inline constexpr auto rotate_x(_angle) noexcept
{
  return UnaryQGate<filters::QFilter, QRotate_X<_angle, _tol>>(
    filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Rotate_X gate creator
*/
template<typename _tol = QConst_t(0.0),
         typename _angle,
         typename _expr,
         typename _filter>
inline constexpr auto
rotate_x(
  _angle,
  const UnaryQGate<_expr,
                   QRotate_X<decltype(-_angle{}), _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Rotate_X gate creator
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr>
inline constexpr auto
rotate_x(_angle,
         UnaryQGate<_expr,
                    QRotate_X<decltype(-_angle{}), _tol>,
                    typename filters::getFilter<_expr>::type>&& expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Rotate_X gate creator
*/
template<typename _tol = QConst_t(0.0),
         typename _angle,
         typename _expr,
         typename _filter>
inline constexpr auto
rotate_x(
  _angle,
  const UnaryQGate<_expr,
                   QRotate_Xdag<_angle, _tol>,
                   typename filters::getFilter<_expr>::type>& expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet Rotate_X gate creator
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr>
inline constexpr auto
rotate_x(_angle,
         UnaryQGate<_expr,
                    QRotate_Xdag<_angle, _tol>,
                    typename filters::getFilter<_expr>::type>&& expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet Rotate_X gate creator

This overload of the LibKet::gates::rotate_x() function accepts
an expression as constant reference
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr>
inline constexpr auto
rotate_x(_angle, const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_X<_angle, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_X gate creator

This overload of the LibKet::gates::rotate_x() function accepts
an expression as universal reference
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr>
inline constexpr auto
rotate_x(_angle, _expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QRotate_X<_angle, _tol>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
ROTATE_X(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
rx(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
RX(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/**
@brief LibKet Rotate_X gate creator

Function alias for LibKet::gates::rotate_x
*/
template<typename _tol = QConst_t(0.0), typename... Args>
inline constexpr auto
Rx(Args&&... args)
{
  return rotate_x<_tol>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _angle, typename _tol>
template<typename T>
inline constexpr auto
QRotate_X<_angle, _tol>::operator()(const T& t) const noexcept
{
  return rotate_x<_tol>(_angle{}, std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename _angle, typename _tol>
template<typename T>
inline constexpr auto
QRotate_X<_angle, _tol>::operator()(T&& t) const noexcept
{
  return rotate_x<_tol>(_angle{}, std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QRotate_X objects
*/
template<std::size_t level = 1, typename _angle, typename _tol>
inline static auto
show(const QRotate_X<_angle, _tol>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QRotate_X " << _angle::to_string() << "\n";

  return gate;
}

} // namespace gates

/**
@brief LibKet dagger creator

This overload of the LibKet::dagger() function returns the Rotate_X gate
*/
template<typename _tol = QConst_t(0.0), typename _angle, typename _expr, typename _filter>
inline constexpr auto
dagger(
       const gates::UnaryQGate<_expr, gates::QRotate_X<_angle, _tol>, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return gates::UnaryQGate<_expr, gates::QRotate_Xdag<_angle, _tol>, typename filters::getFilter<_expr>::type>(expr.expr);
}
  
} // namespace LibKet

#endif // QGATE_ROTATE_X_HPP

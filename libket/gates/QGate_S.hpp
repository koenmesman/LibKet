/** @file libket/gates/QGate_S.hpp

    @brief LibKet quantum S gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_S_HPP
#define QGATE_S_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
class QSdag;

/**
@brief LibKet S gate class

The LibKet S gate class implements the quantum S gate
gate for an arbitrary number of quantum bits
*/

class QS : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("S q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("cirq.S.on(q[" + utils::to_string(i) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "s q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("s q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().s(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\ts q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("S " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    for (auto i : _filter::range(data))
      quest::sGate(data.reg(), i);

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::phase_shift(i)); // Looked up from QX loader

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QS& gate)
{
  os << "s(";
  return os;
}

/**
@brief LibKet S gate creator

This overload of the LibKet::gates::gate_s() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::s();
\endcode
*/
inline constexpr auto
s() noexcept
{
  return UnaryQGate<filters::QFilter, QS>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet S gate creator

This overload of the LibKet::gates::gate_s() function eliminates
the application of the S gate to its adjoint, the S dagger gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
s(const UnaryQGate<_expr, QSdag, typename filters::getFilter<_expr>::type>&
    expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet S gate creator

This overload of the LibKet::gates::gate_s() function eliminates
the application of the S gate to its adjoint, the S dagger gate
*/
template<typename _expr>
inline constexpr auto
s(UnaryQGate<_expr, QSdag, typename filters::getFilter<_expr>::type>&&
    expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet S gate creator

This overload of the LibKet::gates::s() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
s(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QS, typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet S gate creator

This overload of the LibKet::gates::s() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
s(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QS, typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet S gate creator

Function alias for LibKet::gates::s
*/
template<typename... Args>
inline constexpr auto
S(Args&&... args)
{
  return s(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QS::operator()(const T& t) const noexcept
{
  return s(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QS::operator()(T&& t) const noexcept
{
  return s(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QS objects
*/
template<std::size_t level = 1>
inline static auto
show(const QS& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QS\n";

  return gate;
}

} // namespace gates

/**
@brief LibKet dagger creator

This overload of the LibKet::dagger() function returns the S gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
dagger(
       const gates::UnaryQGate<_expr, gates::QS, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return gates::UnaryQGate<_expr, gates::QSdag, typename filters::getFilter<_expr>::type>(expr.expr);
}

} // namespace LibKet

#endif // QGATE_S_HPP

/** @file libket/gates/QGate_Tdag.hpp

    @brief LibKet quantum T-dagger gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_TDAG_HPP
#define QGATE_TDAG_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

// Forward declaration
class QT;

/**
@brief LibKet T-dagger gate class

The LibKet T-dagger gate class implements the quantum T-dagger
gate for an arbitrary number of quantum bits
*/

class QTdag : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

#ifdef LIBKET_WITH_AQASM
  /// Apply function - specialization for AQASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::AQASM>& apply(
    QData<_qubits, QBackendType::AQASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("DAG(T) q[" + utils::to_string(i) + "]\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CIRQ
  /// Apply function - specialization for Cirq backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Cirq>& apply(
    QData<_qubits, QBackendType::Cirq>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("(cirq.T**-1).on(q[" + utils::to_string(i) + "])\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_CQASM
  /// Apply function - specialization for cQASMv1 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::cQASMv1>& apply(
    QData<_qubits, QBackendType::cQASMv1>& data) noexcept
  {
    std::string _expr = "tdag q[";
    for (auto i : _filter::range(data))
      _expr += utils::to_string(i) +
               (i != *(_filter::range(data).end() - 1) ? "," : "]\n");
    data.append_kernel(_expr);

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQASM
  /// Apply function - specialization for OpenQASMv2 backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQASMv2>& apply(
    QData<_qubits, QBackendType::OpenQASMv2>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("tdg q[" + utils::to_string(i) + "];\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_OPENQL
  /// Apply function - specialization for OpenQL backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::OpenQL>& apply(
    QData<_qubits, QBackendType::OpenQL>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel([&]() { data.kernel().tdag(i); });

    return data;
  }
#endif

#ifdef LIBKET_WITH_QASM
  /// Apply function - specialization for QASM backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QASM>& apply(
    QData<_qubits, QBackendType::QASM>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("\ttdag q" + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUIL
  /// Apply function - specialization for Quil backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::Quil>& apply(
    QData<_qubits, QBackendType::Quil>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel("DAGGER T " + utils::to_string(i) + "\n");

    return data;
  }
#endif

#ifdef LIBKET_WITH_QUEST
  /// Apply function - specialization for QuEST-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QuEST>& apply(
    QData<_qubits, QBackendType::QuEST>& data) noexcept
  {
    quest::ComplexMatrix2 tdag = {
      .real = { { 1.0, 0.0 }, { 0.0, 1.0 / std::sqrt(2.0) } },
      .imag = { { 0.0, 0.0 }, { 0.0, -1.0 / std::sqrt(2.0) } }
    };
    for (auto i : _filter::range(data))
      quest::unitary(data.reg(), i, tdag);

    return data;
  }
#endif

#ifdef LIBKET_WITH_QX
  /// Apply function - specialization for QX-simulator backend
  template<std::size_t _qubits, typename _filter>
  inline static QData<_qubits, QBackendType::QX>& apply(
    QData<_qubits, QBackendType::QX>& data) noexcept
  {
    for (auto i : _filter::range(data))
      data.append_kernel(new qx::t_dag_gate(i));

    return data;
  }
#endif
};

/// Serialize operator
std::ostream&
operator<<(std::ostream& os, const QTdag& gate)
{
  os << "tdag(";
  return os;
}

/**
@brief LibKet T-dagger gate creator

This overload of the LibKet::gate:gate_tdag() function can be
used as terminal, i.e. the inner-most gate in a quantum
expression

\code
auto expr = gates::tdag();
\endcode
*/
inline constexpr auto
tdag() noexcept
{
  return UnaryQGate<filters::QFilter, QTdag>(filters::QFilter{});
}

#ifdef LIBKET_OPTIMIZE_GATES

/**
@brief LibKet T-dagger gate creator

This overload of the LibKet::gate:gate_tdag() function
eliminates the application of the T-dagger gate to its adjoint,
the T gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
tdag(const UnaryQGate<_expr, QT, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return expr.expr;
}

/**
@brief LibKet T-dagger gate creator

This overload of the LibKet::gate:gate_tdag() function
eliminates the application of the T-dagger gate to its adjoint,
the T gate
*/
template<typename _expr>
inline constexpr auto
tdag(UnaryQGate<_expr, QT, typename filters::getFilter<_expr>::type>&&
       expr) noexcept
{
  return expr.expr;
}

#endif // LIBKET_OPTIMIZE_GATES

/**
@brief LibKet T-dagger gate creator

This overload of the LibKet::gates::tdag() function accepts
an expression as constant reference
*/
template<typename _expr>
inline constexpr auto
tdag(const _expr& expr) noexcept
{
  return UnaryQGate<_expr, QTdag, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet T-dagger gate creator

This overload of the LibKet::gates::tdag() function accepts
an expression as universal reference
*/
template<typename _expr>
inline constexpr auto
tdag(_expr&& expr) noexcept
{
  return UnaryQGate<_expr, QTdag, typename filters::getFilter<_expr>::type>(
    expr);
}

/**
@brief LibKet T-dagger gate creator

Function alias for LibKet::gates::tdag
*/
template<typename... Args>
inline constexpr auto
TDAG(Args&&... args)
{
  return tdag(std::forward<Args>(args)...);
}

/**
@brief LibKet T-dagger gate creator

Function alias for LibKet::gates::tdag
*/
template<typename... Args>
inline constexpr auto
Tdag(Args&&... args)
{
  return tdag(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename T>
inline constexpr auto
QTdag::operator()(const T& t) const noexcept
{
  return tdag(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename T>
inline constexpr auto
QTdag::operator()(T&& t) const noexcept
{
  return tdag(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QTdag objects
*/
template<std::size_t level = 1>
inline static auto
show(const QTdag& gate, std::ostream& os, const std::string& prefix = "")
{
  os << "QTdag\n";

  return gate;
}

} // namespace gates

/**
@brief LibKet dagger creator

This overload of the LibKet::dagger() function returns the Tdag gate
*/
template<typename _expr, typename _filter>
inline constexpr auto
dagger(
       const gates::UnaryQGate<_expr, gates::QTdag, typename filters::getFilter<_expr>::type>&
       expr) noexcept
{
  return UnaryQGate<_expr, QT, typename filters::getFilter<_expr>::type>(expr.expr);
}

} // namespace LibKet

#endif // QGATE_TDAG_HPP

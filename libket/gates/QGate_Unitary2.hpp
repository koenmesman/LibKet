/** @file libket/gates/QGate_Unitary2.hpp

    @brief LibKet 2x2 unitary gate class

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef QGATE_UNITARY2_HPP
#define QGATE_UNITARY2_HPP

#include <QData.hpp>
#include <QFilter.hpp>

#include <gates/QGate.hpp>

namespace LibKet {

namespace gates {

/**
@brief LibKet 2x2 unitary gate class

The 2x2 unitary gate accepts an arbitrary \f$2\times 2\f$
unitary matrix \f$U\f$ as input and performs the ZYZ decomposition
\f[
U = e^{i\Phi} R_z(\alpha)R_y(\beta)R_z(\gamma)
\f]
Here, \f$\Phi,\alpha,\beta,\gamma\f$ are rotation angles.
*/
template<typename _functor>
class QUnitary2 : public QGate
{
public:
  /// Operator() - by constant reference
  template<typename T>
  inline constexpr auto operator()(const T& t) const noexcept;

  /// Operator() - by universal reference
  template<typename T>
  inline constexpr auto operator()(T&& t) const noexcept;

  /// Apply function
  template<std::size_t _qubits, typename _filter, QBackendType _qbackend>
  inline static QData<_qubits, _qbackend>& apply(
    QData<_qubits, _qbackend>& data)
  {
    auto u = _functor{}();
    decltype(u) x = { 0.0, 1.0, 1.0, 0.0 };

    if (!optim::de(
          x,
          QUnitary2<_functor>::unitary_fn<typename decltype(u)::elem_type>,
          &u))
      throw std::runtime_error(
        "QUnitary2: An error occured in the decomposition");

    QInfo << x << std::endl;
    // @Merel: Please add your code here

    gen_expression("gototag<0>(i(all(tag<0>())))", data);
    return data;
  }

private:
  template<typename T = real_t>
  static T unitary_fn(const arma::Col<T>& vals_inp,
                      arma::Col<T>* grad_out,
                      void* opt_data)
  {
    // Get the matrix values out of the input vector
    arma::Col<T> u_data = *(arma::Col<T>*)(opt_data);

    // Name the seperate components of the to find output vector to
    // make the calculation more readable and a one-on-one copy of the
    // original way of writing it out.
    T u_1 = u_data(0);
    T u_2 = u_data(1);
    T u_3 = u_data(2);
    T u_4 = u_data(3);

    T x_1 = vals_inp(0);
    T x_2 = vals_inp(1);
    T x_3 = vals_inp(2);
    T x_4 = vals_inp(3);

    // Define the necessary imaginary number
    std::complex<T> mycomplex((T)0, (T)1.0);

    using namespace std;
    T obj_val = abs(exp(imag(mycomplex) * (x_1 - x_2 / 2.0 - x_4 / 2.0)) *
                      cos(x_3 / 2.0) -
                    u_1) +
                abs(-exp(imag(mycomplex) * (x_1 - x_2 / 2.0 + x_4 / 2.0)) *
                      sin(x_3 / 2.0) -
                    u_2) +
                abs(exp(imag(mycomplex) * (x_1 + x_2 / 2.0 - x_4 / 2.0)) *
                      sin(x_3 / 2.0) -
                    u_3) +
                abs(exp(imag(mycomplex) * (x_1 + x_2 / 2.0 + x_4 / 2.0)) *
                      cos(x_3 / 2.0) -
                    u_4);

    return obj_val;
  }
};

/**
@brief LibKet 2x2 unitary gate creator

This overload of the LibKet::gates::unitary2() function can be used
as terminal, i.e. the inner-most gate in a quantum expression

\code
auto expr = gates::unitary2();
\endcode
*/
template<typename _functor>
inline constexpr auto
unitary2() noexcept
{
  return UnaryQGate<filters::QFilter, QUnitary2<_functor>, filters::QFilter>(
    filters::QFilter{});
}

/**
@brief LibKet 2x2 unitary gate creator

This overload of the LibKet::gates::unitary2() function accepts an
expression as constant reference
*/
template<typename _functor, typename _expr>
inline constexpr auto
unitary2(const _expr& expr) noexcept
{
  return UnaryQGate<_expr,
                    QUnitary2<_functor>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet 2x2 unitary gate creator

This overload of the LibKet::gates::unitary2() function accepts an
expression as universal reference
*/
template<typename _functor, typename _expr>
inline constexpr auto
unitary2(_expr&& expr) noexcept
{
  return UnaryQGate<_expr,
                    QUnitary2<_functor>,
                    typename filters::getFilter<_expr>::type>(expr);
}

/**
@brief LibKet 2x2 unitary gate creator

Function alias for LibKet::gates::unitary2
*/
template<typename _functor, typename... Args>
inline constexpr auto
u2(Args&&... args)
{
  return unitary2<_functor>(std::forward<Args>(args)...);
}

/**
@brief LibKet 2x2 unitary gate creator

Function alias for LibKet::gates::unitary2
*/
template<typename _functor, typename... Args>
inline auto
UNITARY2(Args&&... args)
{
  return unitary2<_functor>(std::forward<Args>(args)...);
}

/**
@brief LibKet 2x2 unitary gate creator

Function alias for LibKet::gates::unitary2
*/
template<typename _functor, typename... Args>
inline auto
U2(Args&&... args)
{
  return unitary2<_functor>(std::forward<Args>(args)...);
}

/// Operator() - by constant reference
template<typename _functor>
template<typename T>
inline constexpr auto
QUnitary2<_functor>::operator()(const T& t) const noexcept
{
  return unitary2<_functor>(std::forward<T>(t));
}

/// Operator() - by universal reference
template<typename _functor>
template<typename T>
inline constexpr auto
QUnitary2<_functor>::operator()(T&& t) const noexcept
{
  return unitary2<_functor>(std::forward<T>(t));
}

/**
   @brief LibKet show gate type - specialization for QUnitary2 objects
*/
template<std::size_t level = 1, typename _functor>
inline static auto
show(const QUnitary2<_functor>& gate,
     std::ostream& os,
     const std::string& prefix = "")
{
  os << "QUnitary2<" << _functor::hash << ">\n";

  return gate;
}

} // namespace gates

} // namespace LibKet

#endif // QGATE_UNITARY2_HPP

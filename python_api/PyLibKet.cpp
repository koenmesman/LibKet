/** @file python_api/PyLibKet.cpp

@brief LibKet quantum base classes and declarations

@copyright This file is part of the LibKet library (Python API)

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
*/

#include <pybind11/pybind11.h>
#include <pybind11/operators.h>
#include <pybind11/stl.h>

#include "PyQBase.hpp"
#include "PyQCircuits.hpp"
#include "PyQData.hpp"
#include "PyQFilters.hpp"
#include "PyQGates.hpp"
#include "PyQJob.hpp"
#include "PyQStream.hpp"

namespace py = pybind11;

/**
   @brief Creates LibKet Python module
*/
PYBIND11_MODULE(pylibket, m) {
  
  m.attr("__name__") = "pylibket";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";

  // PyQObject
  py::class_<PyQObject, std::shared_ptr<PyQObject>>(m, "PyQObject")
    .def(py::init<>())
    .def("print", &PyQObject::print)
    .def("__repr__", &PyQObject::print)
    ;
  
  // PyQBase
  py::class_<PyQBase, PyQObject, std::shared_ptr<PyQBase>>(m, "PyQBase")
    .def(py::init<>())
    .def(py::init<std::shared_ptr<PyQObject>>())
    .def("apply", &PyQBase::apply)
    .def("print", &PyQBase::print)
    .def("__repr__", &PyQBase::print)
    ;

  py::module circuit = m.def_submodule("circuit");
  pybind11_init_circuit( circuit );
  
  py::module data = m.def_submodule("data");
  pybind11_init_data( data );
  
  py::module filter = m.def_submodule("filter");
  pybind11_init_filter( filter );
         
  py::module gate =  m.def_submodule("gate");
  pybind11_init_gate( gate );
  
  py::module job = m.def_submodule("job");
  pybind11_init_job( job );
  
  py::module stream = m.def_submodule("stream");
  pybind11_init_stream( stream );
}

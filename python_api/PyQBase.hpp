/** @file python_api/PyQBase.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef PYTHON_API_QBASE_HPP
#define PYTHON_API_QBASE_HPP

#include <QConfig.h>

/**
   @brief LibKet quantum object class
*/
class PyQObject
{
public:
  /// @brief Prints content of the current object
  virtual std::string print() const { return "<PyQObject>"; }

  /// @brief Returns duplicate of the current object applied to the given one
  virtual std::shared_ptr<PyQObject> apply(std::shared_ptr<PyQObject> obj) const
  {
    return obj;
  }

  /// @brief Returns the current object applied to the given one
  virtual std::shared_ptr<PyQObject> operator()(std::shared_ptr<PyQObject> obj)
  {
    return obj;
  }

  /// @brief Returns duplicate of the current object
  virtual std::shared_ptr<PyQObject> duplicate() const
  {
    return std::make_shared<PyQObject>();
  }
};

/**
   @brief LibKet quantum base class
*/
class PyQBase
  : public PyQObject
  , public std::enable_shared_from_this<PyQObject>
{
public:
  /// @brief Default constructor
  PyQBase()
    : _obj(std::make_shared<PyQObject>())
  {}

  /// @brief Contructor
  PyQBase(std::shared_ptr<PyQObject> obj)
    : _obj(obj)
  {}

  /// @brief Prints the content of the current object
  virtual std::string print() const override { return "<PyQBase>"; }

  /// @brief Returns duplicate of the current object applied to the given one
  virtual std::shared_ptr<PyQObject> apply(
    std::shared_ptr<PyQObject> obj) const override
  {
    std::shared_ptr<PyQObject> tmp = this->duplicate();
    return tmp->operator()(obj);
  }

  /// @brief Returns the current object applied to the given one
  virtual std::shared_ptr<PyQObject> operator()(
    std::shared_ptr<PyQObject> obj) override
  {
    this->_obj = this->_obj->operator()(obj);
    return shared_from_this();
  }

  /// @brief Returns duplicate of the current object
  virtual std::shared_ptr<PyQObject> duplicate() const override
  {
    std::shared_ptr<PyQBase> tmp = std::make_shared<PyQBase>(*this);
    tmp->_obj = _obj->duplicate();
    return tmp;
  }

  /// @brief Returns constant reference to internal subexpression object
  virtual std::shared_ptr<PyQObject> subexpr() const { return _obj; }

protected:
  std::shared_ptr<PyQObject> _obj;
};

#endif // PYTHON_API_QBASE_HPP

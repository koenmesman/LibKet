/** @file python_api/PyQCircuits.hpp

    @brief LibKet quantum circuit classes

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef PYTHON_API_QCIRCUITS_HPP
#define PYTHON_API_QCIRCUITS_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "PyQBase.hpp"

/**
   @brief LibKet quantum circuit class
*/
class PyQCircuit : public PyQBase
{};

/**
   @brief Creates LibKet quantum circuit Python module
*/
void
pybind11_init_circuit(pybind11::module& m);

#endif // PYTHON_QPI_QCIRCUITS_HPP

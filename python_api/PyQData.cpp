/** @file python_api/PyQData.hpp

    @brief LibKet quantum data class

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#include "PyQData.hpp"

namespace py = pybind11;

/**
   @brief LibKet quantum backend lookup table
 */
std::map<PyQBackendType, std::string> PyQBackendTypeMap = {
       {PyQBackendType::AQASM,      "PyQBackendType::AQASM"},
       {PyQBackendType::cQASMv1,    "PyQBackendType::cQASMv1"},
       {PyQBackendType::OpenQASMv2, "PyQBackendType::OpenQASMv2"},
       {PyQBackendType::OpenQL,     "PyQBackendType::OpenQL"},
       {PyQBackendType::QASM,       "PyQBackendType::QASM"},
       {PyQBackendType::Quil,       "PyQBackendType::Quil"},
       {PyQBackendType::QX,         "PyQBackendType::QX"}
};

std::string PyQData::print() const
{ return "<pylibket.data.PyQData qubits=" + std::to_string(qubits) + ", backend=" + PyQBackendTypeMap[backend] + ">"; }

/**
   @brief Creates LibKet quantum data Python module
*/
void pybind11_init_data(pybind11::module &m)
{
  m.attr("__name__") = "pylibket.data";
  m.attr("__version__") = LIBKET_VERSION;
  m.doc() = "LibKet: the Quantum Expression Template Library";

  py::enum_<PyQBackendType>(m, "PyQBackendType")
    .value("AQASM",      PyQBackendType::AQASM)
    .value("cQASMv1",    PyQBackendType::cQASMv1)
    .value("OpenQASMv2", PyQBackendType::OpenQASMv2)
    .value("OpenQL",     PyQBackendType::OpenQL)
    .value("QASM",       PyQBackendType::QASM)
    .value("Quil",       PyQBackendType::Quil)
    .value("QX",         PyQBackendType::QX)
    .export_values();
  
  py::class_<PyQData, PyQBase, std::shared_ptr<PyQData>>(m, "PyQData")
    .def(py::init<>())
    .def(py::init<std::size_t>())
    .def(py::init<std::size_t, PyQBackendType>())
    .def("print", &PyQData::print)
    .def("__repr__",
         [](const PyQData &obj) {
           return obj.print();
         }
         )
    .def_readwrite("qubits", &PyQData::qubits)
    .def_readwrite("backend", &PyQData::backend);
}

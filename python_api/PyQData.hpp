/** @file python_api/PyQData.hpp

    @brief LibKet quantum data classes

    @copyright This file is part of the LibKet library (Python API)

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef PYTHON_API_QDATA_HPP
#define PYTHON_API_QDATA_HPP

#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "json/json.hpp"
using json = nlohmann::json;

#include "PyQBase.hpp"

/**
   @brief LibKet quantum backends
*/
enum class PyQBackendType
{
  AQASM,
  cQASMv1,
  OpenQASMv2,
  OpenQL,
  QASM,
  Quil,
  QX
};

/**
   @brief LibKet quantum data class
*/
class PyQData : public PyQBase
{
public:
  /// Default constructor
  PyQData()
    : PyQData(0, PyQBackendType::QASM)
  {}

  /// @brief Constructor
  PyQData(std::size_t qubits)
    : PyQData(qubits, PyQBackendType::QASM)
  {}

  /// @brief Constructor
  PyQData(std::size_t qubits, PyQBackendType backend)
    : qubits(qubits)
    , backend(backend)
  {}

  /// @brief Prints filter
  std::string print() const override;

  /// @brief Generates QASM code
  std::string to_qasm(std::shared_ptr<PyQBase> obj) const
  {
    // return gen_expression(obj->print(), )
  }

public:
  std::size_t qubits;
  PyQBackendType backend;
};

/**
   @brief Creates LibKet quantum data Python module
*/
void
pybind11_init_data(pybind11::module& m);

#endif // PYTHON_API_QDATA_HPP

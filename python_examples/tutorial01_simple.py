#!/usr/bin/env python3.6

"""
LibKet tutorial01: Quantum filters, gates, and simple expressions

This tutorial illustrates the basic usage of quantum filters and
quantum gates to compose simple quantum expressions that are
executed on all selected quantum backends

@copyright This file is part of the LibKet library

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.

@author Matthias Moller
"""

try:
    from pylibket import *
    from pylibket.gate import *
    from pylibket.filter import *
    from pylibket.circuit import *

except:
    print("An error occured while loading the Python LibKet module")
    
# Create simple quantum expression:
# 1. Apply Hadamard gate to all qubits
# 2. Apply controlled-not gate to q3,q4,q5 with controlling qubits q0,q1,q2
expr = measure(
    all(
        cnot(
            sel([0,1,2]),
            sel([3,4,5],
                h(
                    init()
                )
            )
        )
    )
)

print(expr)

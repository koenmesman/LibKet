#!/bin/sh

# This script generates specializations of static_for_impl structures
# to eliminate recursive template instantiations during compilation,
# which should reduce compilation times significantly.
#
# The input file must contain a list of rules of the following form:
#
# static_for:1,5,2
#
# This will generate an instantiation of the static for loop from 1 to
# 5 with step size 2. Alternatively ranges can be specied, e.g.
#
#

if [ "$#" -ne 1 ]; then
    echo "Usage: static_for_specializations.sh <inputfile>"
    exit
fi

# Define file header
__file_header="
/** @file libket/specializations/static_for.hpp

    @brief LibKet specializations of static_for

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
 */

#pragma once
#ifndef STATIC_FOR_HPP
#define STATIC_FOR_HPP

"

# Define file footer
__file_footer="

#endif // STATIC_FOR_HPP
"

# Print file header
echo "$__file_header"

# Get input file from command line
input=$1

# Loop through lines of input file
while IFS= read -r line
do    
    token=(${line//:/ })
    
    # Get active on keyword 'static_for'
    if [ "${token[0]}" = "static_for" ]; then

        # Get loop bounds
        range=(${token[1]//,/ })
        for_start=(${range[0]//../ })
        for_stop=(${range[1]//../ })
        for_step=(${range[2]//../ })

        range_start=($(seq ${for_start[@]}))
        range_stop=($(seq ${for_stop[@]}))
        range_step=($(seq ${for_step[@]}))

        # Loop over all start-stop-step combinations
        for start in "${range_start[@]}"; do
            for stop in "${range_stop[@]}"; do
                for step in "${range_step[@]}"; do

                    # Print class header
                    echo "
template<template<index_t start, index_t end, index_t step, index_t index>
         class functor,
         typename functor_return_type,
         typename... functor_types>
struct static_for_impl<$start, $stop, $step, $start, functor, functor_return_type, functor_types...>
{
  inline auto operator()(functor_return_type&& functor_return_arg,
                         functor_types&&... functor_args)
  {
                    "
                    
                    # Print first line of body
                    echo "    auto arg$start = functor_return_arg;"
                    j=$start
                    
                    # Print body for positive step size
                    if [ $start -le $stop -a $step -gt 0 ]; then
                        for ((i=start;i<=stop;i+=step)); do
                            j="$((i+step))"
                            echo "
    auto arg${j//-//M} = functor<$start, $stop, $step, $i>()(std::forward<decltype(arg$i)>(arg$i),
                                      std::forward<functor_types>(functor_args)...);
                        "
                        done
                    fi

                    # Print body for negativestep size
                    if [ $start -ge $stop -a $step -lt 0 ]; then
                        for ((i=start;i>=stop;i+=step)); do
                            j="$((i+step))"
                            echo "
    auto arg${j//-/M} = functor<$start, $stop, $step, $i>()(std::forward<decltype(arg$i)>(arg$i),
                                      std::forward<functor_types>(functor_args)...);
                        "
                        done
                    fi

                    # Print last line of body
                    echo "    return arg${j//-/M};"
                    
                    # Print class footer
                    echo "
  }
};
                    "
                    
                done
            done
        done

    fi
    
done < "$input"

echo "$__file_footer"

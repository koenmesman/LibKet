/** @file unittests/src/detail/AQASM.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_AQASM_HPP
#define DETAIL_AQASM_HPP

#include <test_config.h>

#ifdef LIBKET_WITH_AQASM

template<std::size_t _qubits>
struct Fixture_AQASM
{
public:
  LibKet::QDevice<LibKet::QDeviceType::atos_qlm_feynman_simulator, _qubits>
    device;

  template<typename Expr>
  bool run(const Expr& expr)
  {
#ifdef LIBKET_BUILD_UNITTESTS_BACKEND
    try {
      LibKet::utils::json result = device(expr).eval(16);
      return true;
    } catch (...) {
      return false;
    }
#else
    return true;
#endif
  }
};

#endif // LIBKET_WITH_AQASM
#endif // DETAIL_AQASM_HPP

/** @file unittests/src/detail/OpenQASM.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_OPENQASM_HPP
#define DETAIL_OPENQASM_HPP

#include <test_config.h>

#ifdef LIBKET_WITH_OPENQASM

template<std::size_t _qubits>
struct Fixture_OpenQASM
{
public:
  LibKet::QDevice<LibKet::QDeviceType::qiskit_qasm_simulator, _qubits> device;

  template<typename Expr>
  bool run(const Expr& expr)
  {
#ifdef LIBKET_BUILD_UNITTESTS_BACKEND
    try {
      // Perform forced measurement at the end of the circuit
      LibKet::utils::json result = device(measure(all(expr))).eval(16);
      return device.template get<LibKet::QResultType::status>(result);
    } catch (...) {
      return false;
    }
#else
    return true;
#endif
  }
};

#endif // LIBKET_WITH_OPENQASM
#endif // DETAIL_OPENQASM_HPP

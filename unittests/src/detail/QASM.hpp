/** @file unittests/src/detail/QASM.hpp

    @brief LibKet quantum base classes and declarations

    @copyright This file is part of the LibKet library

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.

    @author Matthias Moller
*/

#pragma once
#ifndef DETAIL_QASM_HPP
#define DETAIL_QASM_HPP

#include <test_config.h>

#ifdef LIBKET_WITH_QASM

template<std::size_t _qubits>
struct Fixture_QASM
{
public:
  LibKet::QDevice<LibKet::QDeviceType::qasm2tex_visualizer, _qubits> device;

  template<typename Expr>
  bool run(const Expr& expr)
  {
#ifdef LIBKET_BUILD_UNITTESTS_BACKEND
    try {
      std::string result = device(expr).to_file("unittest");
      return !result.empty();
    } catch (...) {
      return false;
    }
#else
    return true;
#endif
  }
};

#endif // LIBKET_WITH_QASM
#endif // DETAIL_QASM_HPP

/** @file test_gate_cphase.hpp
 *
 *  @brief UnitTests++ LibKet::gate::cphase() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_cphase)
{

  // --- CPHASE (EXPR , EXPR) ---
  
  try {
    // cphase(QConst(3.141))
    auto expr = cphase(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilter\n|  "
                "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),all(),all())
    auto expr = cphase(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelectAll\n|  "
      "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = cphase(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141), range<0,3>(), range<4,7>())
    auto expr = cphase(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto expr = cphase(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141), qubit<1>(), qubit<2>())
    auto expr = cphase(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // --- CPHASE (EXPR , CPHASE) ---

  try {
    // cphase(QConst(3.141),sel<0>(), sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))))
    auto expr = cphase(QConst(3.141),sel<0>(), sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilterSelect [ 0 1 ]\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = QFilterSelect [ 0 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(), sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))))
    auto expr = cphase(QConst(3.141),sel<1>(), sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = QFilterSelect [ 1 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(), sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init()))))
    auto expr = cphase(QConst(3.141),sel<0>(), sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = QFilterSelect [ 0 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(), sel<1>( all(cphase(QConst(3.141),sel<0>(init()), sel<1>())))
    auto expr = cphase(QConst(3.141),sel<0>(), sel<1>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = QFilterSelect [ 0 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(), sel<1>( all(cphase(QConst(3.141),sel<0>(i(all())), sel<1>(init())))))
    auto expr =
      cphase(QConst(3.141),sel<0>(), sel<1>(all(cphase(QConst(3.141),sel<0>(i(all())), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
#ifdef LIBKET_L2R_EVALUATION
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilterSelectAll\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QIdentity\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QInit\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilter\n");
#endif
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = QFilterSelect [ 0 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QIdentity\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilterSelectAll\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(), sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init()))))
    auto expr = cphase(QConst(3.141),sel<1>(), sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = QFilterSelect [ 1 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(), sel<0>( all(cphase(QConst(3.141),sel<0>(init()), sel<1>())))
    auto expr = cphase(QConst(3.141),sel<1>(), sel<0>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = QFilterSelect [ 1 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(), sel<0>( all(cphase(QConst(3.141),sel<0>(i()), sel<1>(init())))))
    auto expr = cphase(QConst(3.141),sel<1>(), sel<0>(all(cphase(QConst(3.141),sel<0>(i()), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = QFilterSelect [ 1 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QIdentity\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(init()), sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))))
    auto expr = cphase(QConst(3.141),sel<0>(init()), sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QInit\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilter\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(init()), sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))))
    auto expr = cphase(QConst(3.141),sel<1>(init()), sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QInit\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilter\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(i(all())), sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))))
    auto expr =
      cphase(QConst(3.141),sel<0>(i(all())), sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilterSelectAll\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilterSelectAll\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(i(all())), sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))))
    auto expr =
      cphase(QConst(3.141),sel<1>(i(all())), sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilterSelectAll\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(i()), sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))))
    auto expr = cphase(QConst(3.141),sel<0>(i()), sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilter\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(i()), sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))))
    auto expr = cphase(QConst(3.141),sel<1>(i()), sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilter\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(i(all())), sel<1>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())))
    auto expr =
      cphase(QConst(3.141),sel<0>(i(all())), sel<1>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilterSelectAll\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilterSelectAll\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(i(all())), sel<0>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())))
    auto expr =
      cphase(QConst(3.141),sel<1>(i(all())), sel<0>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilterSelectAll\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>(i()), sel<1>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())))
    auto expr = cphase(QConst(3.141),sel<0>(i()), sel<1>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilter\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>(i()), sel<0>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())))
    auto expr = cphase(QConst(3.141),sel<1>(i()), sel<0>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilter\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // --- CPHASE (CPHASE , EXPR) ---

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<1>())
    auto expr = cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<1>());

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilterSelect [ 0 1 ]\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<0>())
    auto expr = cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<0>());

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = QFilterSelect [ 0 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init()))), sel<0>())
    auto expr = cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<0>());

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = QFilterSelect [ 0 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(init()), sel<1>())), sel<0>())
    auto expr = cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))), sel<0>());

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = QFilterSelect [ 0 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(i(all())), sel<1>(init())))), sel<0>())
    auto expr =
      cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(i(all())), sel<1>(init())))), sel<0>());

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QIdentity\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilterSelectAll\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = QFilterSelect [ 0 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init()))), sel<1>())
    auto expr = cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<1>());

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(init()), sel<1>())), sel<1>())
    auto expr = cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))), sel<1>());

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(i()), sel<1>(init())))), sel<1>())
    auto expr = cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(i()), sel<1>(init())))), sel<1>());

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
#ifdef LIBKET_L2R_EVALUATION
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QIdentity\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QInit\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilter\n");
#endif
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QIdentity\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<0>(init()))
    auto expr = cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<0>(init()));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QInit\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<1>(init()))
    auto expr = cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>()))), sel<1>(init()));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && !defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QInit\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<0>(i(all())))
    auto expr =
      cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<0>(i(all())));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<1>(i(all())))
    auto expr =
      cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<1>(i(all())));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && !defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilterSelectAll\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilterSelectAll\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<0>(i()))
    auto expr = cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<0>(i()));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<1>(i()))
    auto expr = cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(), sel<1>(init())))), sel<1>(i()));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && !defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 1 ]\n"
                "|          |          |   expr = QFilter\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())), sel<0>(i(all())))
    auto expr =
      cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))), sel<0>(i(all())));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())), sel<1>(i(all())))
    auto expr =
      cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))), sel<1>(i(all())));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && !defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilterSelectAll\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilterSelectAll\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<1>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())), sel<0>(i()))
    auto expr = cphase(QConst(3.141),sel<1>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))), sel<0>(i()));

    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 1 0 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cphase(QConst(3.141),sel<0>( all(cphase(QConst(3.141),sel<0>(init())), sel<1>())), sel<1>(i()))
    auto expr = cphase(QConst(3.141),sel<0>(all(cphase(QConst(3.141),sel<0>(init()), sel<1>()))), sel<1>(i()));

    std::stringstream ss;

    show<99>(expr, ss);
#if defined(LIBKET_OPTIMIZE_GATES) && !defined(LIBKET_L2R_EVALUATION)
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n"
                "|   gate = QInit\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|   expr = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelectAll\n"
                "|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = UnaryQGate\n"
                "|          |          |   gate = QInit\n"
                "|          |          | filter = QFilterSelect [ 0 ]\n"
                "|          |          |   expr = QFilter\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = UnaryQGate\n"
                "|          |   gate = QIdentity\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // --- CPHASE (CPHASE , CPHASE) ---

  try {
    // cphase(QConst(3.141),sel_<0>(cphase(QConst(3.141),sel_<0>(), sel_<1>())),
    //      sel_<1>(cphase(QConst(3.141),sel_<0>(), sel_<1>())))
    auto expr = cphase(QConst(3.141),sel_<0>(cphase(QConst(3.141),sel_<0>(), sel_<1>())),
                     sel_<1>(cphase(QConst(3.141),sel_<0>(), sel_<1>())));

    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = QFilterSelect [ 0 ]\n"
                "|  expr1 = QFilterSelect [ 1 ]\n");
#else
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n"
                "|   gate = QCPhase 3.141\n"
                "| filter = QFilterSelect [ 0 1 ]\n"
                "|  expr0 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 0 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n"
                "|  expr1 = BinaryQGate\n"
                "|          |   gate = QCPhase 3.141\n"
                "|          | filter = QFilterSelect [ 1 ]\n"
                "|          |  expr0 = QFilterSelect [ 0 ]\n"
                "|          |  expr1 = QFilterSelect [ 1 ]\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
  
  // --- CPHASE alias ---
  
  try {
    // CPHASE(QConst(3.141))
    auto expr = CPHASE(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilter\n|  "
                "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASE(QConst(3.141), all(), all())
    auto expr = CPHASE(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelectAll\n|  "
      "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASE(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = CPHASE(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASE(QConst(3.141), range<0,3>(), range<4,7>())
    auto expr = CPHASE(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASE(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto expr = CPHASE(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CPHASE(QConst(3.141), qubit<1>(), qubit<2>())
    auto expr = CPHASE(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // --- cr alias ---
  
  try {
    // cr(QConst(3.141))
    auto expr = cr(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilter\n|  "
                "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cr(QConst(3.141), all(), all())
    auto expr = cr(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelectAll\n|  "
      "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cr(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = cr(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cr(QConst(3.141), range<0,3>(), range<4,7>())
    auto expr = cr(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cr(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto expr = cr(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // cr(QConst(3.141), qubit<1>(), qubit<2>())
    auto expr = cr(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // --- CR alias ---
  
  try {
    // CR(QConst(3.141))
    auto expr = CR(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilter\n|  "
                "expr0 = QFilter\n|  expr1 = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CR(QConst(3.141), all(), all())
    auto expr = CR(QConst(3.141), all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelectAll\n|  "
      "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CR(QConst(3.141), sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr = CR(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CR(QConst(3.141), range<0,3>(), range<4,7>())
    auto expr = CR(QConst(3.141), range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CR(QConst(3.141), qureg<0,4>(), qureg<4,4>())
    auto expr = CR(QConst(3.141), qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // CR(QConst(3.141), qubit<1>(), qubit<2>())
    auto expr = CR(QConst(3.141), qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // --- QCPhase alias ---
  
  try {
    // QCPhase<QConst_t(3.141)>()
    auto expr = QCPhase<QConst_t(3.141)>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QCPhase 3.141\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhase<QConst_t(3.141)>()(all(), all())
    auto expr = QCPhase<QConst_t(3.141)>()(all(), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelectAll\n|  "
      "expr0 = QFilterSelectAll\n|  expr1 = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhase<QConst_t(3.141)>()(sel<0,1,3,5>(), sel<2,4,6,7>())
    auto expr =
      QCPhase<QConst_t(3.141)>()(sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 3 5 2 4 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 3 5 ]\n|  expr1 = QFilterSelect [ 2 4 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhase<QConst_t(3.141)>()(range<0,3>(), range<4,7>())
    auto expr = QCPhase<QConst_t(3.141)>()(range<0, 3>(), range<4, 7>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhase<QConst_t(3.141)>()(qureg<0,4>(), qureg<4,4>())
    auto expr = QCPhase<QConst_t(3.141)>()(qureg<0, 4>(), qureg<4, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = "
                "QFilterSelect [ 0 1 2 3 4 5 6 7 ]\n|  expr0 = QFilterSelect [ "
                "0 1 2 3 ]\n|  expr1 = QFilterSelect [ 4 5 6 7 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QCPhase<QConst_t(3.141)>()(qubit<1>(), qubit<2>())
    auto expr = QCPhase<QConst_t(3.141)>()(qubit<1>(), qubit<2>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "BinaryQGate\n|   gate = QCPhase 3.141\n| filter = QFilterSelect [ 1 2 "
      "]\n|  expr0 = QFilterSelect [ 1 ]\n|  expr1 = QFilterSelect [ 2 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  // --- run CPHASE ---
  
  try {
    // run(cphase(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(init())))
    auto expr =
      cphase(QConst(3.141), sel<0, 1, 3, 5>(), sel<2, 4, 6, 7>(init()));
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}

/** @file test_gate_measure_x.hpp
 *
 *  @brief UnitTests++ LibKet::gate::measure_x() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_measure_x)
{
  try {
    // measure_x()
    auto expr = LibKet::gates::measure_x();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilter\n|   expr "
      "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(all())
    auto expr = LibKet::gates::measure_x(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QMeasure_X\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(sel<0,1,3,5>())
    auto expr = LibKet::gates::measure_x(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(range<0,3>())
    auto expr = LibKet::gates::measure_x(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(qureg<0,4>())
    auto expr = LibKet::gates::measure_x(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(qubit<1>())
    auto expr = LibKet::gates::measure_x(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 1 "
      "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_X()
    auto expr = LibKet::gates::MEASURE_X();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilter\n|   expr "
      "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_X(all())
    auto expr = LibKet::gates::MEASURE_X(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QMeasure_X\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_X(sel<0,1,3,5>())
    auto expr = LibKet::gates::MEASURE_X(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_X(range<0,3>())
    auto expr = LibKet::gates::MEASURE_X(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_X(qureg<0,4>())
    auto expr = LibKet::gates::MEASURE_X(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // MEASURE_X(qubit<1>())
    auto expr = LibKet::gates::MEASURE_X(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 1 "
      "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_X()
    auto expr = QMeasure_X();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QMeasure_X\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_X()(all())
    auto expr = QMeasure_X()(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QMeasure_X\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_X()(sel<0,1,3,5>())
    auto expr = QMeasure_X()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_X()(range<0,3>())
    auto expr = QMeasure_X()(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_X()(qureg<0,4>())
    auto expr = QMeasure_X()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 0 "
      "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QMeasure_X()(qubit<1>())
    auto expr = QMeasure_X()(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelect [ 1 "
      "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(measure_x())
    auto expr = measure_x(measure_x());
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilter\n|   "
                "expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QMeasure_X\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(init())
    auto expr = measure_x(init());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelectAll\n|   "
      "expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // measure_x(measure_x(init()))
    auto expr = measure_x(measure_x(init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelectAll\n|   "
      "expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QMeasure_X\n| filter = QFilterSelectAll\n|   "
      "expr "
      "= UnaryQGate\n|          |   gate = QMeasure_X\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(measure_x(init()))
    auto expr = LibKet::gates::measure_x(LibKet::gates::init());
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}

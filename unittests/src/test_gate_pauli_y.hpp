/** @file test_gate_pauli_y.hpp
 *
 *  @brief UnitTests++ LibKet::gate::pauli_y() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_pauli_y)
{
  try {
    // pauli_y()
    auto expr = pauli_y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilter\n|   expr "
                "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(all())
    auto expr = pauli_y(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(sel<0,1,3,5>())
    auto expr = pauli_y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(range<0,3>())
    auto expr = pauli_y(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(qureg<0,4>())
    auto expr = pauli_y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(qubit<1>())
    auto expr = pauli_y(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PAULI_Y()
    auto expr = PAULI_Y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilter\n|   expr "
                "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PAULI_Y(all())
    auto expr = PAULI_Y(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PAULI_Y(sel<0,1,3,5>())
    auto expr = PAULI_Y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PAULI_Y(range<0,3>())
    auto expr = PAULI_Y(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PAULI_Y(qureg<0,4>())
    auto expr = PAULI_Y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PAULI_Y(qubit<1>())
    auto expr = PAULI_Y(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // y()
    auto expr = y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilter\n|   expr "
                "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // y(all())
    auto expr = y(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // y(sel<0,1,3,5>())
    auto expr = y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // y(range<0,3>())
    auto expr = y(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // y(qureg<0,4>())
    auto expr = y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // y(qubit<1>())
    auto expr = y(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Y()
    auto expr = Y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilter\n|   expr "
                "= QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Y(all())
    auto expr = Y(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Y(sel<0,1,3,5>())
    auto expr = Y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Y(range<0,3>())
    auto expr = Y(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Y(qureg<0,4>())
    auto expr = Y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Y(qubit<1>())
    auto expr = Y(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPauli_Y()
    auto expr = QPauli_Y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QPauli_Y\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPauli_Y()(all())
    auto expr = QPauli_Y()(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPauli_Y()(sel<0,1,3,5>())
    auto expr = QPauli_Y()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPauli_Y()(range<0,3>())
    auto expr = QPauli_Y()(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPauli_Y()(qureg<0,4>())
    auto expr = QPauli_Y()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 0 "
                "1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPauli_Y()(qubit<1>())
    auto expr = QPauli_Y()(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelect [ 1 "
                "]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(pauli_y())
    auto expr = pauli_y(pauli_y());
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilter\n|   "
                "expr = UnaryQGate\n|          |   gate = QPauli_Y\n|         "
                " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(init())
    auto expr = pauli_y(init());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // pauli_y(pauli_y(init()))
    auto expr = pauli_y(pauli_y(init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPauli_Y\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QPauli_Y\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(pauli_y(init()))
    auto expr = LibKet::gates::pauli_y(LibKet::gates::init());
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}

/** @file test_gate_prep_y.hpp
 *
 *  @brief UnitTests++ LibKet::gate::prep_y() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_prep_y)
{
  try {
    // prep_y()
    auto expr = prep_y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilter\n|   "
                "expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(all())
    auto expr = prep_y(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(sel<0,1,3,5>())
    auto expr = prep_y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(range<0,3>())
    auto expr = prep_y(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(qureg<0,4>())
    auto expr = prep_y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(qubit<1>())
    auto expr = prep_y(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Y()
    auto expr = PREP_Y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilter\n|   "
                "expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Y(all())
    auto expr = PREP_Y(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Y(sel<0,1,3,5>())
    auto expr = PREP_Y(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Y(range<0,3>())
    auto expr = PREP_Y(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Y(qureg<0,4>())
    auto expr = PREP_Y(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Y(qubit<1>())
    auto expr = PREP_Y(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Y()
    auto expr = QPrep_Y();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QPrep_Y\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Y()(all())
    auto expr = QPrep_Y()(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Y()(sel<0,1,3,5>())
    auto expr = QPrep_Y()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Y()(range<0,3>())
    auto expr = QPrep_Y()(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Y()(qureg<0,4>())
    auto expr = QPrep_Y()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Y()(qubit<1>())
    auto expr = QPrep_Y()(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelect [ "
                "1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(prep_y())
    auto expr = LibKet::gates::prep_y(LibKet::gates::prep_y());
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilter\n|   "
                "expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilter\n|   "
                "expr = UnaryQGate\n|          |   gate = QPrep_Y\n|         "
                " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(init())
    auto expr = LibKet::gates::prep_y(init());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_y(prep_y(init()))
    auto expr = LibKet::gates::prep_y(LibKet::gates::prep_y(init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPrep_Y\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QPrep_Y\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(prep_y(init()))
    auto expr = LibKet::gates::prep_y(LibKet::gates::init());
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}

/** @file test_gate_prep_z.hpp
 *
 *  @brief UnitTests++ LibKet::gate::prep_z() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_prep_z)
{
  try {
    // prep_z()
    auto expr = prep_z();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilter\n|   "
                "expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(all())
    auto expr = prep_z(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(sel<0,1,3,5>())
    auto expr = prep_z(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(range<0,3>())
    auto expr = prep_z(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(qureg<0,4>())
    auto expr = prep_z(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(qubit<1>())
    auto expr = prep_z(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Z()
    auto expr = PREP_Z();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilter\n|   "
                "expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Z(all())
    auto expr = PREP_Z(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Z(sel<0,1,3,5>())
    auto expr = PREP_Z(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Z(range<0,3>())
    auto expr = PREP_Z(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Z(qureg<0,4>())
    auto expr = PREP_Z(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // PREP_Z(qubit<1>())
    auto expr = PREP_Z(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Z()
    auto expr = QPrep_Z();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QPrep_Z\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Z()(all())
    auto expr = QPrep_Z()(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Z()(sel<0,1,3,5>())
    auto expr = QPrep_Z()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Z()(range<0,3>())
    auto expr = QPrep_Z()(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Z()(qureg<0,4>())
    auto expr = QPrep_Z()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "0 1 2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QPrep_Z()(qubit<1>())
    auto expr = QPrep_Z()(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelect [ "
                "1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(prep_z())
    auto expr = LibKet::gates::prep_z(LibKet::gates::prep_z());
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilter\n|   "
                "expr = QFilter\n");
#else
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilter\n|   "
                "expr = UnaryQGate\n|          |   gate = QPrep_Z\n|         "
                " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(init())
    auto expr = LibKet::gates::prep_z(init());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // prep_z(prep_z(init()))
    auto expr = LibKet::gates::prep_z(LibKet::gates::prep_z(init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QPrep_Z\n| filter = QFilterSelectAll\n|   expr "
      "= UnaryQGate\n|          |   gate = QPrep_Z\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(prep_z(init()))
    auto expr = LibKet::gates::prep_z(LibKet::gates::init());
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}

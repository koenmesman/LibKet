/** @file test_gate_rotate_x.hpp
 *
 *  @brief UnitTests++ LibKet::gate::rotate_x() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_x)
{
  try {
    // rotate_x(QConst(3.141))
    auto expr = rotate_x(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141),all())
    auto expr = rotate_x(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141),sel<0,1,3,5>())
    auto expr = rotate_x(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141),range<0,3>())
    auto expr = rotate_x(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141),qureg<0,4>())
    auto expr = rotate_x(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141),qubit<1>())
    auto expr = rotate_x(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_X(QConst(3.141))
    auto expr = ROTATE_X(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_X(QConst(3.141),all())
    auto expr = ROTATE_X(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_X(QConst(3.141),sel<0,1,3,5>())
    auto expr = ROTATE_X(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_X(QConst(3.141),range<0,3>())
    auto expr = ROTATE_X(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_X(QConst(3.141),qureg<0,4>())
    auto expr = ROTATE_X(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_X(QConst(3.141),qubit<1>())
    auto expr = ROTATE_X(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rx(QConst(3.141))
    auto expr = rx(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rx(QConst(3.141),all())
    auto expr = rx(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rx(QConst(3.141),sel<0,1,3,5>())
    auto expr = rx(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rx(QConst(3.141),range<0,3>())
    auto expr = rx(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rx(QConst(3.141),qureg<0,4>())
    auto expr = rx(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rx(QConst(3.141),qubit<1>())
    auto expr = rx(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RX(QConst(3.141))
    auto expr = RX(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RX(QConst(3.141),all())
    auto expr = RX(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RX(QConst(3.141),sel<0,1,3,5>())
    auto expr = RX(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RX(QConst(3.141),range<0,3>())
    auto expr = RX(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RX(QConst(3.141),qureg<0,4>())
    auto expr = RX(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RX(QConst(3.141),qubit<1>())
    auto expr = RX(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rx(QConst(3.141))
    auto expr = Rx(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rx(QConst(3.141),all())
    auto expr = Rx(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rx(QConst(3.141),sel<0,1,3,5>())
    auto expr = Rx(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rx(QConst(3.141),range<0,3>())
    auto expr = Rx(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rx(QConst(3.141),qureg<0,4>())
    auto expr = Rx(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Rx(QConst(3.141),qubit<1>())
    auto expr = Rx(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_X<QConst(3.141)>()
    auto expr = QRotate_X<QConst_t(3.141)>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QRotate_X 3.141\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_X<QConst(3.141)>()(all())
    auto expr = QRotate_X<QConst_t(3.141)>()(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_X<QConst(3.141)>()(sel<0,1,3,5>())
    auto expr = QRotate_X<QConst_t(3.141)>()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_X<QConst(3.141)>()(range<0,3>())
    auto expr = QRotate_X<QConst_t(3.141)>()(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_X<QConst(3.141)>()(qureg<0,4>())
    auto expr = QRotate_X<QConst_t(3.141)>()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_X<QConst(3.141)>()(qubit<1>())
    auto expr = QRotate_X<QConst_t(3.141)>()(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_x(QConst(3.141)))
    auto expr = rotate_x(QConst(3.141), rotate_x(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_X 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_x(QConst(-3.141)))
    auto expr = rotate_x(QConst(3.141), rotate_x(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_X -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_x(QConst(3.141)))
    auto expr = rotate_x(QConst(-3.141), rotate_x(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_X 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_x(QConst(-3.141)))
    auto expr = rotate_x(QConst(-3.141), rotate_x(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_X -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), init())
    auto expr = rotate_x(QConst(3.141), init());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_x(QConst(3.141), init()))
    auto expr = rotate_x(QConst(3.141), rotate_x(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_X 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_x(QConst(-3.141), init()))
    auto expr = rotate_x(QConst(3.141), rotate_x(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_X -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_x(QConst(3.141), init()))
    auto expr = rotate_x(QConst(-3.141), rotate_x(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_X 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_x(QConst(-3.141), init()))
    auto expr = rotate_x(QConst(-3.141), rotate_x(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_X -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_xdag(QConst(-3.141)))
    auto expr = rotate_x(QConst(3.141), rotate_xdag(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Xdag -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_xdag(QConst(3.141)))
    auto expr = rotate_x(QConst(3.141), rotate_xdag(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Xdag 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_xdag(QConst(-3.141)))
    auto expr = rotate_x(QConst(-3.141), rotate_xdag(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Xdag -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_xdag(QConst(3.141)))
    auto expr = rotate_x(QConst(-3.141), rotate_xdag(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Xdag 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_xdag(QConst(-3.141), init()))
    auto expr = rotate_x(QConst(3.141), rotate_xdag(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_Xdag -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(3.141), rotate_xdag(QConst(3.141), init()))
    auto expr = rotate_x(QConst(3.141), rotate_xdag(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_Xdag 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_xdag(QConst(-3.141), init()))
    auto expr = rotate_x(QConst(-3.141), rotate_xdag(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_Xdag -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_x(QConst(-3.141), rotate_xdag(QConst(3.141), init()))
    auto expr = rotate_x(QConst(-3.141), rotate_xdag(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_X -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_Xdag 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(rotate_x(QConst(3.141), init()))
    auto expr = rotate_x(QConst(3.141), init());
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}

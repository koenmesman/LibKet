/** @file test_gate_rotate_y.hpp
 *
 *  @brief UnitTests++ LibKet::gate::rotate_y() test
 *
 *  @copyright This file is part of the LibKet library
 *
 *  This Source Code Form is subject to the terms of the Mozilla Public
 *  License, v. 2.0. If a copy of the MPL was not distributed with this
 *  file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 *  @authors Matthias Moller
 */

using namespace LibKet::filters;
using namespace LibKet::gates;

TEST_FIXTURE(Fixture, gate_rotate_y)
{
  try {
    // rotate_y(QConst(3.141))
    auto expr = rotate_y(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141),all())
    auto expr = rotate_y(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141),sel<0,1,3,5>())
    auto expr = rotate_y(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141),range<0,3>())
    auto expr = rotate_y(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141),qureg<0,4>())
    auto expr = rotate_y(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141),qubit<1>())
    auto expr = rotate_y(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Y(QConst(3.141))
    auto expr = ROTATE_Y(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Y(QConst(3.141),all())
    auto expr = ROTATE_Y(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Y(QConst(3.141),sel<0,1,3,5>())
    auto expr = ROTATE_Y(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Y(QConst(3.141),range<0,3>())
    auto expr = ROTATE_Y(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Y(QConst(3.141),qureg<0,4>())
    auto expr = ROTATE_Y(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ROTATE_Y(QConst(3.141),qubit<1>())
    auto expr = ROTATE_Y(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ry(QConst(3.141))
    auto expr = ry(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ry(QConst(3.141),all())
    auto expr = ry(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ry(QConst(3.141),sel<0,1,3,5>())
    auto expr = ry(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ry(QConst(3.141),range<0,3>())
    auto expr = ry(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ry(QConst(3.141),qureg<0,4>())
    auto expr = ry(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // ry(QConst(3.141),qubit<1>())
    auto expr = ry(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RY(QConst(3.141))
    auto expr = RY(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RY(QConst(3.141),all())
    auto expr = RY(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RY(QConst(3.141),sel<0,1,3,5>())
    auto expr = RY(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RY(QConst(3.141),range<0,3>())
    auto expr = RY(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RY(QConst(3.141),qureg<0,4>())
    auto expr = RY(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // RY(QConst(3.141),qubit<1>())
    auto expr = RY(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Ry(QConst(3.141))
    auto expr = Ry(QConst(3.141));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n| "
                "  expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Ry(QConst(3.141),all())
    auto expr = Ry(QConst(3.141), all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Ry(QConst(3.141),sel<0,1,3,5>())
    auto expr = Ry(QConst(3.141), sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Ry(QConst(3.141),range<0,3>())
    auto expr = Ry(QConst(3.141), range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Ry(QConst(3.141),qureg<0,4>())
    auto expr = Ry(QConst(3.141), qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // Ry(QConst(3.141),qubit<1>())
    auto expr = Ry(QConst(3.141), qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Y<QConst(3.141)>()
    auto expr = QRotate_Y<QConst_t(3.141)>();
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(), "QRotate_Y 3.141\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Y<QConst(3.141)>()(all())
    auto expr = QRotate_Y<QConst_t(3.141)>()(all());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelectAll\n|   expr = QFilterSelectAll\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Y<QConst(3.141)>()(sel<0,1,3,5>())
    auto expr = QRotate_Y<QConst_t(3.141)>()(sel<0, 1, 3, 5>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "3 5 ]\n|   expr = QFilterSelect [ 0 1 3 5 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Y<QConst(3.141)>()(range<0,3>())
    auto expr = QRotate_Y<QConst_t(3.141)>()(range<0, 3>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Y<QConst(3.141)>()(qureg<0,4>())
    auto expr = QRotate_Y<QConst_t(3.141)>()(qureg<0, 4>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelect [ 0 1 "
      "2 3 ]\n|   expr = QFilterSelect [ 0 1 2 3 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // QRotate_Y<QConst(3.141)>()(qubit<1>())
    auto expr = QRotate_Y<QConst_t(3.141)>()(qubit<1>());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = "
                "QFilterSelect [ 1 ]\n|   expr = QFilterSelect [ 1 ]\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_y(QConst(3.141)))
    auto expr = rotate_y(QConst(3.141), rotate_y(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Y 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_y(QConst(-3.141)))
    auto expr = rotate_y(QConst(3.141), rotate_y(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Y -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_y(QConst(3.141)))
    auto expr = rotate_y(QConst(-3.141), rotate_y(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Y 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_y(QConst(-3.141)))
    auto expr = rotate_y(QConst(-3.141), rotate_y(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Y -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), init())
    auto expr = rotate_y(QConst(3.141), init());
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QInit\n|          | filter = "
      "QFilterSelectAll\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_y(QConst(3.141), init()))
    auto expr = rotate_y(QConst(3.141), rotate_y(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_Y 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_y(QConst(-3.141), init()))
    auto expr = rotate_y(QConst(3.141), rotate_y(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_Y -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_y(QConst(3.141), init()))
    auto expr = rotate_y(QConst(-3.141), rotate_y(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_Y 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_y(QConst(-3.141), init()))
    auto expr = rotate_y(QConst(-3.141), rotate_y(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_Y -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_ydag(QConst(-3.141)))
    auto expr = rotate_y(QConst(3.141), rotate_ydag(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Ydag -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_ydag(QConst(3.141)))
    auto expr = rotate_y(QConst(3.141), rotate_ydag(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Ydag 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_ydag(QConst(-3.141)))
    auto expr = rotate_y(QConst(-3.141), rotate_ydag(QConst(-3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(), "QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Ydag -3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_ydag(QConst(3.141)))
    auto expr = rotate_y(QConst(-3.141), rotate_ydag(QConst(3.141)));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilter\n|   "
      "expr = UnaryQGate\n|          |   gate = QRotate_Ydag 3.141\n|         "
      " | filter = QFilter\n|          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_ydag(QConst(-3.141), init()))
    auto expr = rotate_y(QConst(3.141), rotate_ydag(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_Ydag -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(3.141), rotate_ydag(QConst(3.141), init()))
    auto expr = rotate_y(QConst(3.141), rotate_ydag(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y 3.141\n| filter = QFilterSelectAll\n|  "
      " expr "
      "= UnaryQGate\n|          |   gate = QRotate_Ydag 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_ydag(QConst(-3.141), init()))
    auto expr = rotate_y(QConst(-3.141), rotate_ydag(QConst(-3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
#ifdef LIBKET_OPTIMIZE_GATES
    CHECK_EQUAL(ss.str(),
                "UnaryQGate\n|   gate = QInit\n| filter = QFilterSelectAll\n|  "
                " expr = QFilter\n");
#else
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_Ydag -3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
#endif
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // rotate_y(QConst(-3.141), rotate_ydag(QConst(3.141), init()))
    auto expr = rotate_y(QConst(-3.141), rotate_ydag(QConst(3.141), init()));
    std::stringstream ss;

    show<99>(expr, ss);
    CHECK_EQUAL(
      ss.str(),
      "UnaryQGate\n|   gate = QRotate_Y -3.141\n| filter = QFilterSelectAll\n| "
      "  expr "
      "= UnaryQGate\n|          |   gate = QRotate_Ydag 3.141\n|          | "
      "filter = "
      "QFilterSelectAll\n|          |   expr = UnaryQGate\n|          |        "
      "  |   gate = QInit\n|          |          | filter = "
      "QFilterSelectAll\n|          |          |   expr = QFilter\n");
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }

  try {
    // run(rotate_y(QConst(3.141), init()))
    auto expr = rotate_y(QConst(3.141), init());
    CHECK(run(expr));
  } catch (const std::exception& e) {
    std::cerr << e.what() << std::endl;
    return;
  }
}
